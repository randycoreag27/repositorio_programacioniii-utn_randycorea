﻿using System;
using System.Collections.Generic;
using System.Xml;
using Capa_Datos;
using Objetos;

namespace Capa_Negocio
{
    public class XML_Registro_Vacunacion
    {
        XmlDocument doc;
        string rutaXml;

        //creación del xml
        public void CrearXML(string ruta, string nodoRaiz)
        {
            rutaXml = ruta;
            doc = new XmlDocument();

            XmlDeclaration xmlDeclaration = doc.CreateXmlDeclaration("1.0", "UTF-8", "no");//declaracion

            XmlNode nodo = doc.DocumentElement;
            doc.InsertBefore(xmlDeclaration, nodo);

            XmlNode elemento = doc.CreateElement(nodoRaiz);
            doc.AppendChild(elemento);

            new Guardar_XML().guardarDatosXML(doc, ruta);
        }

        //Lectura
        public void LeerXML(string ruta)
        {
            rutaXml = ruta;
            doc = new XmlDocument();
            doc.Load(ruta);//lee
        }

        //Registro
        public void Registro_Vacunacion(Objeto_Informacion_Persona obj)
        {
            XmlNode usuario = Crear_Registro_Vacunacion(obj);

            XmlNode nodo = doc.DocumentElement;

            nodo.InsertAfter(usuario, nodo.LastChild);

            new Guardar_XML().guardarDatosXML(doc, rutaXml);
        }

        //Orden de Insertar datos en el archivo

        public XmlNode Crear_Registro_Vacunacion(Objeto_Informacion_Persona obj)
        {
            XmlNode datos = doc.CreateElement("Datos_Personales");

            XmlElement xcedula = doc.CreateElement("Cédula");
            xcedula.InnerText = obj.cedula.ToString();
            datos.AppendChild(xcedula);

            XmlElement xnombre = doc.CreateElement("Nombre");
            xnombre.InnerText = obj.nombre.ToString();
            datos.AppendChild(xnombre);

            XmlElement xgenero = doc.CreateElement("Género");
            xgenero.InnerText = obj.genero.ToString();
            datos.AppendChild(xgenero);

            XmlElement xfechanacimiento = doc.CreateElement("Fecha_Nacimiento");
            xfechanacimiento.InnerText = obj.fecha_nacimiento.ToString();
            datos.AppendChild(xfechanacimiento);

            XmlElement xprovincia = doc.CreateElement("Provincia");
            xprovincia.InnerText = obj.provincia.ToString();
            datos.AppendChild(xprovincia);

            XmlElement xtipo = doc.CreateElement("Tipo_Vacuna");
            xtipo.InnerText = obj.tipo_vacuna.ToString();
            datos.AppendChild(xtipo);

            XmlElement xcantidad = doc.CreateElement("Cantidad_Dosis");
            xcantidad.InnerText = obj.cantidad_dosis.ToString();
            datos.AppendChild(xcantidad);

            XmlElement x1fecha = doc.CreateElement("Fecha_Primera_Dosis");
            x1fecha.InnerText = obj.fecha_primera_dosis.ToString();
            datos.AppendChild(x1fecha);

            XmlElement x2fecha = doc.CreateElement("Fecha_Segunda_Dosis");
            x2fecha.InnerText = obj.fecha_segunda_dosis.ToString();
            datos.AppendChild(x2fecha);

            return datos;
        }

        //login
        public List<Objeto_Usuario> Cargar_Datos_Login()
        {
            XmlNodeList listausers = doc.SelectNodes("Información/Datos_Usuarios");

            List<Objeto_Usuario> lista = new List<Objeto_Usuario>();

            XmlNode unUsuario;

            for (int i = 0; i < listausers.Count; i++)
            {
                unUsuario = listausers.Item(i);
                Objeto_Usuario usuario = new Objeto_Usuario
                {
                    cedula = Convert.ToInt32(unUsuario.SelectSingleNode("Cédula").InnerText),
                    contraseña = unUsuario.SelectSingleNode("Contraseña").InnerText,
                };
                lista.Add(usuario);
            }
            return lista;

        }

    }
}
