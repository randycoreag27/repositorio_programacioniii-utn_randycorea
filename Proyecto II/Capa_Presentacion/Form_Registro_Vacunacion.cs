﻿using System;
using System.IO;
using System.Windows.Forms;
using Capa_Negocio;
using Objetos;

namespace Capa_Presentacion
{
    public partial class Form_Registro_Vacunacion : Form
    {
        XML_Registro_Vacunacion xml = new XML_Registro_Vacunacion(); //Instacia de la clase donde se realiza el proceso del archivo XML
        Objeto_Informacion_Persona obj = new Objeto_Informacion_Persona(); //Instacia de la clase donde se encuentra el objeto de la información de la persona
        string genero = "";
        string numero_dosis = "";

        public Form_Registro_Vacunacion()
        {
            InitializeComponent();
            CrearXML();
        }
        //Crear el archivo xml o bien consultar si ya existe
        public void CrearXML()
        {
            string ruta = "Registro_Vacunacion.xml";
            if (!File.Exists(ruta))
            {
                xml.CrearXML(ruta, "Información");
            }
            else
            {
                xml.LeerXML(ruta);
            }
        }

        //Método para limpiar campos de interfaz gráfica
        public void Limpiar()
        {
            txtCedula.Clear();
            txtNombre.Clear();
            rbFemenino.Checked = false;
            rbMasculino.Checked = false;
            comboTipoVacuna.SelectedItem = false;
            rbUnaDosis.Checked = false;
            rbDosDosis.Checked = false;              
        }

        //Método en el cual se obtinen los datos de la interfaz gráfica
        public void Obtener_Datos()
        {
            //genero
            if (rbFemenino.Checked)
                genero = "Femenino";
            else if (rbMasculino.Checked)
                genero = "Masculino";
            //Cantidad_Dosis
            if (rbUnaDosis.Checked)
                numero_dosis = "Una Dosis";
            else if (rbDosDosis.Checked)
                numero_dosis = "Dos Dosis";

            obj.cedula = Convert.ToInt32(txtCedula.Text);
            obj.nombre = txtNombre.Text;
            obj.genero = genero;
            obj.fecha_nacimiento = DateFecha_nacimiento.Value.Date.ToString();
            obj.provincia = Convert.ToString(comboProvincia.SelectedItem);
            obj.tipo_vacuna = Convert.ToString(comboTipoVacuna.SelectedItem);
            obj.cantidad_dosis = numero_dosis;
            obj.fecha_primera_dosis = DateFecha1Dosis.Value.Date.ToString();
            obj.fecha_segunda_dosis = DateFecha2Dosis.Value.Date.ToString();
        }

        //Método en el cual se realiza el proceso a llevar para insertar los datos de la persona en el archivo XML
        public void Registro()
        {
            Obtener_Datos();
            xml.Registro_Vacunacion(obj);//envio informacion a archivo
            MessageBox.Show("El registro se ha realizado de manera exitosa");
            Limpiar();
        }

        private void btn_iniciar_sesion_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form_Inicio_Sesion frm = new Form_Inicio_Sesion();
            frm.Show();
        }

        private void btn_registrar_Click(object sender, EventArgs e)
        {
            Registro();
        }

        private void btn_salir_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
