﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using Capa_Negocio;
using Objetos;

namespace Capa_Presentacion
{
    public partial class Form_Inicio_Sesion : Form
    {

        XML_Registro_Vacunacion xml = new XML_Registro_Vacunacion();//instancia a la capa de negocio
        List<Objeto_Usuario> listaUsu = new List<Objeto_Usuario>();

        public Form_Inicio_Sesion()
        {
            InitializeComponent();
            string ruta = "Registro_Vacunacion.xml";
            xml.LeerXML(ruta);
        }
        

        private void btn_login_Click(object sender, EventArgs e)
        {
            if (txtUser.Text == "" || txtContraseña.Text == "" || txtUser.Text == "" && txtContraseña.Text == "")
            {
                MessageBox.Show("Debe ingresar todos los datos solicitados");
            }
            else
            {
                if (Regex.IsMatch(txtUser.Text, @"^[0-9]+$")) {//Esta condición valida que el valor ingresado en el cmpo de cedula sea un número
                    listaUsu = xml.Cargar_Datos_Login();
                    for (int i = 0; i < listaUsu.Count; i++)
                    {
                        if (listaUsu[i].cedula.Equals(Convert.ToInt32(txtUser.Text)) && listaUsu[i].contraseña.Equals(txtContraseña.Text))
                        {
                            this.Hide();
                            MessageBox.Show("Bienvenido administrador");
                            Form_Reportes_Administrador frm = new Form_Reportes_Administrador();
                            frm.Show();
                            
                        }
                        else if (listaUsu[i].cedula.Equals(txtUser.Text) && listaUsu[i].contraseña != "admin" 
                            || listaUsu[i].cedula != listaUsu[i].cedula && listaUsu[i].contraseña.Equals("admin"))
                        {
                            MessageBox.Show("Acceso denegado, intentelo nuevamente!");
                        }
                    }
                }
                else
                {
                    MessageBox.Show("El campo cedula solo permite datos numéricos, intentelo nuevamente!");
                }

            }
        }

        private void btn_salir_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btn_regresar_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form_Registro_Vacunacion frm = new Form_Registro_Vacunacion();
            frm.Show();
        }
    }
}
