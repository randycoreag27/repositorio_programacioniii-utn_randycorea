﻿
namespace Objetos
{
    public class Objeto_Informacion_Persona
    {
        public int cedula { get; set; }
        public string nombre { get; set; }
        public string genero { get; set; }
        public string fecha_nacimiento { get; set; }
        public string provincia { get; set; }
        public string tipo_vacuna { get; set; }
        public string cantidad_dosis { get; set; }
        public string fecha_primera_dosis { get; set; }
        public string fecha_segunda_dosis { get; set; }


    }
}
