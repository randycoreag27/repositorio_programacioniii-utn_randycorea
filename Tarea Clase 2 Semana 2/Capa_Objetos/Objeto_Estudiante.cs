﻿using System;

namespace Capa_Objetos
{
    public class Objeto_Estudiante
    {
        public int cedula { get; set; }
        public string nombre { get; set; }
        public int edad { get; set; }
        public string residencia { get; set; }
        public DateTime fecha_nacimiento { get; set; }
    }
}
