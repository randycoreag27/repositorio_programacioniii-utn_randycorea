﻿using Capa_Objetos;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Capa_Datos
{
    public class BD_Estudiantes
    {
        static NpgsqlConnection conexion;
        static NpgsqlCommand cmd;
        //Objeto_Estudiante es;

        public void Insertar_Estudiantes(Objeto_Estudiante es)
        {
            conexion = Conexion_BD.connection();
            conexion.Open();
            cmd = new NpgsqlCommand("INSERT INTO Estudiante(cedula,nombre,edad,fecha_nacimiento,residencia) VALUES " +
                "('" + es.cedula + "', '" + es.nombre + "', '" + es.edad + "', '" + es.fecha_nacimiento + "', '" + es.residencia + "')", conexion);
            cmd.ExecuteNonQuery();
            conexion.Close();
        }

        public void Eliminar_Estudiantes(Objeto_Estudiante es)
        {
            conexion = Conexion_BD.connection();
            conexion.Open();
            cmd = new NpgsqlCommand("DELETE FROM Estudiante WHERE cedula = '" + es.cedula + "'", conexion);
            cmd.ExecuteNonQuery();
            conexion.Close();
            MessageBox.Show("Datos Eliminados Correctamente");
        }

        public void Modificar_Estudiantes(Objeto_Estudiante es)
        {
            conexion = Conexion_BD.connection();
            conexion.Open();
            cmd = new NpgsqlCommand("UPDATE Estudiante SET nombre = '" + es.nombre + "', " + "fecha_nacimiento = '" + es.fecha_nacimiento + "', " +
                "residencia = '" + es.residencia + "', " + "edad = '" + es.edad + "' WHERE cedula = '" + es.cedula + "'", conexion);
            cmd.ExecuteNonQuery();
            conexion.Close();
            MessageBox.Show("Datos Modificados Correctamente");

        }


        public void Mostrar_Estudiantes(DataGridView dgv)
        {
            conexion = Conexion_BD.connection();
            DataTable data_table = new DataTable();
            NpgsqlDataAdapter adapter = new NpgsqlDataAdapter("SELECT * FROM Estudiante", conexion);
            adapter.Fill(data_table);
            dgv.DataSource = data_table;
        }




    }
}
