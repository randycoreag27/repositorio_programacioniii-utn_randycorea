﻿using Capa_Negocio;
using Capa_Objetos;
using System;
using System.Windows.Forms;

namespace Capa_Presentacion
{
    public partial class Frm_Estudiantes : Form
    {

        Objeto_Estudiante oe = new Objeto_Estudiante();
        Estudiantes es = new Estudiantes();

        public Frm_Estudiantes()
        {
            InitializeComponent();
        }

        public void Obtener_Datos()
        {
            oe.cedula = Convert.ToInt32(txt_cedula.Text);
            oe.nombre = txt_nombre.Text;
            oe.edad = Convert.ToInt32(txt_edad.Text);
            oe.residencia = txt_residencia.Text;
            oe.fecha_nacimiento = dtp_fecha_nacimiento.Value;
        }

        public void Limpiar_Datos()
        {
            txt_cedula.Clear();
            txt_nombre.Clear();
            txt_edad.Clear();
            txt_residencia.Clear();
            dtp_fecha_nacimiento.ResetText();
        }

        public void Mostrar_Datos()
        {
            es.Mostrar(dgv_estudiantes);
        }

        //private void Frm_Estudiantes_Load(object sender, EventArgs e)
        //{
        //    Mostrar_Datos();
        //}

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                txt_cedula.Enabled = false;
                DataGridViewRow row = dgv_estudiantes.Rows[e.RowIndex];
                txt_cedula.Text = row.Cells["cedula"].Value.ToString();
                txt_nombre.Text = row.Cells["nombre"].Value.ToString();
                txt_edad.Text = row.Cells["edad"].Value.ToString();
                txt_residencia.Text = row.Cells["residencia"].Value.ToString();
                dtp_fecha_nacimiento.Value = Convert.ToDateTime(row.Cells["fecha_nacimiento"].Value.ToString());

            }

        }

        private void btn_limpiar_Click(object sender, EventArgs e)
        {
            Limpiar_Datos();
        }

        private void btn_insertar_Click(object sender, EventArgs e)
        {
            Obtener_Datos();
            es.Insertar(oe);
            Limpiar_Datos();
            Mostrar_Datos();
        }

        private void Frm_Estudiantes_Load_1(object sender, EventArgs e)
        {
            Mostrar_Datos();
        }

        private void btn_eliminar_Click(object sender, EventArgs e)
        {
            Obtener_Datos();

            DialogResult respuesta = MessageBox.Show("¿Deseas Eliminar el Registro Seleccionado?", "Eliminar Registro?", 
                MessageBoxButtons.YesNo); 

            if (respuesta == DialogResult.Yes) 
            {
                es.Eliminar(oe);
                Limpiar_Datos();
                Mostrar_Datos();
            }
        }

        private void btn_modificar_Click(object sender, EventArgs e)
        {
            Obtener_Datos();

            DialogResult respuesta = MessageBox.Show("¿Deseas Modificar el Registro Seleccionado?", "Modificar Registro?",
                MessageBoxButtons.YesNo);

            if (respuesta == DialogResult.Yes)
            {
                es.Modificar(oe);
                Limpiar_Datos();
                Mostrar_Datos();
            }
        }
    }
}
