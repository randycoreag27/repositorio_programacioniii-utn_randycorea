﻿using Capa_Datos;
using Capa_Objetos;
using System.Windows.Forms;

namespace Capa_Negocio
{
    public class Estudiantes
    {

        BD_Estudiantes bd_es = new BD_Estudiantes();

        public void Insertar(Objeto_Estudiante es)
        {
            bd_es.Insertar_Estudiantes(es);
        }

        public void Eliminar(Objeto_Estudiante es)
        {
            bd_es.Eliminar_Estudiantes(es);
        }

        public void Modificar(Objeto_Estudiante es)
        {
            bd_es.Modificar_Estudiantes(es);
        }

        public void Mostrar(DataGridView dgv)
        {
            bd_es.Mostrar_Estudiantes(dgv);
        }
    }
}
