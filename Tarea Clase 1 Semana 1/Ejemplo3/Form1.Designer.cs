﻿
namespace Ejemplo3
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.check_box_1 = new System.Windows.Forms.CheckBox();
            this.check_box_2 = new System.Windows.Forms.CheckBox();
            this.check_box_3 = new System.Windows.Forms.CheckBox();
            this.radio_boton_1 = new System.Windows.Forms.RadioButton();
            this.radio_boton_2 = new System.Windows.Forms.RadioButton();
            this.radio_boton_3 = new System.Windows.Forms.RadioButton();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.boton_calcular = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // check_box_1
            // 
            this.check_box_1.AutoSize = true;
            this.check_box_1.Location = new System.Drawing.Point(22, 225);
            this.check_box_1.Name = "check_box_1";
            this.check_box_1.Size = new System.Drawing.Size(99, 19);
            this.check_box_1.TabIndex = 0;
            this.check_box_1.Text = "Por Email ($5)";
            this.check_box_1.UseVisualStyleBackColor = true;
            // 
            // check_box_2
            // 
            this.check_box_2.AutoSize = true;
            this.check_box_2.Location = new System.Drawing.Point(22, 270);
            this.check_box_2.Name = "check_box_2";
            this.check_box_2.Size = new System.Drawing.Size(118, 19);
            this.check_box_2.TabIndex = 1;
            this.check_box_2.Text = "Por Telefono($15)";
            this.check_box_2.UseVisualStyleBackColor = true;
            // 
            // check_box_3
            // 
            this.check_box_3.AutoSize = true;
            this.check_box_3.Location = new System.Drawing.Point(22, 313);
            this.check_box_3.Name = "check_box_3";
            this.check_box_3.Size = new System.Drawing.Size(91, 19);
            this.check_box_3.TabIndex = 2;
            this.check_box_3.Text = "Por Fax($20)";
            this.check_box_3.UseVisualStyleBackColor = true;
            // 
            // radio_boton_1
            // 
            this.radio_boton_1.AutoSize = true;
            this.radio_boton_1.Checked = true;
            this.radio_boton_1.Location = new System.Drawing.Point(22, 52);
            this.radio_boton_1.Name = "radio_boton_1";
            this.radio_boton_1.Size = new System.Drawing.Size(154, 19);
            this.radio_boton_1.TabIndex = 3;
            this.radio_boton_1.TabStop = true;
            this.radio_boton_1.Text = "Por Correo Normal ($50)";
            this.radio_boton_1.UseVisualStyleBackColor = true;
            // 
            // radio_boton_2
            // 
            this.radio_boton_2.AutoSize = true;
            this.radio_boton_2.Location = new System.Drawing.Point(22, 92);
            this.radio_boton_2.Name = "radio_boton_2";
            this.radio_boton_2.Size = new System.Drawing.Size(180, 19);
            this.radio_boton_2.TabIndex = 4;
            this.radio_boton_2.TabStop = true;
            this.radio_boton_2.Text = "Por Paquetería Normal ($100)";
            this.radio_boton_2.UseVisualStyleBackColor = true;
            // 
            // radio_boton_3
            // 
            this.radio_boton_3.AutoSize = true;
            this.radio_boton_3.Location = new System.Drawing.Point(22, 136);
            this.radio_boton_3.Name = "radio_boton_3";
            this.radio_boton_3.Size = new System.Drawing.Size(182, 19);
            this.radio_boton_3.TabIndex = 5;
            this.radio_boton_3.TabStop = true;
            this.radio_boton_3.Text = "Por Paquetería Urgente ($150)";
            this.radio_boton_3.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(22, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(262, 15);
            this.label1.TabIndex = 6;
            this.label1.Text = "Como desea recibir el pedido? (marcar solo una)";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(22, 186);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(325, 15);
            this.label2.TabIndex = 7;
            this.label2.Text = "Como desea ser notificado del envío? (marcar las que desea)";
            // 
            // boton_calcular
            // 
            this.boton_calcular.Location = new System.Drawing.Point(498, 308);
            this.boton_calcular.Name = "boton_calcular";
            this.boton_calcular.Size = new System.Drawing.Size(249, 23);
            this.boton_calcular.TabIndex = 8;
            this.boton_calcular.Text = "Calcular Gastos de Envío";
            this.boton_calcular.UseVisualStyleBackColor = true;
            this.boton_calcular.Click += new System.EventHandler(this.boton_calcular_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 357);
            this.Controls.Add(this.boton_calcular);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.radio_boton_3);
            this.Controls.Add(this.radio_boton_2);
            this.Controls.Add(this.radio_boton_1);
            this.Controls.Add(this.check_box_3);
            this.Controls.Add(this.check_box_2);
            this.Controls.Add(this.check_box_1);
            this.Name = "Form1";
            this.Text = "Gastos de Envío";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox check_box_1;
        private System.Windows.Forms.CheckBox check_box_2;
        private System.Windows.Forms.CheckBox check_box_3;
        private System.Windows.Forms.RadioButton radio_boton_1;
        private System.Windows.Forms.RadioButton radio_boton_2;
        private System.Windows.Forms.RadioButton radio_boton_3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button boton_calcular;
    }
}

