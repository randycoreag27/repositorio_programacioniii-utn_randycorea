﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ejemplo3
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void boton_calcular_Click(object sender, EventArgs e)
        {
            int total=0;

            //Condiciones Radio Buttons
            if (radio_boton_1.Checked==true)
            {
                total = total + 50;
            }
            if (radio_boton_2.Checked == true)
            {
                total = total + 100;
            }
            if (radio_boton_3.Checked == true)
            {
                total = total + 150;
            }

            //Condiciones CheckBoxs

            if (check_box_1.Checked == true)
            {
                total = total + 5;
            }
            if (check_box_2.Checked == true)
            {
                total = total + 15;
            }
            if (check_box_3.Checked == true)
            {
                total = total + 20;

            }

            MessageBox.Show("El total de gastos de envios es: " + total.ToString("c2"));

        }
    }
}
