﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ejemplo1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void boton_suma_Click(object sender, EventArgs e)
        {
            
            int valor_1, valor_2, resultado;
            valor_1 = int.Parse(txt_primer_valor.Text);
            valor_2 = int.Parse(txt_segundo_valor.Text);
            resultado = valor_1 + valor_2;
            txt_resultado.Text = resultado.ToString();

        }

        private void boton_salir_Click(object sender, EventArgs e)
        {

            Application.Exit();

        }

        private void boton_limpiar_Click(object sender, EventArgs e)
        {

            txt_primer_valor.Clear();
            txt_segundo_valor.Clear();
            txt_resultado.Clear();

        }
    }
}
