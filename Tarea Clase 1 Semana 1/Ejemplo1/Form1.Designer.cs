﻿
namespace Ejemplo1
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.boton_suma = new System.Windows.Forms.Button();
            this.boton_salir = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txt_primer_valor = new System.Windows.Forms.TextBox();
            this.txt_segundo_valor = new System.Windows.Forms.TextBox();
            this.txt_resultado = new System.Windows.Forms.TextBox();
            this.boton_limpiar = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // boton_suma
            // 
            this.boton_suma.Location = new System.Drawing.Point(12, 197);
            this.boton_suma.Name = "boton_suma";
            this.boton_suma.Size = new System.Drawing.Size(75, 23);
            this.boton_suma.TabIndex = 0;
            this.boton_suma.Text = "Sumar";
            this.boton_suma.UseVisualStyleBackColor = true;
            this.boton_suma.Click += new System.EventHandler(this.boton_suma_Click);
            // 
            // boton_salir
            // 
            this.boton_salir.Location = new System.Drawing.Point(194, 197);
            this.boton_salir.Name = "boton_salir";
            this.boton_salir.Size = new System.Drawing.Size(75, 23);
            this.boton_salir.TabIndex = 1;
            this.boton_salir.Text = "Salir";
            this.boton_salir.UseVisualStyleBackColor = true;
            this.boton_salir.Click += new System.EventHandler(this.boton_salir_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(44, 40);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(74, 15);
            this.label1.TabIndex = 2;
            this.label1.Text = "Primer Valor:";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(44, 90);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(86, 15);
            this.label2.TabIndex = 3;
            this.label2.Text = "Segundo Valor:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(44, 136);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(62, 15);
            this.label3.TabIndex = 4;
            this.label3.Text = "Resultado:";
            // 
            // txt_primer_valor
            // 
            this.txt_primer_valor.Location = new System.Drawing.Point(140, 37);
            this.txt_primer_valor.Name = "txt_primer_valor";
            this.txt_primer_valor.Size = new System.Drawing.Size(108, 23);
            this.txt_primer_valor.TabIndex = 5;
            // 
            // txt_segundo_valor
            // 
            this.txt_segundo_valor.Location = new System.Drawing.Point(140, 87);
            this.txt_segundo_valor.Name = "txt_segundo_valor";
            this.txt_segundo_valor.Size = new System.Drawing.Size(108, 23);
            this.txt_segundo_valor.TabIndex = 6;
            // 
            // txt_resultado
            // 
            this.txt_resultado.Location = new System.Drawing.Point(140, 133);
            this.txt_resultado.Name = "txt_resultado";
            this.txt_resultado.Size = new System.Drawing.Size(108, 23);
            this.txt_resultado.TabIndex = 7;
            // 
            // boton_limpiar
            // 
            this.boton_limpiar.Location = new System.Drawing.Point(104, 197);
            this.boton_limpiar.Name = "boton_limpiar";
            this.boton_limpiar.Size = new System.Drawing.Size(75, 23);
            this.boton_limpiar.TabIndex = 8;
            this.boton_limpiar.Text = "Limpiar";
            this.boton_limpiar.UseVisualStyleBackColor = true;
            this.boton_limpiar.Click += new System.EventHandler(this.boton_limpiar_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(281, 241);
            this.Controls.Add(this.boton_limpiar);
            this.Controls.Add(this.txt_resultado);
            this.Controls.Add(this.txt_segundo_valor);
            this.Controls.Add(this.txt_primer_valor);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.boton_salir);
            this.Controls.Add(this.boton_suma);
            this.Name = "Form1";
            this.Text = "SUMA";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button boton_suma;
        private System.Windows.Forms.Button boton_salir;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txt_primer_valor;
        private System.Windows.Forms.TextBox txt_segundo_valor;
        private System.Windows.Forms.TextBox txt_resultado;
        private System.Windows.Forms.Button boton_limpiar;
    }
}

