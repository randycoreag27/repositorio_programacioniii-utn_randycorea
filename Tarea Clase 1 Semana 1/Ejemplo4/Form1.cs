﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ejemplo4
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            list_box_suma.Items.Clear();
        }

        private void boton_iniciar_Click(object sender, EventArgs e)
        {
            list_box_suma.Items.Clear();
            int x, n, suma=0;
            n = int.Parse(txt_numero.Text);
            for (x = 1; x <= n; x++)
            {
                suma = suma + x;
                if (check_box_parciales.Checked == true)
                {
                    list_box_suma.Items.Add("sumando: " + x + " suma parcial: " + suma);
                }
            }
            list_box_suma.Items.Add("Las suma total es de: " + suma);
        }
    }
}
