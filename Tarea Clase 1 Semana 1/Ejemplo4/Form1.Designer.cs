﻿
namespace Ejemplo4
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txt_numero = new System.Windows.Forms.TextBox();
            this.check_box_parciales = new System.Windows.Forms.CheckBox();
            this.list_box_suma = new System.Windows.Forms.ListBox();
            this.boton_iniciar = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(28, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(153, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "Calcular la suma de 1 hasta:";
            // 
            // txt_numero
            // 
            this.txt_numero.Location = new System.Drawing.Point(196, 20);
            this.txt_numero.Name = "txt_numero";
            this.txt_numero.Size = new System.Drawing.Size(100, 23);
            this.txt_numero.TabIndex = 1;
            // 
            // check_box_parciales
            // 
            this.check_box_parciales.AutoSize = true;
            this.check_box_parciales.Location = new System.Drawing.Point(28, 51);
            this.check_box_parciales.Name = "check_box_parciales";
            this.check_box_parciales.Size = new System.Drawing.Size(143, 19);
            this.check_box_parciales.TabIndex = 2;
            this.check_box_parciales.Text = "Ver resultado parciales";
            this.check_box_parciales.UseVisualStyleBackColor = true;
            // 
            // list_box_suma
            // 
            this.list_box_suma.FormattingEnabled = true;
            this.list_box_suma.ItemHeight = 15;
            this.list_box_suma.Location = new System.Drawing.Point(28, 92);
            this.list_box_suma.Name = "list_box_suma";
            this.list_box_suma.Size = new System.Drawing.Size(268, 139);
            this.list_box_suma.TabIndex = 3;
            // 
            // boton_iniciar
            // 
            this.boton_iniciar.Location = new System.Drawing.Point(131, 237);
            this.boton_iniciar.Name = "boton_iniciar";
            this.boton_iniciar.Size = new System.Drawing.Size(75, 23);
            this.boton_iniciar.TabIndex = 4;
            this.boton_iniciar.Text = "Iniciar";
            this.boton_iniciar.UseVisualStyleBackColor = true;
            this.boton_iniciar.Click += new System.EventHandler(this.boton_iniciar_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(322, 271);
            this.Controls.Add(this.boton_iniciar);
            this.Controls.Add(this.list_box_suma);
            this.Controls.Add(this.check_box_parciales);
            this.Controls.Add(this.txt_numero);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Suma de 1 a N";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txt_numero;
        private System.Windows.Forms.CheckBox check_box_parciales;
        private System.Windows.Forms.ListBox list_box_suma;
        private System.Windows.Forms.Button boton_iniciar;
    }
}

