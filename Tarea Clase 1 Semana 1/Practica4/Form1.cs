﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Practica4
{
    public partial class Form1 : Form
    {
        NumeroRomano nRomano;
        public Form1()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            nRomano = new NumeroRomano();
            int n = int.Parse(txt_numero.Text);
            string r = nRomano.convertir_a_romano(n);
            label_resultado.Text=r;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            txt_numero.Clear();
            label_resultado.Text = "";
        }
    }
}
