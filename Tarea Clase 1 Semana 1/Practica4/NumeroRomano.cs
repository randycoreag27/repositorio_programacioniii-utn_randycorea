﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace Practica4
{
    class NumeroRomano
    {
        public string convertir_a_romano(int num)
        {
            string romano="";
            while (true)
            {
                if (num < 4000 && num > 999)
                {
                    switch (num / 1000)
                    {
                        case 1: romano +="M"; break;
                        case 2: romano += "MM"; break;
                        case 3: romano += "MMM"; break;
                    }

                    num -= 1000*(num / 1000);

                }else if(num < 1000 && num > 99)
                {
                    switch (num / 100)
                    {
                        case 1: romano += "C"; break;
                        case 2: romano += "CC"; break;
                        case 3: romano += "CCC"; break;
                        case 4: romano += "CD"; break;
                        case 5: romano += "D"; break;
                        case 6: romano += "DC"; break;
                        case 7: romano += "DCC"; break;
                        case 8: romano += "DCCC"; break;
                        case 9: romano += "CM"; break;
                    }
                    num -= 100 * (num / 100);
                }
                else if (num < 100 && num > 10)
                {
                    switch (num / 10)
                    {
                        case 1: romano += "X"; break;
                        case 2: romano += "XX"; break;
                        case 3: romano += "XXX"; break;
                        case 4: romano += "XL"; break;
                        case 5: romano += "L"; break;
                        case 6: romano += "LX"; break;
                        case 7: romano += "LXX"; break;
                        case 8: romano += "LXXX"; break;
                        case 9: romano += "XC"; break;
                    }
                    num -= 10 * (num / 10);
                }
                else if (num <= 9 && num > 0)
                {
                    switch (num)
                    {
                        case 1: romano += "I"; break;
                        case 2: romano += "II"; break;
                        case 3: romano += "III"; break;
                        case 4: romano += "IV"; break;
                        case 5: romano += "V"; break;
                        case 6: romano += "VI"; break;
                        case 7: romano += "VII"; break;
                        case 8: romano += "VIII"; break;
                        case 9: romano += "IX"; break;
                    }
                    num -= num;
                }
                else
                {
                    MessageBox.Show("Error, el número está fuera del rango");
                    break;
                }
                if (num == 0)
                {
                    break;
                }
            }

            return romano;
        }
    }
}
