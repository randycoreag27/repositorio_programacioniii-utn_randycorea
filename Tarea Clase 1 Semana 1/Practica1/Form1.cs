﻿using Microsoft.VisualBasic;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Practica1
{
    public partial class Form1 : Form
    {

        List<int> valores = new List<int>();
        int num;
        public Form1()
        {
            InitializeComponent();
            this.list_box_multiplos.Items.Clear();
        }

        private void radio_boton_1_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void boton_realizar_Click(object sender, EventArgs e)
        {
            if (radio_boton_1.Checked == true)
            {
                int x;
                for (x = 1; x <= 100; x++)
                {
                    if (x % 3 == 0)
                    {
                        list_box_multiplos.Items.Add(x);
                    }
                }
            }else if(radio_boton_2.Checked == true)
            {
                int p;
                foreach (int n in valores)
                {
                    if (n % 2 == 0)
                    {
                        p = valores.IndexOf(n);
                        list_box_pares_impares.Items.Add(n + ": es numero par y se encuentra en la posición: " + p);
                    }
                    else if (n % 2 == 1)
                    {
                        p = valores.IndexOf(n);
                        list_box_pares_impares.Items.Add(n + ": es numero impar y se encuentra en la posición: " + p);
                    }
                }
            }else if (radio_boton_3.Checked == true)
            {
                int a = 0, num;
                num = int.Parse(txt_numero_primo.Text);
                for (int i = 1; i < (num + 1); i++)
                {
                    if (num % i == 0)
                    {
                        a++;
                    }
                }
                if (a != 2)
                {
                    txt_resultado.Text= "No es primo";
                }
                else
                {
                    txt_resultado.Text = "Si es primo";
                }
            }
        }

        private void boton_añadir_Click(object sender, EventArgs e)
        {
            if (radio_boton_2.Checked == true)
            {
                list_box_pares_impares.Items.Clear();
                if (valores.Count == 20)
                {
                    MessageBox.Show("El arreglo esta lleno");
                    txt_numero.Text = "";
                }
                else
                {
                    num = int.Parse(txt_numero.Text);
                    valores.Add(num);
                    MessageBox.Show("Numero agregado");
                    txt_numero.Text = "";
                }
            }
            else
            {
                MessageBox.Show("Error debe de seleccionar la opción de Ejercicio 2");
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void boton_limpiar_Click(object sender, EventArgs e)
        {
            list_box_multiplos.Items.Clear();
            list_box_pares_impares.Items.Clear();
            txt_numero_primo.Text = "";
            txt_resultado.Text = "";
        }
    }
}
