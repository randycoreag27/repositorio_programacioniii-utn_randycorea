﻿
namespace Practica1
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.radio_boton_1 = new System.Windows.Forms.RadioButton();
            this.radio_boton_2 = new System.Windows.Forms.RadioButton();
            this.radio_boton_3 = new System.Windows.Forms.RadioButton();
            this.list_box_multiplos = new System.Windows.Forms.ListBox();
            this.boton_realizar = new System.Windows.Forms.Button();
            this.boton_limpiar = new System.Windows.Forms.Button();
            this.list_box_pares_impares = new System.Windows.Forms.ListBox();
            this.txt_numero = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.boton_añadir = new System.Windows.Forms.Button();
            this.txt_numero_primo = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txt_resultado = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // radio_boton_1
            // 
            this.radio_boton_1.AutoSize = true;
            this.radio_boton_1.Location = new System.Drawing.Point(28, 34);
            this.radio_boton_1.Name = "radio_boton_1";
            this.radio_boton_1.Size = new System.Drawing.Size(78, 19);
            this.radio_boton_1.TabIndex = 0;
            this.radio_boton_1.TabStop = true;
            this.radio_boton_1.Text = "Ejercicio 1";
            this.radio_boton_1.UseVisualStyleBackColor = true;
            this.radio_boton_1.CheckedChanged += new System.EventHandler(this.radio_boton_1_CheckedChanged);
            // 
            // radio_boton_2
            // 
            this.radio_boton_2.AutoSize = true;
            this.radio_boton_2.Location = new System.Drawing.Point(28, 134);
            this.radio_boton_2.Name = "radio_boton_2";
            this.radio_boton_2.Size = new System.Drawing.Size(78, 19);
            this.radio_boton_2.TabIndex = 1;
            this.radio_boton_2.TabStop = true;
            this.radio_boton_2.Text = "Ejercicio 2";
            this.radio_boton_2.UseVisualStyleBackColor = true;
            // 
            // radio_boton_3
            // 
            this.radio_boton_3.AutoSize = true;
            this.radio_boton_3.Location = new System.Drawing.Point(28, 236);
            this.radio_boton_3.Name = "radio_boton_3";
            this.radio_boton_3.Size = new System.Drawing.Size(78, 19);
            this.radio_boton_3.TabIndex = 2;
            this.radio_boton_3.TabStop = true;
            this.radio_boton_3.Text = "Ejercicio 3";
            this.radio_boton_3.UseVisualStyleBackColor = true;
            // 
            // list_box_multiplos
            // 
            this.list_box_multiplos.FormattingEnabled = true;
            this.list_box_multiplos.ItemHeight = 15;
            this.list_box_multiplos.Location = new System.Drawing.Point(28, 69);
            this.list_box_multiplos.Name = "list_box_multiplos";
            this.list_box_multiplos.Size = new System.Drawing.Size(332, 49);
            this.list_box_multiplos.TabIndex = 3;
            // 
            // boton_realizar
            // 
            this.boton_realizar.Location = new System.Drawing.Point(28, 319);
            this.boton_realizar.Name = "boton_realizar";
            this.boton_realizar.Size = new System.Drawing.Size(110, 23);
            this.boton_realizar.TabIndex = 4;
            this.boton_realizar.Text = "Realizar";
            this.boton_realizar.UseVisualStyleBackColor = true;
            this.boton_realizar.Click += new System.EventHandler(this.boton_realizar_Click);
            // 
            // boton_limpiar
            // 
            this.boton_limpiar.Location = new System.Drawing.Point(171, 319);
            this.boton_limpiar.Name = "boton_limpiar";
            this.boton_limpiar.Size = new System.Drawing.Size(112, 23);
            this.boton_limpiar.TabIndex = 5;
            this.boton_limpiar.Text = "Limpiar";
            this.boton_limpiar.UseVisualStyleBackColor = true;
            this.boton_limpiar.Click += new System.EventHandler(this.boton_limpiar_Click);
            // 
            // list_box_pares_impares
            // 
            this.list_box_pares_impares.FormattingEnabled = true;
            this.list_box_pares_impares.ItemHeight = 15;
            this.list_box_pares_impares.Location = new System.Drawing.Point(28, 172);
            this.list_box_pares_impares.Name = "list_box_pares_impares";
            this.list_box_pares_impares.Size = new System.Drawing.Size(332, 49);
            this.list_box_pares_impares.TabIndex = 6;
            // 
            // txt_numero
            // 
            this.txt_numero.Location = new System.Drawing.Point(386, 198);
            this.txt_numero.Name = "txt_numero";
            this.txt_numero.Size = new System.Drawing.Size(113, 23);
            this.txt_numero.TabIndex = 7;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(386, 172);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(113, 15);
            this.label1.TabIndex = 8;
            this.label1.Text = "Ingrese un número: ";
            // 
            // boton_añadir
            // 
            this.boton_añadir.Location = new System.Drawing.Point(506, 197);
            this.boton_añadir.Name = "boton_añadir";
            this.boton_añadir.Size = new System.Drawing.Size(75, 23);
            this.boton_añadir.TabIndex = 9;
            this.boton_añadir.Text = "Añadir";
            this.boton_añadir.UseVisualStyleBackColor = true;
            this.boton_añadir.Click += new System.EventHandler(this.boton_añadir_Click);
            // 
            // txt_numero_primo
            // 
            this.txt_numero_primo.Location = new System.Drawing.Point(28, 280);
            this.txt_numero_primo.Name = "txt_numero_primo";
            this.txt_numero_primo.Size = new System.Drawing.Size(110, 23);
            this.txt_numero_primo.TabIndex = 10;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(28, 262);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(255, 15);
            this.label2.TabIndex = 11;
            this.label2.Text = "Ingrese el numero que desea saber si es primo: ";
            // 
            // txt_resultado
            // 
            this.txt_resultado.Location = new System.Drawing.Point(171, 280);
            this.txt_resultado.Name = "txt_resultado";
            this.txt_resultado.Size = new System.Drawing.Size(112, 23);
            this.txt_resultado.TabIndex = 12;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(601, 361);
            this.Controls.Add(this.txt_resultado);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txt_numero_primo);
            this.Controls.Add(this.boton_añadir);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txt_numero);
            this.Controls.Add(this.list_box_pares_impares);
            this.Controls.Add(this.boton_limpiar);
            this.Controls.Add(this.boton_realizar);
            this.Controls.Add(this.list_box_multiplos);
            this.Controls.Add(this.radio_boton_3);
            this.Controls.Add(this.radio_boton_2);
            this.Controls.Add(this.radio_boton_1);
            this.Name = "Form1";
            this.Text = "Menú de Ejercicios";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RadioButton radio_boton_1;
        private System.Windows.Forms.RadioButton radio_boton_2;
        private System.Windows.Forms.RadioButton radio_boton_3;
        private System.Windows.Forms.ListBox list_box_multiplos;
        private System.Windows.Forms.Button boton_realizar;
        private System.Windows.Forms.Button boton_limpiar;
        private System.Windows.Forms.ListBox list_box_pares_impares;
        private System.Windows.Forms.TextBox txt_numero;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button boton_añadir;
        private System.Windows.Forms.TextBox txt_numero_primo;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txt_resultado;
    }
}

