﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Practica2
{
    public partial class From_Convertidor_Temperaturas : Form
    {
        public From_Convertidor_Temperaturas()
        {
            InitializeComponent();
        }

        private void boton_convertir_Click(object sender, EventArgs e)
        {
            double c, f;

            if (txt_faren.Text.Length==0)
            {
                c = double.Parse(txt_centi.Text);
                f = (c * 1.8) + 32;
                txt_faren.Text=f.ToString();
            }

            if(txt_centi.Text.Length == 0)
            {
                f = double.Parse(txt_faren.Text);
                c = (f - 32) / 1.8;
                txt_centi.Text = c.ToString();
            }

        }

        private void boton_limpiar_Click(object sender, EventArgs e)
        {
            txt_centi.Clear();
            txt_faren.Clear();
        }
    }
}
