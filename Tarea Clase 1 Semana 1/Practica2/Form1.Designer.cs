﻿
namespace Practica2
{
    partial class From_Convertidor_Temperaturas
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txt_centi = new System.Windows.Forms.TextBox();
            this.txt_faren = new System.Windows.Forms.TextBox();
            this.boton_convertir = new System.Windows.Forms.Button();
            this.boton_limpiar = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(19, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(172, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "Ingrese los grados Centigrados:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(18, 60);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(157, 15);
            this.label2.TabIndex = 1;
            this.label2.Text = "Ingrese los grados Farenheit:";
            // 
            // txt_centi
            // 
            this.txt_centi.Location = new System.Drawing.Point(21, 34);
            this.txt_centi.Name = "txt_centi";
            this.txt_centi.Size = new System.Drawing.Size(170, 23);
            this.txt_centi.TabIndex = 2;
            // 
            // txt_faren
            // 
            this.txt_faren.Location = new System.Drawing.Point(21, 78);
            this.txt_faren.Name = "txt_faren";
            this.txt_faren.Size = new System.Drawing.Size(170, 23);
            this.txt_faren.TabIndex = 3;
            // 
            // boton_convertir
            // 
            this.boton_convertir.Location = new System.Drawing.Point(213, 16);
            this.boton_convertir.Name = "boton_convertir";
            this.boton_convertir.Size = new System.Drawing.Size(101, 85);
            this.boton_convertir.TabIndex = 4;
            this.boton_convertir.Text = "Convertir";
            this.boton_convertir.UseVisualStyleBackColor = true;
            this.boton_convertir.Click += new System.EventHandler(this.boton_convertir_Click);
            // 
            // boton_limpiar
            // 
            this.boton_limpiar.Location = new System.Drawing.Point(321, 16);
            this.boton_limpiar.Name = "boton_limpiar";
            this.boton_limpiar.Size = new System.Drawing.Size(104, 85);
            this.boton_limpiar.TabIndex = 5;
            this.boton_limpiar.Text = "Limpiar";
            this.boton_limpiar.UseVisualStyleBackColor = true;
            this.boton_limpiar.Click += new System.EventHandler(this.boton_limpiar_Click);
            // 
            // From_Convertidor_Temperaturas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(443, 117);
            this.Controls.Add(this.boton_limpiar);
            this.Controls.Add(this.boton_convertir);
            this.Controls.Add(this.txt_faren);
            this.Controls.Add(this.txt_centi);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "From_Convertidor_Temperaturas";
            this.Text = "Convertidor de Temperaturas";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txt_centi;
        private System.Windows.Forms.TextBox txt_faren;
        private System.Windows.Forms.Button boton_convertir;
        private System.Windows.Forms.Button boton_limpiar;
    }
}

