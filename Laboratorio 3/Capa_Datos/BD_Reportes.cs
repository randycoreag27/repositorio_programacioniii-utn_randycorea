﻿using Npgsql;
using Objetos;
using System;
using System.Collections.Generic;

namespace Capa_Datos
{
    public class BD_Reportes
    {
        static NpgsqlConnection conexion;
        static NpgsqlCommand cmd;
        public List<Reporte> Reporte1()
        {
            List<Reporte> datos = new List<Reporte>();
            conexion = Conexion_BD.connection();
            conexion.Open();
            cmd = new NpgsqlCommand("SELECT COUNT(R.CODIGO_USUARIO), U.NOMBRE FROM RESERVA R" +
                " INNER JOIN USUARIO U ON R.CODIGO_USUARIO = U.CODIGO_USUARIO GROUP BY U.NOMBRE", conexion);
            NpgsqlDataReader rd = cmd.ExecuteReader();
            while (rd.Read())
            {
                Reporte re = new Reporte();
                re.cantidad_reservas = rd.GetInt32(0);
                re.nombre_usuario = rd.GetString(1);
                datos.Add(re);
            }
            conexion.Close();
            return datos;
        }
        public List<Reporte> Reporte2(Reporte r)
        {
            List<Reporte> datos = new List<Reporte>();
            conexion = Conexion_BD.connection();
            conexion.Open();
            cmd = new NpgsqlCommand("SELECT C.CEDULA, C.NOMBRE, R.FECHA_ENTRADA, R.FECHA_SALIDA, R.CANTIDAD_PERSONAS, " +
                "R.CANTIDAD_NOCHES, R.MONTO_TOTAL, U.NOMBRE  FROM RESERVA R INNER JOIN CLIENTE C " +
                "ON R.CODIGO_CLIENTE = C.CODIGO_CLIENTE INNER JOIN USUARIO U ON R.CODIGO_USUARIO = U.CODIGO_USUARIO " +
                "WHERE FECHA_ENTRADA >= " + "'" + r.fecha_entrada + "'" + " AND FECHA_SALIDA <= '" + r.fecha_salida + "'", conexion);
            NpgsqlDataReader rd = cmd.ExecuteReader();

            while (rd.Read())
            {
                Reporte re = new Reporte();
                re.cedula = rd.GetInt32(0);
                re.nombre_cliente = rd.GetString(1);
                re.fecha_entrada = rd.GetDateTime(2);
                re.fecha_salida = rd.GetDateTime(3);
                re.cantidad_personas = rd.GetInt32(4);
                re.cantidad_noches = rd.GetInt32(5);
                re.monto_total = Convert.ToInt32(rd.GetDouble(6));
                re.nombre_usuario = rd.GetString(7);
                datos.Add(re);
            }
            conexion.Close();
            return datos;
        }
    }
}
