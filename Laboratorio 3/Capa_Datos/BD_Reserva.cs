﻿using Npgsql;
using Objetos;
using System;
using System.Collections.Generic;


namespace Capa_Datos
{
    public class BD_Reserva
    {
        static NpgsqlConnection conexion;
        static NpgsqlCommand cmd;
        BD_Cliente bd_cliente = new BD_Cliente();
        public void Agregar_Reserva(Reserva r, Cliente c)
        {

            int cod_cliente = bd_cliente.Obtener_Codigo_Cliente(c);

            conexion = Conexion_BD.connection();
            conexion.Open();

            cmd = new NpgsqlCommand("INSERT INTO RESERVA (CODIGO_USUARIO, CODIGO_CLIENTE, FECHA_ENTRADA, FECHA_SALIDA, " +
                "CANTIDAD_NOCHES, CANTIDAD_PERSONAS, TARIFA, MONTO_INICIAL, MONTO_IMPUESTO, MONTO_TOTAL) VALUES " +
                "('" + r.codigo_usuario + "', '" + cod_cliente + "', '" + r.fecha_entrada + "', '" + r.fecha_salida +
                "', '" + r.cantidad_noches + "', '" + r.cantidad_personas + "', '" + r.tarifa + "', '" + 
                r.monto_inicial + "', '"  + r.monto_impuesto + "', '" + r.monto_final + "')", conexion);

            cmd.ExecuteNonQuery();
            conexion.Close();
        }
        public void Modificar_Reserva(Reserva r)
        {
            conexion = Conexion_BD.connection();
            conexion.Open();
            cmd = new NpgsqlCommand("UPDATE RESERVA SET FECHA_ENTRADA = '" + r.fecha_entrada + "', " + "FECHA_SALIDA = '" + r.fecha_salida +
                "', " + "CANTIDAD_NOCHES = '" + r.cantidad_noches + "', " + "CANTIDAD_PERSONAS = '" + r.cantidad_personas + "', " + "TARIFA = '" 
                + r.tarifa + "', " + "MONTO_INICIAL = '" + r.monto_inicial + "', " + "MONTO_IMPUESTO = '" + r.monto_impuesto + "', " 
                + "MONTO_TOTAL = '" + r.monto_final + "' WHERE CODIGO_RESERVA = '" + r.codigo_reserva + "'", conexion);
            cmd.ExecuteNonQuery();
            conexion.Close();
        }
        public void Eliminar_Reserva(Reserva r)
        {
            conexion = Conexion_BD.connection();
            conexion.Open();
            cmd = new NpgsqlCommand("DELETE FROM RESERVA WHERE CODIGO_RESERVA = '" + r.codigo_reserva + "'", conexion);
            cmd.ExecuteNonQuery();
            conexion.Close();
        }
        public int Consulta_Disponibilidad(DateTime fecha)
        {
            int respuesta = 0;
            conexion = Conexion_BD.connection();
            conexion.Open();
            cmd = new NpgsqlCommand("SELECT COUNT(CODIGO_RESERVA) FROM RESERVA GROUP BY FECHA_ENTRADA HAVING FECHA_ENTRADA = '" + fecha + "'", conexion);
            NpgsqlDataReader rd = cmd.ExecuteReader();

            while (rd.Read())
            {
                respuesta = rd.GetInt32(0);
            }

            conexion.Close();
            return respuesta;
        }
        public List<Reserva> Obtener_Reservas()
        {
            List<Reserva> reservas = new List<Reserva>();

            conexion = Conexion_BD.connection();
            conexion.Open();
            cmd = new NpgsqlCommand("SELECT CODIGO_RESERVA, FECHA_ENTRADA, FECHA_SALIDA, CANTIDAD_NOCHES, CANTIDAD_PERSONAS, " +
                "TARIFA, MONTO_INICIAL, MONTO_IMPUESTO, MONTO_TOTAL FROM RESERVA", conexion);
            NpgsqlDataReader rd = cmd.ExecuteReader();

            while (rd.Read())
            {
                Reserva r = new Reserva();
                r.codigo_reserva = rd.GetInt32(0);
                r.fecha_entrada = rd.GetDateTime(1);
                r.fecha_salida = rd.GetDateTime(2);
                r.cantidad_noches = rd.GetInt32(3);
                r.cantidad_personas = rd.GetInt32(4);
                r.tarifa = Convert.ToInt32(rd.GetDouble(5));
                r.monto_inicial = Convert.ToInt32(rd.GetDouble(6));
                r.monto_impuesto = Convert.ToInt32(rd.GetDouble(7));
                r.monto_final = Convert.ToInt32(rd.GetDouble(8));
                reservas.Add(r);
            }
            conexion.Close();
            return reservas;
        }
        public List<Reporte> Facturacion()
        {
            List<Reporte> datos = new List<Reporte>();
            conexion = Conexion_BD.connection();
            conexion.Open();
            cmd = new NpgsqlCommand("SELECT R.CODIGO_RESERVA, C.CEDULA, C.NOMBRE, R.FECHA_ENTRADA, R.FECHA_SALIDA, R.TARIFA, " +
                "R.CANTIDAD_NOCHES, R.MONTO_TOTAL, U.NOMBRE  FROM RESERVA R INNER JOIN CLIENTE C " +
                "ON R.CODIGO_CLIENTE = C.CODIGO_CLIENTE INNER JOIN USUARIO U ON R.CODIGO_USUARIO = U.CODIGO_USUARIO ", conexion);
            NpgsqlDataReader rd = cmd.ExecuteReader();

            while (rd.Read())
            {
                Reporte re = new Reporte();
                re.codigo_reserva = rd.GetInt32(0);
                re.cedula = rd.GetInt32(1);
                re.nombre_cliente = rd.GetString(2);
                re.fecha_entrada = rd.GetDateTime(3);
                re.fecha_salida = rd.GetDateTime(4);
                re.cantidad_personas = Convert.ToInt32(rd.GetDouble(5));
                re.cantidad_noches = rd.GetInt32(6);
                re.monto_total = Convert.ToInt32(rd.GetDouble(7));
                re.nombre_usuario = rd.GetString(8);
                datos.Add(re);
            }
            conexion.Close();
            return datos;
        }
    }
}
