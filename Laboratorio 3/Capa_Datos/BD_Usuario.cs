﻿using Npgsql;
using Objetos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Capa_Datos
{
    public class BD_Usuario
    {
        static NpgsqlConnection conexion;
        static NpgsqlCommand cmd;

        public List<Usuario> Obtener_Usuarios()
        {
            List<Usuario> usuarios = new List<Usuario>();

            conexion = Conexion_BD.connection();
            conexion.Open();
            cmd = new NpgsqlCommand("SELECT * FROM USUARIO", conexion);
            NpgsqlDataReader rd = cmd.ExecuteReader();

            while (rd.Read())
            {
                Usuario u = new Usuario();
                u.codigo_usuario = rd.GetString(0);
                u.cedula_usuario = rd.GetInt32(1);
                u.nombre_usuario = rd.GetString(2);
                usuarios.Add(u);
            }
            conexion.Close();
            return usuarios;
        }

    }
}
