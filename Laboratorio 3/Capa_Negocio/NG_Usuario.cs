﻿using Capa_Datos;
using Objetos;
using System.Collections.Generic;


namespace Capa_Negocio
{
    public class NG_Usuario
    {
        public List<Usuario> Obtener_Reservas()
        {
            return new BD_Usuario().Obtener_Usuarios();
        }
    }
}
