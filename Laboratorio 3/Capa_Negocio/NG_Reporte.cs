﻿using Capa_Datos;
using Objetos;
using System.Collections.Generic;

namespace Capa_Negocio
{
    public class NG_Reporte
    {
        public List<Reporte> Reporte1()
        {
            return new BD_Reportes().Reporte1();
        }
        public List<Reporte> Reporte2(Reporte r)
        {
            return new BD_Reportes().Reporte2(r);
        }
    }
}
