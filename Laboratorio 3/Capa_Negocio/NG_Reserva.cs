﻿using Capa_Datos;
using Objetos;
using System;
using System.Collections.Generic;


namespace Capa_Negocio
{
    public class NG_Reserva
    {
        BD_Reserva bd_reserva = new BD_Reserva();
        public void Agregar_Reserva(Reserva r, Cliente c)
        {
            bd_reserva.Agregar_Reserva(r, c);
        }
        public void Modificar_Reserva(Reserva r)
        {
            bd_reserva.Modificar_Reserva(r);
        }
        public void Eliminar_Reserva(Reserva r)
        {
            bd_reserva.Eliminar_Reserva(r);
        }
        public int Consulta_Disponibilidad(DateTime f)
        {
            return new BD_Reserva().Consulta_Disponibilidad(f);
        }
        public List<Reserva> Obtener_Reservas()
        {
            return new BD_Reserva().Obtener_Reservas();
        }
        public List<Reporte> Facturacion()
        {
            return new BD_Reserva().Facturacion();
        }

    }
}
