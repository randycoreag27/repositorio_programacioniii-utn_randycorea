﻿
namespace Objetos
{
    public class Cliente
    {
        public int codigo_cliente { get; set; }
        public string nombre_cliente { get; set; }
        public int cedula_cliente { get; set; }
        public string genero { get; set; }
        public int telefono_fijo { get; set; }
        public int telefono_movil { get; set; }
        public string correo_electronico { get; set; }
    }
}
