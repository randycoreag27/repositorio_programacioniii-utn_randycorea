﻿using System;

namespace Objetos
{
    public class Reserva
    {
        public int codigo_reserva { get; set; }
        public string codigo_usuario  { get; set; }
        public int codigo_cliente { get; set; }
        public DateTime fecha_entrada { get; set; }
        public DateTime fecha_salida { get; set; }
        public int cantidad_noches { get; set; }
        public int cantidad_personas { get; set; }
        public int tarifa { get; set; }
        public int monto_inicial { get; set; }
        public int monto_impuesto { get; set; }
        public int monto_final { get; set; }
    }
}
