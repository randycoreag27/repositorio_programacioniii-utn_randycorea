﻿using System;

namespace Objetos
{
    public class Reporte
    {
        public int codigo_reserva { get; set; }
        public int cedula { get; set; }
        public string nombre_cliente { get; set; }
        public DateTime fecha_entrada { get; set; }
        public DateTime fecha_salida { get; set; }
        public int cantidad_noches { get; set; }
        public int cantidad_personas { get; set; }
        public int monto_total { get; set; }
        public string nombre_usuario { get; set; }
        public int cantidad_reservas { get; set; }
    }
}
