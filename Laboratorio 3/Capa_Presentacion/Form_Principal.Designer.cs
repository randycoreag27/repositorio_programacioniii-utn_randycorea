﻿
namespace Capa_Presentacion
{
    partial class Form_Principal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form_Principal));
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panel_barra_menu = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btn_tipo_cambio = new System.Windows.Forms.PictureBox();
            this.btn_facturas = new System.Windows.Forms.PictureBox();
            this.btn_reportes = new System.Windows.Forms.PictureBox();
            this.btn_tarifas = new System.Windows.Forms.PictureBox();
            this.btn_salir = new System.Windows.Forms.PictureBox();
            this.btn_reservas = new System.Windows.Forms.PictureBox();
            this.panel_principal = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label_hora = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label_fecha = new System.Windows.Forms.Label();
            this.timer_fecha_hora = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel_barra_menu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btn_tipo_cambio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_facturas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_reportes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_tarifas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_salir)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_reservas)).BeginInit();
            this.panel_principal.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(3, -4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(151, 109);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 2;
            this.pictureBox1.TabStop = false;
            // 
            // panel_barra_menu
            // 
            this.panel_barra_menu.BackColor = System.Drawing.Color.ForestGreen;
            this.panel_barra_menu.Controls.Add(this.label5);
            this.panel_barra_menu.Controls.Add(this.label4);
            this.panel_barra_menu.Controls.Add(this.label3);
            this.panel_barra_menu.Controls.Add(this.label2);
            this.panel_barra_menu.Controls.Add(this.label1);
            this.panel_barra_menu.Controls.Add(this.btn_tipo_cambio);
            this.panel_barra_menu.Controls.Add(this.btn_facturas);
            this.panel_barra_menu.Controls.Add(this.btn_reportes);
            this.panel_barra_menu.Controls.Add(this.btn_tarifas);
            this.panel_barra_menu.Controls.Add(this.btn_salir);
            this.panel_barra_menu.Controls.Add(this.btn_reservas);
            this.panel_barra_menu.Location = new System.Drawing.Point(156, 0);
            this.panel_barra_menu.Name = "panel_barra_menu";
            this.panel_barra_menu.Size = new System.Drawing.Size(861, 105);
            this.panel_barra_menu.TabIndex = 4;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Arial Rounded MT Bold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(641, 79);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(106, 18);
            this.label5.TabIndex = 13;
            this.label5.Text = "Tipo Cambio";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial Rounded MT Bold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(504, 79);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(82, 18);
            this.label4.TabIndex = 11;
            this.label4.Text = "Reportes";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial Rounded MT Bold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(352, 79);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(80, 18);
            this.label3.TabIndex = 12;
            this.label3.Text = "Facturas";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial Rounded MT Bold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(206, 79);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(63, 18);
            this.label2.TabIndex = 11;
            this.label2.Text = "Tarifas";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial Rounded MT Bold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(43, 79);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(84, 18);
            this.label1.TabIndex = 10;
            this.label1.Text = "Reservas";
            // 
            // btn_tipo_cambio
            // 
            this.btn_tipo_cambio.Image = ((System.Drawing.Image)(resources.GetObject("btn_tipo_cambio.Image")));
            this.btn_tipo_cambio.Location = new System.Drawing.Point(644, 7);
            this.btn_tipo_cambio.Name = "btn_tipo_cambio";
            this.btn_tipo_cambio.Size = new System.Drawing.Size(97, 67);
            this.btn_tipo_cambio.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btn_tipo_cambio.TabIndex = 9;
            this.btn_tipo_cambio.TabStop = false;
            this.btn_tipo_cambio.Click += new System.EventHandler(this.btn_tipo_cambio_Click);
            // 
            // btn_facturas
            // 
            this.btn_facturas.Image = ((System.Drawing.Image)(resources.GetObject("btn_facturas.Image")));
            this.btn_facturas.Location = new System.Drawing.Point(345, 7);
            this.btn_facturas.Name = "btn_facturas";
            this.btn_facturas.Size = new System.Drawing.Size(97, 67);
            this.btn_facturas.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btn_facturas.TabIndex = 8;
            this.btn_facturas.TabStop = false;
            this.btn_facturas.Click += new System.EventHandler(this.btn_facturas_Click);
            // 
            // btn_reportes
            // 
            this.btn_reportes.Image = ((System.Drawing.Image)(resources.GetObject("btn_reportes.Image")));
            this.btn_reportes.Location = new System.Drawing.Point(496, 7);
            this.btn_reportes.Name = "btn_reportes";
            this.btn_reportes.Size = new System.Drawing.Size(97, 67);
            this.btn_reportes.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btn_reportes.TabIndex = 6;
            this.btn_reportes.TabStop = false;
            this.btn_reportes.Click += new System.EventHandler(this.btn_reportes_Click);
            // 
            // btn_tarifas
            // 
            this.btn_tarifas.Image = ((System.Drawing.Image)(resources.GetObject("btn_tarifas.Image")));
            this.btn_tarifas.Location = new System.Drawing.Point(190, 7);
            this.btn_tarifas.Name = "btn_tarifas";
            this.btn_tarifas.Size = new System.Drawing.Size(97, 67);
            this.btn_tarifas.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btn_tarifas.TabIndex = 7;
            this.btn_tarifas.TabStop = false;
            this.btn_tarifas.Click += new System.EventHandler(this.btn_tarifas_Click);
            // 
            // btn_salir
            // 
            this.btn_salir.Image = ((System.Drawing.Image)(resources.GetObject("btn_salir.Image")));
            this.btn_salir.Location = new System.Drawing.Point(813, 12);
            this.btn_salir.Name = "btn_salir";
            this.btn_salir.Size = new System.Drawing.Size(33, 23);
            this.btn_salir.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btn_salir.TabIndex = 3;
            this.btn_salir.TabStop = false;
            this.btn_salir.Click += new System.EventHandler(this.btn_salir_Click_1);
            // 
            // btn_reservas
            // 
            this.btn_reservas.Image = ((System.Drawing.Image)(resources.GetObject("btn_reservas.Image")));
            this.btn_reservas.Location = new System.Drawing.Point(35, 7);
            this.btn_reservas.Name = "btn_reservas";
            this.btn_reservas.Size = new System.Drawing.Size(97, 67);
            this.btn_reservas.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btn_reservas.TabIndex = 5;
            this.btn_reservas.TabStop = false;
            this.btn_reservas.Click += new System.EventHandler(this.btn_reservas_Click);
            // 
            // panel_principal
            // 
            this.panel_principal.Controls.Add(this.panel2);
            this.panel_principal.Controls.Add(this.panel1);
            this.panel_principal.Location = new System.Drawing.Point(-12, 111);
            this.panel_principal.Name = "panel_principal";
            this.panel_principal.Size = new System.Drawing.Size(1029, 563);
            this.panel_principal.TabIndex = 5;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.ForestGreen;
            this.panel2.Controls.Add(this.label_hora);
            this.panel2.Location = new System.Drawing.Point(26, 532);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(93, 21);
            this.panel2.TabIndex = 17;
            // 
            // label_hora
            // 
            this.label_hora.AutoSize = true;
            this.label_hora.BackColor = System.Drawing.Color.Transparent;
            this.label_hora.Font = new System.Drawing.Font("Arial Rounded MT Bold", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_hora.ForeColor = System.Drawing.Color.White;
            this.label_hora.Location = new System.Drawing.Point(2, -1);
            this.label_hora.Name = "label_hora";
            this.label_hora.Size = new System.Drawing.Size(88, 22);
            this.label_hora.TabIndex = 15;
            this.label_hora.Text = "19:32:01";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.ForestGreen;
            this.panel1.Controls.Add(this.label_fecha);
            this.panel1.Location = new System.Drawing.Point(697, 531);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(319, 22);
            this.panel1.TabIndex = 16;
            // 
            // label_fecha
            // 
            this.label_fecha.AutoSize = true;
            this.label_fecha.BackColor = System.Drawing.Color.Transparent;
            this.label_fecha.Font = new System.Drawing.Font("Arial Rounded MT Bold", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_fecha.ForeColor = System.Drawing.Color.White;
            this.label_fecha.Location = new System.Drawing.Point(3, 0);
            this.label_fecha.Name = "label_fecha";
            this.label_fecha.Size = new System.Drawing.Size(304, 22);
            this.label_fecha.TabIndex = 14;
            this.label_fecha.Text = "miércoles 17 de noviembre 2021";
            // 
            // timer_fecha_hora
            // 
            this.timer_fecha_hora.Enabled = true;
            this.timer_fecha_hora.Tick += new System.EventHandler(this.timer_fecha_hora_Tick);
            // 
            // Form_Principal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1014, 671);
            this.Controls.Add(this.panel_principal);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.panel_barra_menu);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form_Principal";
            this.Opacity = 0.9D;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form_Principal";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel_barra_menu.ResumeLayout(false);
            this.panel_barra_menu.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btn_tipo_cambio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_facturas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_reportes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_tarifas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_salir)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_reservas)).EndInit();
            this.panel_principal.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel panel_barra_menu;
        private System.Windows.Forms.PictureBox btn_salir;
        private System.Windows.Forms.PictureBox btn_facturas;
        private System.Windows.Forms.PictureBox btn_reportes;
        private System.Windows.Forms.PictureBox btn_tarifas;
        private System.Windows.Forms.PictureBox btn_reservas;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox btn_tipo_cambio;
        private System.Windows.Forms.Panel panel_principal;
        private System.Windows.Forms.Label label_fecha;
        private System.Windows.Forms.Timer timer_fecha_hora;
        private System.Windows.Forms.Label label_hora;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
    }
}