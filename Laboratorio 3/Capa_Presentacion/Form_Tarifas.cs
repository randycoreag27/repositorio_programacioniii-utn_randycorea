﻿using Capa_Negocio;
using Objetos;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Capa_Presentacion
{
    public partial class Form_Tarifas : Form
    {
        Tarifa tarifa = new Tarifa();
        NG_Tarifa ng_tarifa = new NG_Tarifa();
        List<Tarifa> lista_tarifas = new List<Tarifa>();
        public Form_Tarifas()
        {
            InitializeComponent();
            Mostrar_Datos();
        }
        private void btn_salir_Click(object sender, EventArgs e)
        {
            this.Hide();
        }
        public void Obtener_Datos(DataGridViewCellEventArgs e)
        {
            DataGridViewRow row = dgv_tarifas.Rows[e.RowIndex];
            tarifa.nombre_tarifa = row.Cells["column_nombre_tarifa"].Value.ToString();
            tarifa.monto_tarifa = Convert.ToDouble(row.Cells["column_monto_tarifa"].Value.ToString());
        }
        public void Obtener_Datos_2(DataGridViewCellEventArgs e)
        {
            DataGridViewRow row = dgv_tarifas.Rows[e.RowIndex];
            tarifa.codigo_tarifa = Convert.ToInt32(row.Cells["column_codigo_tarifa"].Value.ToString());
            tarifa.nombre_tarifa = row.Cells["column_nombre_tarifa"].Value.ToString();
            tarifa.monto_tarifa = Convert.ToDouble(row.Cells["column_monto_tarifa"].Value.ToString());
        }
        public void Mostrar_Datos()
        {
            lista_tarifas = ng_tarifa.Obtener_Tarifas();
            for (int i = 0; i < lista_tarifas.Count; i++)
            {
                DataGridViewRow fila = new DataGridViewRow();
                fila.CreateCells(dgv_tarifas);
                fila.Cells[0].Value = lista_tarifas[i].codigo_tarifa;
                fila.Cells[1].Value = lista_tarifas[i].nombre_tarifa;
                fila.Cells[2].Value = lista_tarifas[i].monto_tarifa;
                dgv_tarifas.Rows.Add(fila);
            }
        }
        private void dtg_tarifas_CellPainting(object sender, DataGridViewCellPaintingEventArgs e)
        {
            if (e.ColumnIndex >= 0 && this.dgv_tarifas.Columns[e.ColumnIndex].Name == "Agregar" && e.RowIndex >= 0)
            {
                e.Paint(e.CellBounds, DataGridViewPaintParts.All);

                DataGridViewButtonCell btn_agregar = this.dgv_tarifas.Rows[e.RowIndex].Cells["Agregar"] as DataGridViewButtonCell;
                Icon agregar = new Icon(Environment.CurrentDirectory + @"\icono_agregar.ico");
                e.Graphics.DrawIcon(agregar, e.CellBounds.Left + 3, e.CellBounds.Top + 3);

                this.dgv_tarifas.Rows[e.RowIndex].Height = agregar.Height + 10;
                this.dgv_tarifas.Columns[e.ColumnIndex].Width = agregar.Width + 10;

                e.Handled = true;
            }

            if (e.ColumnIndex >= 0 && this.dgv_tarifas.Columns[e.ColumnIndex].Name == "Modificar" && e.RowIndex >= 0)
            {
                e.Paint(e.CellBounds, DataGridViewPaintParts.All);

                DataGridViewButtonCell btn_modificar = this.dgv_tarifas.Rows[e.RowIndex].Cells["Modificar"] as DataGridViewButtonCell;
                Icon modificar = new Icon(Environment.CurrentDirectory + @"\icono_modificar.ico");
                e.Graphics.DrawIcon(modificar, e.CellBounds.Left + 3, e.CellBounds.Top + 3);


                this.dgv_tarifas.Rows[e.RowIndex].Height = modificar.Height + 10;
                this.dgv_tarifas.Columns[e.ColumnIndex].Width = modificar.Width + 10;

                e.Handled = true;
            }

            if (e.ColumnIndex >= 0 && this.dgv_tarifas.Columns[e.ColumnIndex].Name == "Eliminar" && e.RowIndex >= 0)
            {
                e.Paint(e.CellBounds, DataGridViewPaintParts.All);

                DataGridViewButtonCell btn_eliminar = this.dgv_tarifas.Rows[e.RowIndex].Cells["Eliminar"] as DataGridViewButtonCell;
                Icon eliminar = new Icon(Environment.CurrentDirectory + @"\icono_eliminar.ico");
                e.Graphics.DrawIcon(eliminar, e.CellBounds.Left + 3, e.CellBounds.Top + 3);

                this.dgv_tarifas.Rows[e.RowIndex].Height = eliminar.Height + 10;
                this.dgv_tarifas.Columns[e.ColumnIndex].Width = eliminar.Width + 10;

                e.Handled = true;
            }
        }
        private void dtg_tarifas_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 3)
            {
                Obtener_Datos(e);
                ng_tarifa.Agregar_Tarifa(tarifa);
                dgv_tarifas.Rows.Clear();
                Mostrar_Datos();
                MessageBox.Show("Datos Agregados Correctamente", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            if (e.ColumnIndex == 4)
            {
                Obtener_Datos_2(e);
                ng_tarifa.Modificar_Tarifa(tarifa);
                dgv_tarifas.Rows.Clear();
                Mostrar_Datos();
                MessageBox.Show("Datos Modificados Correctamente", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            if (e.ColumnIndex == 5)
            {
                Obtener_Datos_2(e);
                ng_tarifa.Eliminar_Tarifa(tarifa);
                dgv_tarifas.Rows.Clear();
                Mostrar_Datos();
                MessageBox.Show("Datos Eliminados Correctamente", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void Form_Tarifas_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.Hide();
        }
    }
}
