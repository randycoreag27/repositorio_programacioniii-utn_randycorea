﻿using Capa_Negocio;
using System;
using System.Windows.Forms;

namespace Capa_Presentacion
{
    public partial class Form_Inicio : Form
    {

        NG_Usuario ng_usuario = new NG_Usuario();
        public static string codigo_usuario = "";

        public Form_Inicio()
        {
            InitializeComponent();
            Cargar_Combo_Usuarios();
        }

        public void Cargar_Combo_Usuarios()
        {
            cbx_usuarios.DataSource = ng_usuario.Obtener_Reservas();
            cbx_usuarios.DisplayMember = "nombre_usuario";
            cbx_usuarios.ValueMember = "codigo_usuario";
        }

        private void btn_salir_Click(object sender, EventArgs e)
        {
            DialogResult respuesta = MessageBox.Show("¿Desea salir de la Aplicación?", "Advertencia", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);

            if (respuesta == DialogResult.Yes)
            {
                Application.Exit();
            }
        }

        private void btn_acceder_Click(object sender, EventArgs e)
        {
            codigo_usuario = Convert.ToString(cbx_usuarios.SelectedValue);
            Form_Principal frm_pri = new Form_Principal();
            frm_pri.Show();
            this.Hide();
        }
    }
}
