﻿
namespace Capa_Presentacion
{
    partial class Form_Gestion_Reservas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form_Gestion_Reservas));
            this.dgv_reservas = new System.Windows.Forms.DataGridView();
            this.column_codigo_reserva = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.column_fecha_entrada = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.column_fecha_salida = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.column_cantidad_noches = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.column_cantidad_personas = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.column_tarifa = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.column_monto_inicial = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.column_impuesto = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.column_monto_final = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Modificar = new System.Windows.Forms.DataGridViewButtonColumn();
            this.Eliminar = new System.Windows.Forms.DataGridViewButtonColumn();
            this.panel_principal = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.img = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_reservas)).BeginInit();
            this.panel_principal.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.img)).BeginInit();
            this.SuspendLayout();
            // 
            // dgv_reservas
            // 
            this.dgv_reservas.AllowUserToAddRows = false;
            this.dgv_reservas.AllowUserToDeleteRows = false;
            this.dgv_reservas.BackgroundColor = System.Drawing.Color.White;
            this.dgv_reservas.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgv_reservas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_reservas.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.column_codigo_reserva,
            this.column_fecha_entrada,
            this.column_fecha_salida,
            this.column_cantidad_noches,
            this.column_cantidad_personas,
            this.column_tarifa,
            this.column_monto_inicial,
            this.column_impuesto,
            this.column_monto_final,
            this.Modificar,
            this.Eliminar});
            this.dgv_reservas.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv_reservas.Location = new System.Drawing.Point(0, 69);
            this.dgv_reservas.Name = "dgv_reservas";
            this.dgv_reservas.RowHeadersVisible = false;
            this.dgv_reservas.Size = new System.Drawing.Size(852, 178);
            this.dgv_reservas.TabIndex = 11;
            this.dgv_reservas.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_reservas_CellClick);
            this.dgv_reservas.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_tarifas_CellContentClick);
            this.dgv_reservas.CellPainting += new System.Windows.Forms.DataGridViewCellPaintingEventHandler(this.dgv_reservas_CellPainting);
            this.dgv_reservas.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dgv_reservas_DataError);
            this.dgv_reservas.Scroll += new System.Windows.Forms.ScrollEventHandler(this.dgv_reservas_Scroll);
            // 
            // column_codigo_reserva
            // 
            this.column_codigo_reserva.Frozen = true;
            this.column_codigo_reserva.HeaderText = "Codigo Reserva";
            this.column_codigo_reserva.Name = "column_codigo_reserva";
            this.column_codigo_reserva.ReadOnly = true;
            this.column_codigo_reserva.Width = 99;
            // 
            // column_fecha_entrada
            // 
            this.column_fecha_entrada.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.column_fecha_entrada.Frozen = true;
            this.column_fecha_entrada.HeaderText = "Fecha Entrada";
            this.column_fecha_entrada.Name = "column_fecha_entrada";
            this.column_fecha_entrada.ReadOnly = true;
            this.column_fecha_entrada.Width = 94;
            // 
            // column_fecha_salida
            // 
            this.column_fecha_salida.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.column_fecha_salida.Frozen = true;
            this.column_fecha_salida.HeaderText = "Fecha Salida";
            this.column_fecha_salida.Name = "column_fecha_salida";
            this.column_fecha_salida.ReadOnly = true;
            this.column_fecha_salida.Width = 87;
            // 
            // column_cantidad_noches
            // 
            this.column_cantidad_noches.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.column_cantidad_noches.Frozen = true;
            this.column_cantidad_noches.HeaderText = "Cantidad Noches";
            this.column_cantidad_noches.Name = "column_cantidad_noches";
            this.column_cantidad_noches.ReadOnly = true;
            this.column_cantidad_noches.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.column_cantidad_noches.Width = 105;
            // 
            // column_cantidad_personas
            // 
            this.column_cantidad_personas.Frozen = true;
            this.column_cantidad_personas.HeaderText = "Cantidad de Personas";
            this.column_cantidad_personas.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8"});
            this.column_cantidad_personas.Name = "column_cantidad_personas";
            // 
            // column_tarifa
            // 
            this.column_tarifa.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.column_tarifa.Frozen = true;
            this.column_tarifa.HeaderText = "Tarifa";
            this.column_tarifa.Name = "column_tarifa";
            this.column_tarifa.Width = 80;
            // 
            // column_monto_inicial
            // 
            this.column_monto_inicial.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.column_monto_inicial.Frozen = true;
            this.column_monto_inicial.HeaderText = "Monto Inicial";
            this.column_monto_inicial.Name = "column_monto_inicial";
            this.column_monto_inicial.ReadOnly = true;
            this.column_monto_inicial.Width = 85;
            // 
            // column_impuesto
            // 
            this.column_impuesto.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.column_impuesto.Frozen = true;
            this.column_impuesto.HeaderText = "Impuesto";
            this.column_impuesto.Name = "column_impuesto";
            this.column_impuesto.ReadOnly = true;
            this.column_impuesto.Width = 75;
            // 
            // column_monto_final
            // 
            this.column_monto_final.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.column_monto_final.Frozen = true;
            this.column_monto_final.HeaderText = "Total";
            this.column_monto_final.Name = "column_monto_final";
            this.column_monto_final.ReadOnly = true;
            this.column_monto_final.Width = 56;
            // 
            // Modificar
            // 
            this.Modificar.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.Modificar.Frozen = true;
            this.Modificar.HeaderText = "";
            this.Modificar.Name = "Modificar";
            this.Modificar.Width = 36;
            // 
            // Eliminar
            // 
            this.Eliminar.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.Eliminar.Frozen = true;
            this.Eliminar.HeaderText = "";
            this.Eliminar.Name = "Eliminar";
            this.Eliminar.Width = 36;
            // 
            // panel_principal
            // 
            this.panel_principal.BackColor = System.Drawing.Color.ForestGreen;
            this.panel_principal.Controls.Add(this.label1);
            this.panel_principal.Controls.Add(this.img);
            this.panel_principal.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel_principal.Location = new System.Drawing.Point(0, 0);
            this.panel_principal.Name = "panel_principal";
            this.panel_principal.Size = new System.Drawing.Size(852, 69);
            this.panel_principal.TabIndex = 10;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial Rounded MT Bold", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(318, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(287, 32);
            this.label1.TabIndex = 11;
            this.label1.Text = "Gestión de Reservas";
            // 
            // img
            // 
            this.img.BackColor = System.Drawing.Color.Transparent;
            this.img.Image = ((System.Drawing.Image)(resources.GetObject("img.Image")));
            this.img.Location = new System.Drawing.Point(241, 11);
            this.img.Name = "img";
            this.img.Size = new System.Drawing.Size(86, 49);
            this.img.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.img.TabIndex = 12;
            this.img.TabStop = false;
            // 
            // Form_Gestion_Reservas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(852, 247);
            this.Controls.Add(this.dgv_reservas);
            this.Controls.Add(this.panel_principal);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Location = new System.Drawing.Point(100, 75);
            this.Name = "Form_Gestion_Reservas";
            this.Opacity = 0.9D;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            ((System.ComponentModel.ISupportInitialize)(this.dgv_reservas)).EndInit();
            this.panel_principal.ResumeLayout(false);
            this.panel_principal.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.img)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgv_reservas;
        private System.Windows.Forms.Panel panel_principal;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox img;
        private System.Windows.Forms.DataGridViewTextBoxColumn column_codigo_reserva;
        private System.Windows.Forms.DataGridViewTextBoxColumn column_fecha_entrada;
        private System.Windows.Forms.DataGridViewTextBoxColumn column_fecha_salida;
        private System.Windows.Forms.DataGridViewTextBoxColumn column_cantidad_noches;
        private System.Windows.Forms.DataGridViewComboBoxColumn column_cantidad_personas;
        private System.Windows.Forms.DataGridViewComboBoxColumn column_tarifa;
        private System.Windows.Forms.DataGridViewTextBoxColumn column_monto_inicial;
        private System.Windows.Forms.DataGridViewTextBoxColumn column_impuesto;
        private System.Windows.Forms.DataGridViewTextBoxColumn column_monto_final;
        private System.Windows.Forms.DataGridViewButtonColumn Modificar;
        private System.Windows.Forms.DataGridViewButtonColumn Eliminar;
    }
}