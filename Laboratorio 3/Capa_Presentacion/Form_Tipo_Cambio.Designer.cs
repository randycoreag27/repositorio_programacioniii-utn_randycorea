﻿
namespace Capa_Presentacion
{
    partial class Form_Tipo_Cambio
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form_Tipo_Cambio));
            this.label5 = new System.Windows.Forms.Label();
            this.btn_tipo_cambio = new System.Windows.Forms.PictureBox();
            this.txt_dolar = new System.Windows.Forms.TextBox();
            this.txt_colones = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label_tipo_cambio = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.btn_tipo_cambio)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Arial Rounded MT Bold", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(83, 26);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(139, 24);
            this.label5.TabIndex = 16;
            this.label5.Text = "Tipo Cambio";
            // 
            // btn_tipo_cambio
            // 
            this.btn_tipo_cambio.Image = ((System.Drawing.Image)(resources.GetObject("btn_tipo_cambio.Image")));
            this.btn_tipo_cambio.Location = new System.Drawing.Point(27, 12);
            this.btn_tipo_cambio.Name = "btn_tipo_cambio";
            this.btn_tipo_cambio.Size = new System.Drawing.Size(50, 51);
            this.btn_tipo_cambio.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btn_tipo_cambio.TabIndex = 15;
            this.btn_tipo_cambio.TabStop = false;
            // 
            // txt_dolar
            // 
            this.txt_dolar.BackColor = System.Drawing.Color.White;
            this.txt_dolar.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_dolar.Font = new System.Drawing.Font("Arial Rounded MT Bold", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_dolar.ForeColor = System.Drawing.Color.Black;
            this.txt_dolar.Location = new System.Drawing.Point(49, 102);
            this.txt_dolar.Name = "txt_dolar";
            this.txt_dolar.Size = new System.Drawing.Size(154, 25);
            this.txt_dolar.TabIndex = 17;
            this.txt_dolar.TextChanged += new System.EventHandler(this.txt_dolar_TextChanged);
            this.txt_dolar.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_dolar_KeyPress);
            // 
            // txt_colones
            // 
            this.txt_colones.BackColor = System.Drawing.Color.White;
            this.txt_colones.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_colones.Font = new System.Drawing.Font("Arial Rounded MT Bold", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_colones.ForeColor = System.Drawing.Color.Black;
            this.txt_colones.Location = new System.Drawing.Point(49, 142);
            this.txt_colones.Name = "txt_colones";
            this.txt_colones.Size = new System.Drawing.Size(154, 25);
            this.txt_colones.TabIndex = 18;
            this.txt_colones.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_colones_KeyPress);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Arial Rounded MT Bold", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(23, 103);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(21, 22);
            this.label1.TabIndex = 19;
            this.label1.Text = "$";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Arial Rounded MT Bold", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(23, 143);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(22, 22);
            this.label2.TabIndex = 20;
            this.label2.Text = "₡";
            // 
            // label_tipo_cambio
            // 
            this.label_tipo_cambio.AutoSize = true;
            this.label_tipo_cambio.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_tipo_cambio.ForeColor = System.Drawing.Color.Black;
            this.label_tipo_cambio.Location = new System.Drawing.Point(109, 174);
            this.label_tipo_cambio.Name = "label_tipo_cambio";
            this.label_tipo_cambio.Size = new System.Drawing.Size(31, 15);
            this.label_tipo_cambio.TabIndex = 21;
            this.label_tipo_cambio.Text = "638";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(99, 174);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(14, 15);
            this.label3.TabIndex = 22;
            this.label3.Text = "₡";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.ForestGreen;
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.btn_tipo_cambio);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(242, 73);
            this.panel1.TabIndex = 23;
            // 
            // Form_Tipo_Cambio
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(242, 202);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label_tipo_cambio);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txt_colones);
            this.Controls.Add(this.txt_dolar);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Location = new System.Drawing.Point(400, 90);
            this.Name = "Form_Tipo_Cambio";
            this.Opacity = 0.9D;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Tipo de Cambio";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form_Tipo_Cambio_FormClosed);
            this.Load += new System.EventHandler(this.Form_Tipo_Cambio_Load);
            ((System.ComponentModel.ISupportInitialize)(this.btn_tipo_cambio)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.PictureBox btn_tipo_cambio;
        private System.Windows.Forms.TextBox txt_dolar;
        private System.Windows.Forms.TextBox txt_colones;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label_tipo_cambio;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel1;
    }
}