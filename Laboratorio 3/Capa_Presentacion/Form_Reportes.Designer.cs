﻿
namespace Capa_Presentacion
{
    partial class Form_Reportes
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form_Reportes));
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea4 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend4 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series4 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.panel_principal = new System.Windows.Forms.Panel();
            this.btn_reservas = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.dgv_reporte_2 = new System.Windows.Forms.DataGridView();
            this.grafico_reporte_1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.dtp_desde = new System.Windows.Forms.DateTimePicker();
            this.btn_generar = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.dtp_hasta = new System.Windows.Forms.DateTimePicker();
            this.column_cedula = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.column_nombre = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.column_fecha_entrada = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.column_fecha_salida = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.column_cantidad_personas = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.column_cantidad_noches = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.column_monto_total = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.column_usuario = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel_principal.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btn_reservas)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_reporte_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grafico_reporte_1)).BeginInit();
            this.SuspendLayout();
            // 
            // panel_principal
            // 
            this.panel_principal.BackColor = System.Drawing.Color.ForestGreen;
            this.panel_principal.Controls.Add(this.label1);
            this.panel_principal.Controls.Add(this.btn_reservas);
            this.panel_principal.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel_principal.Location = new System.Drawing.Point(0, 0);
            this.panel_principal.Name = "panel_principal";
            this.panel_principal.Size = new System.Drawing.Size(783, 69);
            this.panel_principal.TabIndex = 7;
            // 
            // btn_reservas
            // 
            this.btn_reservas.Image = ((System.Drawing.Image)(resources.GetObject("btn_reservas.Image")));
            this.btn_reservas.Location = new System.Drawing.Point(276, 7);
            this.btn_reservas.Name = "btn_reservas";
            this.btn_reservas.Size = new System.Drawing.Size(97, 57);
            this.btn_reservas.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btn_reservas.TabIndex = 12;
            this.btn_reservas.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial Rounded MT Bold", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(353, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(136, 32);
            this.label1.TabIndex = 11;
            this.label1.Text = "Reportes";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.dgv_reporte_2);
            this.panel1.Location = new System.Drawing.Point(12, 288);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(759, 148);
            this.panel1.TabIndex = 8;
            // 
            // dgv_reporte_2
            // 
            this.dgv_reporte_2.AllowUserToAddRows = false;
            this.dgv_reporte_2.AllowUserToDeleteRows = false;
            this.dgv_reporte_2.BackgroundColor = System.Drawing.Color.White;
            this.dgv_reporte_2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgv_reporte_2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_reporte_2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.column_cedula,
            this.column_nombre,
            this.column_fecha_entrada,
            this.column_fecha_salida,
            this.column_cantidad_personas,
            this.column_cantidad_noches,
            this.column_monto_total,
            this.column_usuario});
            this.dgv_reporte_2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv_reporte_2.Location = new System.Drawing.Point(0, 0);
            this.dgv_reporte_2.Name = "dgv_reporte_2";
            this.dgv_reporte_2.ReadOnly = true;
            this.dgv_reporte_2.RowHeadersVisible = false;
            this.dgv_reporte_2.Size = new System.Drawing.Size(759, 148);
            this.dgv_reporte_2.TabIndex = 0;
            // 
            // grafico_reporte_1
            // 
            chartArea4.Name = "ChartArea1";
            this.grafico_reporte_1.ChartAreas.Add(chartArea4);
            legend4.Name = "Legend1";
            this.grafico_reporte_1.Legends.Add(legend4);
            this.grafico_reporte_1.Location = new System.Drawing.Point(134, 92);
            this.grafico_reporte_1.Name = "grafico_reporte_1";
            series4.ChartArea = "ChartArea1";
            series4.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Doughnut;
            series4.Label = "#PERCENT{P0}";
            series4.LabelToolTip = "#VALX";
            series4.Legend = "Legend1";
            series4.LegendText = "#VALX";
            series4.LegendToolTip = "#PERCENT{P0}";
            series4.Name = "Series1";
            this.grafico_reporte_1.Series.Add(series4);
            this.grafico_reporte_1.Size = new System.Drawing.Size(528, 142);
            this.grafico_reporte_1.TabIndex = 9;
            this.grafico_reporte_1.Text = "chart1";
            // 
            // dtp_desde
            // 
            this.dtp_desde.Location = new System.Drawing.Point(61, 262);
            this.dtp_desde.Name = "dtp_desde";
            this.dtp_desde.Size = new System.Drawing.Size(229, 20);
            this.dtp_desde.TabIndex = 10;
            // 
            // btn_generar
            // 
            this.btn_generar.BackColor = System.Drawing.Color.ForestGreen;
            this.btn_generar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_generar.Font = new System.Drawing.Font("Arial Rounded MT Bold", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_generar.ForeColor = System.Drawing.Color.White;
            this.btn_generar.Location = new System.Drawing.Point(585, 262);
            this.btn_generar.Name = "btn_generar";
            this.btn_generar.Size = new System.Drawing.Size(186, 20);
            this.btn_generar.TabIndex = 12;
            this.btn_generar.Text = "Generar";
            this.btn_generar.UseVisualStyleBackColor = false;
            this.btn_generar.Click += new System.EventHandler(this.btn_generar_Click_1);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial Rounded MT Bold", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(16, 266);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(43, 12);
            this.label2.TabIndex = 13;
            this.label2.Text = "Desde:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial Rounded MT Bold", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(300, 266);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(40, 12);
            this.label3.TabIndex = 14;
            this.label3.Text = "Hasta:";
            // 
            // dtp_hasta
            // 
            this.dtp_hasta.Location = new System.Drawing.Point(346, 262);
            this.dtp_hasta.Name = "dtp_hasta";
            this.dtp_hasta.Size = new System.Drawing.Size(233, 20);
            this.dtp_hasta.TabIndex = 11;
            // 
            // column_cedula
            // 
            this.column_cedula.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.column_cedula.Frozen = true;
            this.column_cedula.HeaderText = "Cedula";
            this.column_cedula.Name = "column_cedula";
            this.column_cedula.ReadOnly = true;
            this.column_cedula.Width = 65;
            // 
            // column_nombre
            // 
            this.column_nombre.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.column_nombre.Frozen = true;
            this.column_nombre.HeaderText = "Nombre";
            this.column_nombre.Name = "column_nombre";
            this.column_nombre.ReadOnly = true;
            // 
            // column_fecha_entrada
            // 
            this.column_fecha_entrada.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.column_fecha_entrada.Frozen = true;
            this.column_fecha_entrada.HeaderText = "Fecha Entrada";
            this.column_fecha_entrada.Name = "column_fecha_entrada";
            this.column_fecha_entrada.ReadOnly = true;
            this.column_fecha_entrada.Width = 102;
            // 
            // column_fecha_salida
            // 
            this.column_fecha_salida.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.column_fecha_salida.Frozen = true;
            this.column_fecha_salida.HeaderText = "Fecha Salida";
            this.column_fecha_salida.Name = "column_fecha_salida";
            this.column_fecha_salida.ReadOnly = true;
            this.column_fecha_salida.Width = 94;
            // 
            // column_cantidad_personas
            // 
            this.column_cantidad_personas.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.column_cantidad_personas.Frozen = true;
            this.column_cantidad_personas.HeaderText = "Personas";
            this.column_cantidad_personas.Name = "column_cantidad_personas";
            this.column_cantidad_personas.ReadOnly = true;
            this.column_cantidad_personas.Width = 76;
            // 
            // column_cantidad_noches
            // 
            this.column_cantidad_noches.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.column_cantidad_noches.Frozen = true;
            this.column_cantidad_noches.HeaderText = "Noches";
            this.column_cantidad_noches.Name = "column_cantidad_noches";
            this.column_cantidad_noches.ReadOnly = true;
            this.column_cantidad_noches.Width = 69;
            // 
            // column_monto_total
            // 
            this.column_monto_total.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.column_monto_total.Frozen = true;
            this.column_monto_total.HeaderText = "Precio Total";
            this.column_monto_total.Name = "column_monto_total";
            this.column_monto_total.ReadOnly = true;
            this.column_monto_total.Width = 89;
            // 
            // column_usuario
            // 
            this.column_usuario.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.column_usuario.Frozen = true;
            this.column_usuario.HeaderText = "Reservador";
            this.column_usuario.Name = "column_usuario";
            this.column_usuario.ReadOnly = true;
            this.column_usuario.Width = 160;
            // 
            // Form_Reportes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(783, 450);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btn_generar);
            this.Controls.Add(this.dtp_hasta);
            this.Controls.Add(this.dtp_desde);
            this.Controls.Add(this.grafico_reporte_1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel_principal);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Location = new System.Drawing.Point(125, 10);
            this.Name = "Form_Reportes";
            this.Opacity = 0.9D;
            this.Load += new System.EventHandler(this.Form_Reportes_Load);
            this.panel_principal.ResumeLayout(false);
            this.panel_principal.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btn_reservas)).EndInit();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_reporte_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grafico_reporte_1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel_principal;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox btn_reservas;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DataGridView dgv_reporte_2;
        private System.Windows.Forms.DataVisualization.Charting.Chart grafico_reporte_1;
        private System.Windows.Forms.DateTimePicker dtp_desde;
        private System.Windows.Forms.Button btn_generar;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DateTimePicker dtp_hasta;
        private System.Windows.Forms.DataGridViewTextBoxColumn column_cedula;
        private System.Windows.Forms.DataGridViewTextBoxColumn column_nombre;
        private System.Windows.Forms.DataGridViewTextBoxColumn column_fecha_entrada;
        private System.Windows.Forms.DataGridViewTextBoxColumn column_fecha_salida;
        private System.Windows.Forms.DataGridViewTextBoxColumn column_cantidad_personas;
        private System.Windows.Forms.DataGridViewTextBoxColumn column_cantidad_noches;
        private System.Windows.Forms.DataGridViewTextBoxColumn column_monto_total;
        private System.Windows.Forms.DataGridViewTextBoxColumn column_usuario;
    }
}