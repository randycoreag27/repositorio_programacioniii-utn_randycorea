﻿
namespace Capa_Presentacion
{
    partial class Form_Reservas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form_Reservas));
            this.dtp_fecha_entrada = new System.Windows.Forms.DateTimePicker();
            this.dtp_fecha_salida = new System.Windows.Forms.DateTimePicker();
            this.cbx_cantidad_personas = new System.Windows.Forms.ComboBox();
            this.panel_principal = new System.Windows.Forms.Panel();
            this.btn_reservas = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.btn_consultar = new System.Windows.Forms.Button();
            this.txt_cedula = new System.Windows.Forms.TextBox();
            this.txt_nombre = new System.Windows.Forms.TextBox();
            this.rdb_masculino = new System.Windows.Forms.RadioButton();
            this.rdb_femenino = new System.Windows.Forms.RadioButton();
            this.txt_telefono_movil = new System.Windows.Forms.TextBox();
            this.txt_telefono_fijo = new System.Windows.Forms.TextBox();
            this.txt_correo_electronico = new System.Windows.Forms.TextBox();
            this.btn_confirmar_reserva = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.cbx_tarifa = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.rbt_clientes_existentes = new System.Windows.Forms.RadioButton();
            this.cbx_clientes = new System.Windows.Forms.ComboBox();
            this.panel_principal.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btn_reservas)).BeginInit();
            this.SuspendLayout();
            // 
            // dtp_fecha_entrada
            // 
            this.dtp_fecha_entrada.Font = new System.Drawing.Font("Arial Rounded MT Bold", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtp_fecha_entrada.Location = new System.Drawing.Point(12, 103);
            this.dtp_fecha_entrada.Name = "dtp_fecha_entrada";
            this.dtp_fecha_entrada.Size = new System.Drawing.Size(294, 25);
            this.dtp_fecha_entrada.TabIndex = 0;
            // 
            // dtp_fecha_salida
            // 
            this.dtp_fecha_salida.Font = new System.Drawing.Font("Arial Rounded MT Bold", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtp_fecha_salida.Location = new System.Drawing.Point(312, 103);
            this.dtp_fecha_salida.Name = "dtp_fecha_salida";
            this.dtp_fecha_salida.Size = new System.Drawing.Size(294, 25);
            this.dtp_fecha_salida.TabIndex = 1;
            // 
            // cbx_cantidad_personas
            // 
            this.cbx_cantidad_personas.Font = new System.Drawing.Font("Arial Rounded MT Bold", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbx_cantidad_personas.FormattingEnabled = true;
            this.cbx_cantidad_personas.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8"});
            this.cbx_cantidad_personas.Location = new System.Drawing.Point(612, 103);
            this.cbx_cantidad_personas.Name = "cbx_cantidad_personas";
            this.cbx_cantidad_personas.Size = new System.Drawing.Size(160, 25);
            this.cbx_cantidad_personas.TabIndex = 2;
            // 
            // panel_principal
            // 
            this.panel_principal.BackColor = System.Drawing.Color.ForestGreen;
            this.panel_principal.Controls.Add(this.btn_reservas);
            this.panel_principal.Controls.Add(this.label1);
            this.panel_principal.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel_principal.Location = new System.Drawing.Point(0, 0);
            this.panel_principal.Name = "panel_principal";
            this.panel_principal.Size = new System.Drawing.Size(784, 69);
            this.panel_principal.TabIndex = 6;
            // 
            // btn_reservas
            // 
            this.btn_reservas.Image = ((System.Drawing.Image)(resources.GetObject("btn_reservas.Image")));
            this.btn_reservas.Location = new System.Drawing.Point(256, 9);
            this.btn_reservas.Name = "btn_reservas";
            this.btn_reservas.Size = new System.Drawing.Size(86, 49);
            this.btn_reservas.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btn_reservas.TabIndex = 12;
            this.btn_reservas.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial Rounded MT Bold", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(348, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(139, 32);
            this.label1.TabIndex = 11;
            this.label1.Text = "Reservas";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(12, 81);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(121, 15);
            this.label2.TabIndex = 13;
            this.label2.Text = "Fecha de Entrada";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(309, 81);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(114, 15);
            this.label3.TabIndex = 14;
            this.label3.Text = "Fecha de Salida:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(609, 81);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(149, 15);
            this.label4.TabIndex = 15;
            this.label4.Text = "Cantidad de Personas";
            // 
            // btn_consultar
            // 
            this.btn_consultar.BackColor = System.Drawing.Color.ForestGreen;
            this.btn_consultar.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DarkGreen;
            this.btn_consultar.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DarkGreen;
            this.btn_consultar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_consultar.Font = new System.Drawing.Font("Arial Rounded MT Bold", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_consultar.ForeColor = System.Drawing.Color.White;
            this.btn_consultar.Location = new System.Drawing.Point(612, 180);
            this.btn_consultar.Name = "btn_consultar";
            this.btn_consultar.Size = new System.Drawing.Size(160, 23);
            this.btn_consultar.TabIndex = 16;
            this.btn_consultar.Text = "Consultar Disponibilidad";
            this.btn_consultar.UseVisualStyleBackColor = false;
            this.btn_consultar.Click += new System.EventHandler(this.btn_consultar_Click);
            // 
            // txt_cedula
            // 
            this.txt_cedula.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_cedula.Enabled = false;
            this.txt_cedula.Font = new System.Drawing.Font("Arial Rounded MT Bold", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_cedula.Location = new System.Drawing.Point(12, 240);
            this.txt_cedula.Name = "txt_cedula";
            this.txt_cedula.Size = new System.Drawing.Size(189, 25);
            this.txt_cedula.TabIndex = 17;
            this.txt_cedula.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_cedula_KeyPress);
            // 
            // txt_nombre
            // 
            this.txt_nombre.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_nombre.Enabled = false;
            this.txt_nombre.Font = new System.Drawing.Font("Arial Rounded MT Bold", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_nombre.Location = new System.Drawing.Point(207, 240);
            this.txt_nombre.Name = "txt_nombre";
            this.txt_nombre.Size = new System.Drawing.Size(212, 25);
            this.txt_nombre.TabIndex = 18;
            // 
            // rdb_masculino
            // 
            this.rdb_masculino.AutoSize = true;
            this.rdb_masculino.Enabled = false;
            this.rdb_masculino.Font = new System.Drawing.Font("Arial Rounded MT Bold", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdb_masculino.Location = new System.Drawing.Point(425, 240);
            this.rdb_masculino.Name = "rdb_masculino";
            this.rdb_masculino.Size = new System.Drawing.Size(99, 21);
            this.rdb_masculino.TabIndex = 19;
            this.rdb_masculino.TabStop = true;
            this.rdb_masculino.Text = "Masculino";
            this.rdb_masculino.UseVisualStyleBackColor = true;
            // 
            // rdb_femenino
            // 
            this.rdb_femenino.AutoSize = true;
            this.rdb_femenino.Enabled = false;
            this.rdb_femenino.Font = new System.Drawing.Font("Arial Rounded MT Bold", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdb_femenino.Location = new System.Drawing.Point(530, 240);
            this.rdb_femenino.Name = "rdb_femenino";
            this.rdb_femenino.Size = new System.Drawing.Size(97, 21);
            this.rdb_femenino.TabIndex = 20;
            this.rdb_femenino.TabStop = true;
            this.rdb_femenino.Text = "Femenino";
            this.rdb_femenino.UseVisualStyleBackColor = true;
            // 
            // txt_telefono_movil
            // 
            this.txt_telefono_movil.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_telefono_movil.Enabled = false;
            this.txt_telefono_movil.Font = new System.Drawing.Font("Arial Rounded MT Bold", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_telefono_movil.Location = new System.Drawing.Point(209, 294);
            this.txt_telefono_movil.Name = "txt_telefono_movil";
            this.txt_telefono_movil.Size = new System.Drawing.Size(210, 25);
            this.txt_telefono_movil.TabIndex = 21;
            this.txt_telefono_movil.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_cedula_KeyPress);
            // 
            // txt_telefono_fijo
            // 
            this.txt_telefono_fijo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_telefono_fijo.Enabled = false;
            this.txt_telefono_fijo.Font = new System.Drawing.Font("Arial Rounded MT Bold", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_telefono_fijo.Location = new System.Drawing.Point(12, 294);
            this.txt_telefono_fijo.Name = "txt_telefono_fijo";
            this.txt_telefono_fijo.Size = new System.Drawing.Size(189, 25);
            this.txt_telefono_fijo.TabIndex = 22;
            this.txt_telefono_fijo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_cedula_KeyPress);
            // 
            // txt_correo_electronico
            // 
            this.txt_correo_electronico.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_correo_electronico.Enabled = false;
            this.txt_correo_electronico.Font = new System.Drawing.Font("Arial Rounded MT Bold", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_correo_electronico.Location = new System.Drawing.Point(425, 294);
            this.txt_correo_electronico.Name = "txt_correo_electronico";
            this.txt_correo_electronico.Size = new System.Drawing.Size(202, 25);
            this.txt_correo_electronico.TabIndex = 23;
            // 
            // btn_confirmar_reserva
            // 
            this.btn_confirmar_reserva.BackColor = System.Drawing.Color.ForestGreen;
            this.btn_confirmar_reserva.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DarkGreen;
            this.btn_confirmar_reserva.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DarkGreen;
            this.btn_confirmar_reserva.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_confirmar_reserva.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_confirmar_reserva.ForeColor = System.Drawing.Color.White;
            this.btn_confirmar_reserva.Location = new System.Drawing.Point(664, 240);
            this.btn_confirmar_reserva.Name = "btn_confirmar_reserva";
            this.btn_confirmar_reserva.Size = new System.Drawing.Size(108, 79);
            this.btn_confirmar_reserva.TabIndex = 24;
            this.btn_confirmar_reserva.Text = "Confirmar Reserva";
            this.btn_confirmar_reserva.UseVisualStyleBackColor = false;
            this.btn_confirmar_reserva.Click += new System.EventHandler(this.btn_confirmar_reserva_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(12, 215);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(57, 15);
            this.label5.TabIndex = 25;
            this.label5.Text = "Cédula:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(206, 215);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(129, 15);
            this.label6.TabIndex = 26;
            this.label6.Text = "Nombre Completo:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(422, 215);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(59, 15);
            this.label7.TabIndex = 27;
            this.label7.Text = "Género:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(12, 272);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(94, 15);
            this.label8.TabIndex = 28;
            this.label8.Text = "Telefono Fijo:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(206, 272);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(104, 15);
            this.label9.TabIndex = 29;
            this.label9.Text = "Telefono Movil:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(422, 272);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(136, 15);
            this.label10.TabIndex = 30;
            this.label10.Text = "Correo Electronico:";
            // 
            // cbx_tarifa
            // 
            this.cbx_tarifa.Font = new System.Drawing.Font("Arial Rounded MT Bold", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbx_tarifa.FormattingEnabled = true;
            this.cbx_tarifa.Location = new System.Drawing.Point(612, 149);
            this.cbx_tarifa.Name = "cbx_tarifa";
            this.cbx_tarifa.Size = new System.Drawing.Size(160, 25);
            this.cbx_tarifa.TabIndex = 31;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Arial Rounded MT Bold", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Black;
            this.label11.Location = new System.Drawing.Point(12, 137);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(191, 32);
            this.label11.TabIndex = 13;
            this.label11.Text = "Datos Cliente";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Black;
            this.label12.Location = new System.Drawing.Point(609, 131);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(45, 15);
            this.label12.TabIndex = 32;
            this.label12.Text = "Tarifa";
            // 
            // rbt_clientes_existentes
            // 
            this.rbt_clientes_existentes.AutoSize = true;
            this.rbt_clientes_existentes.Font = new System.Drawing.Font("Arial Rounded MT Bold", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbt_clientes_existentes.Location = new System.Drawing.Point(209, 147);
            this.rbt_clientes_existentes.Name = "rbt_clientes_existentes";
            this.rbt_clientes_existentes.Size = new System.Drawing.Size(148, 21);
            this.rbt_clientes_existentes.TabIndex = 33;
            this.rbt_clientes_existentes.TabStop = true;
            this.rbt_clientes_existentes.Text = "Cliente Existente";
            this.rbt_clientes_existentes.UseVisualStyleBackColor = true;
            this.rbt_clientes_existentes.CheckedChanged += new System.EventHandler(this.rbt_clientes_existentes_CheckedChanged);
            // 
            // cbx_clientes
            // 
            this.cbx_clientes.Enabled = false;
            this.cbx_clientes.Font = new System.Drawing.Font("Arial Rounded MT Bold", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbx_clientes.FormattingEnabled = true;
            this.cbx_clientes.Location = new System.Drawing.Point(12, 178);
            this.cbx_clientes.Name = "cbx_clientes";
            this.cbx_clientes.Size = new System.Drawing.Size(189, 25);
            this.cbx_clientes.TabIndex = 34;
            this.cbx_clientes.SelectedIndexChanged += new System.EventHandler(this.cbx_clientes_SelectedIndexChanged);
            // 
            // Form_Reservas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(784, 334);
            this.Controls.Add(this.cbx_clientes);
            this.Controls.Add(this.rbt_clientes_existentes);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.cbx_tarifa);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.btn_confirmar_reserva);
            this.Controls.Add(this.txt_correo_electronico);
            this.Controls.Add(this.txt_telefono_fijo);
            this.Controls.Add(this.txt_telefono_movil);
            this.Controls.Add(this.rdb_femenino);
            this.Controls.Add(this.rdb_masculino);
            this.Controls.Add(this.txt_nombre);
            this.Controls.Add(this.txt_cedula);
            this.Controls.Add(this.btn_consultar);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.panel_principal);
            this.Controls.Add(this.cbx_cantidad_personas);
            this.Controls.Add(this.dtp_fecha_salida);
            this.Controls.Add(this.dtp_fecha_entrada);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Location = new System.Drawing.Point(125, 70);
            this.Name = "Form_Reservas";
            this.Opacity = 0.9D;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Reservas";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form_Reservas_FormClosed);
            this.Load += new System.EventHandler(this.Form_Reservas_Load);
            this.panel_principal.ResumeLayout(false);
            this.panel_principal.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btn_reservas)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DateTimePicker dtp_fecha_entrada;
        private System.Windows.Forms.DateTimePicker dtp_fecha_salida;
        private System.Windows.Forms.ComboBox cbx_cantidad_personas;
        private System.Windows.Forms.Panel panel_principal;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox btn_reservas;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btn_consultar;
        private System.Windows.Forms.TextBox txt_cedula;
        private System.Windows.Forms.TextBox txt_nombre;
        private System.Windows.Forms.RadioButton rdb_masculino;
        private System.Windows.Forms.RadioButton rdb_femenino;
        private System.Windows.Forms.TextBox txt_telefono_movil;
        private System.Windows.Forms.TextBox txt_telefono_fijo;
        private System.Windows.Forms.TextBox txt_correo_electronico;
        private System.Windows.Forms.Button btn_confirmar_reserva;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox cbx_tarifa;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.RadioButton rbt_clientes_existentes;
        private System.Windows.Forms.ComboBox cbx_clientes;
    }
}