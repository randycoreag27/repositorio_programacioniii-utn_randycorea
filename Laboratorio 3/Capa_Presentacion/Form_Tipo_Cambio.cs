﻿using Objetos;
using System;
using System.Data;
using System.Windows.Forms;

namespace Capa_Presentacion
{
    public partial class Form_Tipo_Cambio : Form
    {

        Tipo_Cambio tipo_cambio = new Tipo_Cambio(); 

        public Form_Tipo_Cambio()
        {
            InitializeComponent();
            Obtener_Tipo_Cambio_DataSet();
        }

        public void Obtener_Tipo_Cambio_DataSet()
        {
            cr.fi.bccr.gee.wsindicadoreseconomicos dato = new cr.fi.bccr.gee.wsindicadoreseconomicos();
            DataSet tipoCambio = dato.ObtenerIndicadoresEconomicos("317", DateTime.Now.ToString("dd/MM/yyyy"), DateTime.Now.ToString("dd/MM/yyyy"),
                "Randy Corea", "N", "randycoreag27@gmail.com", "OC74000PGR");
            tipo_cambio.tipo_cambio = Convert.ToInt32(tipoCambio.Tables[0].Rows[0].ItemArray[2]);
            label_tipo_cambio.Text  = Convert.ToString(tipo_cambio.tipo_cambio);
        }

        public int Multiplicacion(int tc, int d)
        {
            com.dneonline.www.Calculator calculator = new com.dneonline.www.Calculator();
            int dato = calculator.Multiply(tc, d);
            return dato;
        }

        private void txt_dolar_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar >= 32 && e.KeyChar <= 47) || (e.KeyChar >= 58 && e.KeyChar <= 255))
            {
                MessageBox.Show("Solo se pueden ingresar números", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                e.Handled = true;
                return;
            }
        }

        private void txt_colones_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar >= 32 && e.KeyChar <= 47) || (e.KeyChar >= 58 && e.KeyChar <= 255))
            {
                MessageBox.Show("Solo se pueden ingresar números", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                e.Handled = true;
                return;
            }
        }

        private void txt_dolar_TextChanged(object sender, EventArgs e)
        {
            if(txt_dolar.Text != "")
            {
                tipo_cambio.dolares = Convert.ToInt32(txt_dolar.Text);
                tipo_cambio.colones = Multiplicacion(tipo_cambio.tipo_cambio, tipo_cambio.dolares);
                txt_colones.Text = Convert.ToString(tipo_cambio.colones);
            }
            else
            {
                txt_colones.Clear();
            }

        }

        private void Form_Tipo_Cambio_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.Hide();
        }

        private void Form_Tipo_Cambio_Load(object sender, EventArgs e)
        {

        }
    }
}
