﻿
namespace Capa_Presentacion
{
    partial class Form_Facturacion
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form_Facturacion));
            this.panel_factura = new System.Windows.Forms.Panel();
            this.colones = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.total = new System.Windows.Forms.Label();
            this.iva = new System.Windows.Forms.Label();
            this.subtotal = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.fecha_salida = new System.Windows.Forms.Label();
            this.fecha_entrada = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.precio = new System.Windows.Forms.Label();
            this.tarifa = new System.Windows.Forms.Label();
            this.cantidad_noches = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.hora = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label_ced_cliente = new System.Windows.Forms.Label();
            this.label_nom_cliente = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.fecha = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label_num_factura = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label13 = new System.Windows.Forms.Label();
            this.shapeContainer2 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.lineShape4 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape3 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape2 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape1 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.cbx_reservas = new System.Windows.Forms.ComboBox();
            this.printDocument1 = new System.Drawing.Printing.PrintDocument();
            this.printPreviewDialog1 = new System.Windows.Forms.PrintPreviewDialog();
            this.btn_imprimir = new System.Windows.Forms.PictureBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.panel_factura.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btn_imprimir)).BeginInit();
            this.SuspendLayout();
            // 
            // panel_factura
            // 
            this.panel_factura.BackColor = System.Drawing.Color.White;
            this.panel_factura.Controls.Add(this.colones);
            this.panel_factura.Controls.Add(this.label26);
            this.panel_factura.Controls.Add(this.total);
            this.panel_factura.Controls.Add(this.iva);
            this.panel_factura.Controls.Add(this.subtotal);
            this.panel_factura.Controls.Add(this.label22);
            this.panel_factura.Controls.Add(this.label21);
            this.panel_factura.Controls.Add(this.label20);
            this.panel_factura.Controls.Add(this.fecha_salida);
            this.panel_factura.Controls.Add(this.fecha_entrada);
            this.panel_factura.Controls.Add(this.label17);
            this.panel_factura.Controls.Add(this.precio);
            this.panel_factura.Controls.Add(this.tarifa);
            this.panel_factura.Controls.Add(this.cantidad_noches);
            this.panel_factura.Controls.Add(this.label12);
            this.panel_factura.Controls.Add(this.label11);
            this.panel_factura.Controls.Add(this.label10);
            this.panel_factura.Controls.Add(this.label9);
            this.panel_factura.Controls.Add(this.hora);
            this.panel_factura.Controls.Add(this.label6);
            this.panel_factura.Controls.Add(this.label_ced_cliente);
            this.panel_factura.Controls.Add(this.label_nom_cliente);
            this.panel_factura.Controls.Add(this.label4);
            this.panel_factura.Controls.Add(this.fecha);
            this.panel_factura.Controls.Add(this.label2);
            this.panel_factura.Controls.Add(this.label_num_factura);
            this.panel_factura.Controls.Add(this.label1);
            this.panel_factura.Controls.Add(this.pictureBox2);
            this.panel_factura.Controls.Add(this.panel3);
            this.panel_factura.Controls.Add(this.shapeContainer2);
            this.panel_factura.Location = new System.Drawing.Point(12, 127);
            this.panel_factura.Name = "panel_factura";
            this.panel_factura.Size = new System.Drawing.Size(566, 584);
            this.panel_factura.TabIndex = 0;
            // 
            // colones
            // 
            this.colones.AutoSize = true;
            this.colones.Font = new System.Drawing.Font("Arial Rounded MT Bold", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.colones.ForeColor = System.Drawing.Color.Black;
            this.colones.Location = new System.Drawing.Point(452, 417);
            this.colones.Name = "colones";
            this.colones.Size = new System.Drawing.Size(76, 22);
            this.colones.TabIndex = 33;
            this.colones.Text = "250000";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Arial Rounded MT Bold", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.ForeColor = System.Drawing.Color.ForestGreen;
            this.label26.Location = new System.Drawing.Point(273, 415);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(169, 24);
            this.label26.TabIndex = 32;
            this.label26.Text = "Monto Colones:";
            // 
            // total
            // 
            this.total.AutoSize = true;
            this.total.Font = new System.Drawing.Font("Arial Rounded MT Bold", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.total.ForeColor = System.Drawing.Color.Black;
            this.total.Location = new System.Drawing.Point(463, 362);
            this.total.Name = "total";
            this.total.Size = new System.Drawing.Size(54, 22);
            this.total.TabIndex = 31;
            this.total.Text = "$400";
            // 
            // iva
            // 
            this.iva.AutoSize = true;
            this.iva.Font = new System.Drawing.Font("Arial Rounded MT Bold", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.iva.ForeColor = System.Drawing.Color.Black;
            this.iva.Location = new System.Drawing.Point(463, 327);
            this.iva.Name = "iva";
            this.iva.Size = new System.Drawing.Size(54, 22);
            this.iva.TabIndex = 30;
            this.iva.Text = "$400";
            // 
            // subtotal
            // 
            this.subtotal.AutoSize = true;
            this.subtotal.Font = new System.Drawing.Font("Arial Rounded MT Bold", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.subtotal.ForeColor = System.Drawing.Color.Black;
            this.subtotal.Location = new System.Drawing.Point(463, 292);
            this.subtotal.Name = "subtotal";
            this.subtotal.Size = new System.Drawing.Size(54, 22);
            this.subtotal.TabIndex = 29;
            this.subtotal.Text = "$400";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Arial Rounded MT Bold", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.ForestGreen;
            this.label22.Location = new System.Drawing.Point(376, 360);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(68, 24);
            this.label22.TabIndex = 28;
            this.label22.Text = "Total:";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Arial Rounded MT Bold", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.ForestGreen;
            this.label21.Location = new System.Drawing.Point(344, 290);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(102, 24);
            this.label21.TabIndex = 27;
            this.label21.Text = "Subtotal:";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Arial Rounded MT Bold", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.ForestGreen;
            this.label20.Location = new System.Drawing.Point(378, 325);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(67, 24);
            this.label20.TabIndex = 26;
            this.label20.Text = "I.V.A:";
            // 
            // fecha_salida
            // 
            this.fecha_salida.AutoSize = true;
            this.fecha_salida.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fecha_salida.ForeColor = System.Drawing.Color.Black;
            this.fecha_salida.Location = new System.Drawing.Point(28, 262);
            this.fecha_salida.Name = "fecha_salida";
            this.fecha_salida.Size = new System.Drawing.Size(63, 15);
            this.fecha_salida.TabIndex = 25;
            this.fecha_salida.Text = "18/18/18";
            // 
            // fecha_entrada
            // 
            this.fecha_entrada.AutoSize = true;
            this.fecha_entrada.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fecha_entrada.ForeColor = System.Drawing.Color.Black;
            this.fecha_entrada.Location = new System.Drawing.Point(28, 241);
            this.fecha_entrada.Name = "fecha_entrada";
            this.fecha_entrada.Size = new System.Drawing.Size(63, 15);
            this.fecha_entrada.TabIndex = 24;
            this.fecha_entrada.Text = "16/16/16";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.Black;
            this.label17.Location = new System.Drawing.Point(28, 222);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(167, 15);
            this.label17.TabIndex = 23;
            this.label17.Text = "Hospedaje en las Fechas";
            // 
            // precio
            // 
            this.precio.AutoSize = true;
            this.precio.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.precio.ForeColor = System.Drawing.Color.Black;
            this.precio.Location = new System.Drawing.Point(468, 226);
            this.precio.Name = "precio";
            this.precio.Size = new System.Drawing.Size(79, 15);
            this.precio.TabIndex = 22;
            this.precio.Text = "604690534";
            // 
            // tarifa
            // 
            this.tarifa.AutoSize = true;
            this.tarifa.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tarifa.ForeColor = System.Drawing.Color.Black;
            this.tarifa.Location = new System.Drawing.Point(385, 225);
            this.tarifa.Name = "tarifa";
            this.tarifa.Size = new System.Drawing.Size(41, 15);
            this.tarifa.TabIndex = 21;
            this.tarifa.Text = "ALTA";
            // 
            // cantidad_noches
            // 
            this.cantidad_noches.AutoSize = true;
            this.cantidad_noches.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cantidad_noches.ForeColor = System.Drawing.Color.Black;
            this.cantidad_noches.Location = new System.Drawing.Point(265, 223);
            this.cantidad_noches.Name = "cantidad_noches";
            this.cantidad_noches.Size = new System.Drawing.Size(15, 15);
            this.cantidad_noches.TabIndex = 20;
            this.cantidad_noches.Text = "2";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Arial Rounded MT Bold", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.ForestGreen;
            this.label12.Location = new System.Drawing.Point(468, 197);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(76, 24);
            this.label12.TabIndex = 19;
            this.label12.Text = "Precio";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Arial Rounded MT Bold", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.ForestGreen;
            this.label11.Location = new System.Drawing.Point(372, 196);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(69, 24);
            this.label11.TabIndex = 18;
            this.label11.Text = "Tarifa";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Arial Rounded MT Bold", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.ForestGreen;
            this.label10.Location = new System.Drawing.Point(206, 194);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(147, 24);
            this.label10.TabIndex = 17;
            this.label10.Text = "Cant. Noches";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Arial Rounded MT Bold", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.ForestGreen;
            this.label9.Location = new System.Drawing.Point(20, 193);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(133, 24);
            this.label9.TabIndex = 16;
            this.label9.Text = "Descripción";
            // 
            // hora
            // 
            this.hora.AutoSize = true;
            this.hora.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.hora.ForeColor = System.Drawing.Color.Black;
            this.hora.Location = new System.Drawing.Point(19, 88);
            this.hora.Name = "hora";
            this.hora.Size = new System.Drawing.Size(63, 15);
            this.hora.TabIndex = 15;
            this.hora.Text = "13:30:28";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(21, 158);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(57, 15);
            this.label6.TabIndex = 9;
            this.label6.Text = "Cédula:";
            // 
            // label_ced_cliente
            // 
            this.label_ced_cliente.AutoSize = true;
            this.label_ced_cliente.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_ced_cliente.ForeColor = System.Drawing.Color.Black;
            this.label_ced_cliente.Location = new System.Drawing.Point(75, 159);
            this.label_ced_cliente.Name = "label_ced_cliente";
            this.label_ced_cliente.Size = new System.Drawing.Size(79, 15);
            this.label_ced_cliente.TabIndex = 8;
            this.label_ced_cliente.Text = "604690534";
            // 
            // label_nom_cliente
            // 
            this.label_nom_cliente.AutoSize = true;
            this.label_nom_cliente.Font = new System.Drawing.Font("Arial Rounded MT Bold", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_nom_cliente.ForeColor = System.Drawing.Color.Black;
            this.label_nom_cliente.Location = new System.Drawing.Point(104, 132);
            this.label_nom_cliente.Name = "label_nom_cliente";
            this.label_nom_cliente.Size = new System.Drawing.Size(292, 24);
            this.label_nom_cliente.TabIndex = 7;
            this.label_nom_cliente.Text = "Pancracio Ramirez Jimenez";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial Rounded MT Bold", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.ForestGreen;
            this.label4.Location = new System.Drawing.Point(18, 132);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(89, 24);
            this.label4.TabIndex = 6;
            this.label4.Text = "Cliente:";
            // 
            // fecha
            // 
            this.fecha.AutoSize = true;
            this.fecha.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fecha.ForeColor = System.Drawing.Color.Black;
            this.fecha.Location = new System.Drawing.Point(19, 68);
            this.fecha.Name = "fecha";
            this.fecha.Size = new System.Drawing.Size(202, 15);
            this.fecha.TabIndex = 5;
            this.fecha.Text = "jueves, 18 de noviembre, 2021";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(19, 49);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(147, 15);
            this.label2.TabIndex = 4;
            this.label2.Text = "Florencia, San Carlos";
            // 
            // label_num_factura
            // 
            this.label_num_factura.AutoSize = true;
            this.label_num_factura.Font = new System.Drawing.Font("Arial Rounded MT Bold", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_num_factura.ForeColor = System.Drawing.Color.Black;
            this.label_num_factura.Location = new System.Drawing.Point(107, 23);
            this.label_num_factura.Name = "label_num_factura";
            this.label_num_factura.Size = new System.Drawing.Size(94, 24);
            this.label_num_factura.TabIndex = 3;
            this.label_num_factura.Text = "#######";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial Rounded MT Bold", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.ForestGreen;
            this.label1.Location = new System.Drawing.Point(15, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(95, 24);
            this.label1.TabIndex = 2;
            this.label1.Text = "Factura:";
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(423, 9);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(127, 109);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 1;
            this.pictureBox2.TabStop = false;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.ForestGreen;
            this.panel3.Controls.Add(this.label13);
            this.panel3.Location = new System.Drawing.Point(0, 547);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(566, 37);
            this.panel3.TabIndex = 0;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Arial Rounded MT Bold", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.White;
            this.label13.Location = new System.Drawing.Point(146, 6);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(262, 24);
            this.label13.TabIndex = 20;
            this.label13.Text = "RAMAYE BEACH HOUSE";
            // 
            // shapeContainer2
            // 
            this.shapeContainer2.Location = new System.Drawing.Point(0, 0);
            this.shapeContainer2.Margin = new System.Windows.Forms.Padding(0);
            this.shapeContainer2.Name = "shapeContainer2";
            this.shapeContainer2.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.lineShape4,
            this.lineShape3,
            this.lineShape2,
            this.lineShape1});
            this.shapeContainer2.Size = new System.Drawing.Size(566, 584);
            this.shapeContainer2.TabIndex = 12;
            this.shapeContainer2.TabStop = false;
            // 
            // lineShape4
            // 
            this.lineShape4.BorderColor = System.Drawing.Color.Goldenrod;
            this.lineShape4.BorderWidth = 2;
            this.lineShape4.Name = "lineShape4";
            this.lineShape4.SelectionColor = System.Drawing.Color.Goldenrod;
            this.lineShape4.X1 = 18;
            this.lineShape4.X2 = 549;
            this.lineShape4.Y1 = 407;
            this.lineShape4.Y2 = 407;
            // 
            // lineShape3
            // 
            this.lineShape3.BorderColor = System.Drawing.Color.Goldenrod;
            this.lineShape3.BorderWidth = 2;
            this.lineShape3.Name = "lineShape3";
            this.lineShape3.SelectionColor = System.Drawing.Color.Goldenrod;
            this.lineShape3.X1 = 18;
            this.lineShape3.X2 = 549;
            this.lineShape3.Y1 = 285;
            this.lineShape3.Y2 = 285;
            // 
            // lineShape2
            // 
            this.lineShape2.BorderColor = System.Drawing.Color.Goldenrod;
            this.lineShape2.BorderWidth = 2;
            this.lineShape2.Name = "lineShape2";
            this.lineShape2.SelectionColor = System.Drawing.Color.Goldenrod;
            this.lineShape2.X1 = 18;
            this.lineShape2.X2 = 549;
            this.lineShape2.Y1 = 183;
            this.lineShape2.Y2 = 183;
            // 
            // lineShape1
            // 
            this.lineShape1.BorderColor = System.Drawing.Color.Goldenrod;
            this.lineShape1.BorderWidth = 2;
            this.lineShape1.Name = "lineShape1";
            this.lineShape1.SelectionColor = System.Drawing.Color.Goldenrod;
            this.lineShape1.X1 = 18;
            this.lineShape1.X2 = 549;
            this.lineShape1.Y1 = 124;
            this.lineShape1.Y2 = 124;
            // 
            // cbx_reservas
            // 
            this.cbx_reservas.Font = new System.Drawing.Font("Arial Rounded MT Bold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbx_reservas.FormattingEnabled = true;
            this.cbx_reservas.Location = new System.Drawing.Point(13, 46);
            this.cbx_reservas.Name = "cbx_reservas";
            this.cbx_reservas.Size = new System.Drawing.Size(194, 26);
            this.cbx_reservas.TabIndex = 1;
            this.cbx_reservas.SelectedIndexChanged += new System.EventHandler(this.cbx_reservas_SelectedIndexChanged);
            // 
            // printDocument1
            // 
            this.printDocument1.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(this.printDocument1_PrintPage);
            // 
            // printPreviewDialog1
            // 
            this.printPreviewDialog1.AutoScrollMargin = new System.Drawing.Size(0, 0);
            this.printPreviewDialog1.AutoScrollMinSize = new System.Drawing.Size(0, 0);
            this.printPreviewDialog1.ClientSize = new System.Drawing.Size(400, 300);
            this.printPreviewDialog1.Enabled = true;
            this.printPreviewDialog1.Icon = ((System.Drawing.Icon)(resources.GetObject("printPreviewDialog1.Icon")));
            this.printPreviewDialog1.Name = "printPreviewDialog1";
            this.printPreviewDialog1.Visible = false;
            // 
            // btn_imprimir
            // 
            this.btn_imprimir.Image = ((System.Drawing.Image)(resources.GetObject("btn_imprimir.Image")));
            this.btn_imprimir.Location = new System.Drawing.Point(495, 35);
            this.btn_imprimir.Name = "btn_imprimir";
            this.btn_imprimir.Size = new System.Drawing.Size(83, 66);
            this.btn_imprimir.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btn_imprimir.TabIndex = 2;
            this.btn_imprimir.TabStop = false;
            this.btn_imprimir.Click += new System.EventHandler(this.btn_imprimir_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial Rounded MT Bold", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.ForestGreen;
            this.label3.Location = new System.Drawing.Point(12, 88);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(139, 24);
            this.label3.TabIndex = 34;
            this.label3.Text = "Facturación:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Arial Rounded MT Bold", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.ForestGreen;
            this.label8.Location = new System.Drawing.Point(12, 9);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(103, 24);
            this.label8.TabIndex = 35;
            this.label8.Text = "Reservas";
            // 
            // Form_Facturacion
            // 
            this.ClientSize = new System.Drawing.Size(590, 723);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.btn_imprimir);
            this.Controls.Add(this.cbx_reservas);
            this.Controls.Add(this.panel_factura);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form_Facturacion";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Facturación";
            this.Load += new System.EventHandler(this.Form_Facturacion_Load_1);
            this.panel_factura.ResumeLayout(false);
            this.panel_factura.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btn_imprimir)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer1;
        private Microsoft.VisualBasic.PowerPacks.RectangleShape rectangleShape1;
        private System.Windows.Forms.Panel panel_factura;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label_ced_cliente;
        private System.Windows.Forms.Label label_nom_cliente;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label fecha;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label_num_factura;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Panel panel3;
        private Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer2;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape2;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape1;
        private System.Windows.Forms.Label fecha_salida;
        private System.Windows.Forms.Label fecha_entrada;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label precio;
        private System.Windows.Forms.Label tarifa;
        private System.Windows.Forms.Label cantidad_noches;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label hora;
        private System.Windows.Forms.Label label13;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape3;
        private System.Windows.Forms.Label colones;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label total;
        private System.Windows.Forms.Label iva;
        private System.Windows.Forms.Label subtotal;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label20;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape4;
        private System.Windows.Forms.ComboBox cbx_reservas;
        private System.Drawing.Printing.PrintDocument printDocument1;
        private System.Windows.Forms.PrintPreviewDialog printPreviewDialog1;
        private System.Windows.Forms.PictureBox btn_imprimir;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label8;
    }
}