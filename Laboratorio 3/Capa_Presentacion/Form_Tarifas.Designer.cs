﻿
namespace Capa_Presentacion
{
    partial class Form_Tarifas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form_Tarifas));
            this.panel_principal = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.btn_reservas = new System.Windows.Forms.PictureBox();
            this.dgv_tarifas = new System.Windows.Forms.DataGridView();
            this.column_codigo_tarifa = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.column_nombre_tarifa = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.column_monto_tarifa = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Agregar = new System.Windows.Forms.DataGridViewButtonColumn();
            this.Modificar = new System.Windows.Forms.DataGridViewButtonColumn();
            this.Eliminar = new System.Windows.Forms.DataGridViewButtonColumn();
            this.panel_principal.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btn_reservas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_tarifas)).BeginInit();
            this.SuspendLayout();
            // 
            // panel_principal
            // 
            this.panel_principal.BackColor = System.Drawing.Color.ForestGreen;
            this.panel_principal.Controls.Add(this.label1);
            this.panel_principal.Controls.Add(this.btn_reservas);
            this.panel_principal.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel_principal.Location = new System.Drawing.Point(0, 0);
            this.panel_principal.Name = "panel_principal";
            this.panel_principal.Size = new System.Drawing.Size(471, 69);
            this.panel_principal.TabIndex = 8;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial Rounded MT Bold", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(145, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(255, 32);
            this.label1.TabIndex = 11;
            this.label1.Text = "Gestión de Tarifas";
            // 
            // btn_reservas
            // 
            this.btn_reservas.BackColor = System.Drawing.Color.Transparent;
            this.btn_reservas.Image = ((System.Drawing.Image)(resources.GetObject("btn_reservas.Image")));
            this.btn_reservas.Location = new System.Drawing.Point(75, 10);
            this.btn_reservas.Name = "btn_reservas";
            this.btn_reservas.Size = new System.Drawing.Size(86, 49);
            this.btn_reservas.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btn_reservas.TabIndex = 12;
            this.btn_reservas.TabStop = false;
            // 
            // dgv_tarifas
            // 
            this.dgv_tarifas.AllowUserToDeleteRows = false;
            this.dgv_tarifas.BackgroundColor = System.Drawing.Color.White;
            this.dgv_tarifas.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgv_tarifas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_tarifas.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.column_codigo_tarifa,
            this.column_nombre_tarifa,
            this.column_monto_tarifa,
            this.Agregar,
            this.Modificar,
            this.Eliminar});
            this.dgv_tarifas.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv_tarifas.Location = new System.Drawing.Point(0, 69);
            this.dgv_tarifas.Name = "dgv_tarifas";
            this.dgv_tarifas.RowHeadersVisible = false;
            this.dgv_tarifas.Size = new System.Drawing.Size(471, 153);
            this.dgv_tarifas.TabIndex = 9;
            this.dgv_tarifas.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dtg_tarifas_CellContentClick);
            this.dgv_tarifas.CellPainting += new System.Windows.Forms.DataGridViewCellPaintingEventHandler(this.dtg_tarifas_CellPainting);
            // 
            // column_codigo_tarifa
            // 
            this.column_codigo_tarifa.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.column_codigo_tarifa.Frozen = true;
            this.column_codigo_tarifa.HeaderText = "Código de Tarifa";
            this.column_codigo_tarifa.Name = "column_codigo_tarifa";
            this.column_codigo_tarifa.ReadOnly = true;
            this.column_codigo_tarifa.Width = 130;
            // 
            // column_nombre_tarifa
            // 
            this.column_nombre_tarifa.Frozen = true;
            this.column_nombre_tarifa.HeaderText = "Tarifa";
            this.column_nombre_tarifa.Name = "column_nombre_tarifa";
            // 
            // column_monto_tarifa
            // 
            this.column_monto_tarifa.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.column_monto_tarifa.Frozen = true;
            this.column_monto_tarifa.HeaderText = "Precio de la Tarifa";
            this.column_monto_tarifa.Name = "column_monto_tarifa";
            this.column_monto_tarifa.Width = 130;
            // 
            // Agregar
            // 
            this.Agregar.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.Agregar.Frozen = true;
            this.Agregar.HeaderText = "";
            this.Agregar.Name = "Agregar";
            this.Agregar.Width = 36;
            // 
            // Modificar
            // 
            this.Modificar.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.Modificar.Frozen = true;
            this.Modificar.HeaderText = "";
            this.Modificar.Name = "Modificar";
            this.Modificar.Width = 36;
            // 
            // Eliminar
            // 
            this.Eliminar.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.Eliminar.Frozen = true;
            this.Eliminar.HeaderText = "";
            this.Eliminar.Name = "Eliminar";
            this.Eliminar.Width = 36;
            // 
            // Form_Tarifas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(471, 222);
            this.Controls.Add(this.dgv_tarifas);
            this.Controls.Add(this.panel_principal);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Location = new System.Drawing.Point(250, 90);
            this.Name = "Form_Tarifas";
            this.Opacity = 0.9D;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Tarifas";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form_Tarifas_FormClosed);
            this.panel_principal.ResumeLayout(false);
            this.panel_principal.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btn_reservas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_tarifas)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel_principal;
        private System.Windows.Forms.PictureBox btn_reservas;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dgv_tarifas;
        private System.Windows.Forms.DataGridViewTextBoxColumn column_codigo_tarifa;
        private System.Windows.Forms.DataGridViewTextBoxColumn column_nombre_tarifa;
        private System.Windows.Forms.DataGridViewTextBoxColumn column_monto_tarifa;
        private System.Windows.Forms.DataGridViewButtonColumn Agregar;
        private System.Windows.Forms.DataGridViewButtonColumn Modificar;
        private System.Windows.Forms.DataGridViewButtonColumn Eliminar;
    }
}