﻿using Capa_Negocio;
using Objetos;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Capa_Presentacion
{
    public partial class Form_Reportes : Form
    {
        List<Reporte> datos = new List<Reporte>();
        List<Reporte> datos_rep2_1 = new List<Reporte>();
        List<Reporte> datos_rep2_2 = new List<Reporte>();
        NG_Reporte ng_reporte = new NG_Reporte();
        Reporte reporte = new Reporte();
        public Form_Reportes()
        {
            InitializeComponent();
        }
        public void Reporte_1()
        {
            grafico_reporte_1.Titles.Add("Porcentajes de Reservas Realizadas por cada Usuario");
            datos = ng_reporte.Reporte1();
            string[] usuarios = {datos[0].nombre_usuario, datos[1].nombre_usuario};
            int[] cantidad = { datos[0].cantidad_reservas, datos[1].cantidad_reservas};
            for (int i = 0; i < usuarios.Length; i++)
            {
                grafico_reporte_1.Series["Series1"].Points.AddXY(usuarios[i], cantidad[i]);
            }
        }
        public void Reporte_2()
        {
            grafico_reporte_2.Titles.Add("Porcentajes de Reservas Realizadas por Nacionales o Extranjeros");
            datos_rep2_1 = ng_reporte.Reporte2_1();
            datos_rep2_2 = ng_reporte.Reporte2_2();
            string[] nacionaliades = { "Nacionales","Extranjeros" };
            int[] cantidad = { datos_rep2_1[0].cantidad_reservas, datos_rep2_2[0].cantidad_reservas };
            for (int i = 0; i < nacionaliades.Length; i++)
            {
                grafico_reporte_2.Series["Series1"].Points.AddXY(nacionaliades[i], cantidad[i]);
            }
        }
        public void Reporte_3()
        {
            dgv_reporte_2.Rows.Clear();
            reporte.fecha_entrada = dtp_desde.Value;
            reporte.fecha_salida = dtp_hasta.Value;
            datos = ng_reporte.Reporte3(reporte);
            if(datos.Count == 0)
            {
                MessageBox.Show("No hay Reservas en el Rango Seleccionado", "Información", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            else
            {
                for (int i = 0; i < datos.Count; i++)
                {
                    DataGridViewRow fila = new DataGridViewRow();
                    fila.CreateCells(dgv_reporte_2);
                    fila.Cells[0].Value = datos[i].cedula.ToString();
                    fila.Cells[1].Value = datos[i].nombre_cliente;
                    fila.Cells[2].Value = datos[i].fecha_entrada.ToString("dd/MM/yyyy");
                    fila.Cells[3].Value = datos[i].fecha_salida.ToString("dd/MM/yyyy");
                    fila.Cells[4].Value = datos[i].cantidad_noches;
                    fila.Cells[5].Value = datos[i].cantidad_personas;
                    fila.Cells[6].Value = datos[i].monto_total;
                    fila.Cells[7].Value = datos[i].nombre_usuario;
                    dgv_reporte_2.Rows.Add(fila);
                }
            }
        }
        private void Form_Reportes_Load(object sender, EventArgs e)
        {
            Reporte_1();
            Reporte_2();
        }
        private void btn_generar_Click_1(object sender, EventArgs e)
        {
            if(dtp_hasta.Value <= dtp_desde.Value)
            {
                MessageBox.Show("La fecha del rango hasta debe de ser mayor a la del rango desde", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                Reporte_3();
            }
        }

        private void grafico_reporte_1_Click(object sender, EventArgs e)
        {

        }

        private void dgv_reporte_2_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
