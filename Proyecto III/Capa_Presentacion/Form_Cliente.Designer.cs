﻿
namespace Capa_Presentacion
{
    partial class Form_Cliente
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form_Cliente));
            this.panel_barra_menu = new System.Windows.Forms.Panel();
            this.label6 = new System.Windows.Forms.Label();
            this.btn_como_llegar = new System.Windows.Forms.PictureBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btn_tipo_cambio = new System.Windows.Forms.PictureBox();
            this.btn_facturas = new System.Windows.Forms.PictureBox();
            this.btn_reportes = new System.Windows.Forms.PictureBox();
            this.btn_tarifas = new System.Windows.Forms.PictureBox();
            this.btn_salir = new System.Windows.Forms.PictureBox();
            this.btn_reservas = new System.Windows.Forms.PictureBox();
            this.panel_principal = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label7 = new System.Windows.Forms.Label();
            this.btn_com_ll = new System.Windows.Forms.PictureBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.btn_tip_cam = new System.Windows.Forms.PictureBox();
            this.btn_exit = new System.Windows.Forms.PictureBox();
            this.btn_reservar = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.panel_barra_menu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btn_como_llegar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_tipo_cambio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_facturas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_reportes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_tarifas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_salir)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_reservas)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btn_com_ll)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_tip_cam)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_exit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_reservar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.SuspendLayout();
            // 
            // panel_barra_menu
            // 
            this.panel_barra_menu.BackColor = System.Drawing.Color.ForestGreen;
            this.panel_barra_menu.Controls.Add(this.label6);
            this.panel_barra_menu.Controls.Add(this.btn_como_llegar);
            this.panel_barra_menu.Controls.Add(this.label5);
            this.panel_barra_menu.Controls.Add(this.label4);
            this.panel_barra_menu.Controls.Add(this.label3);
            this.panel_barra_menu.Controls.Add(this.label2);
            this.panel_barra_menu.Controls.Add(this.label1);
            this.panel_barra_menu.Controls.Add(this.btn_tipo_cambio);
            this.panel_barra_menu.Controls.Add(this.btn_facturas);
            this.panel_barra_menu.Controls.Add(this.btn_reportes);
            this.panel_barra_menu.Controls.Add(this.btn_tarifas);
            this.panel_barra_menu.Controls.Add(this.btn_salir);
            this.panel_barra_menu.Controls.Add(this.btn_reservas);
            this.panel_barra_menu.Location = new System.Drawing.Point(54, -112);
            this.panel_barra_menu.Name = "panel_barra_menu";
            this.panel_barra_menu.Size = new System.Drawing.Size(861, 105);
            this.panel_barra_menu.TabIndex = 6;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Arial Rounded MT Bold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.White;
            this.label6.Location = new System.Drawing.Point(666, 79);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(109, 18);
            this.label6.TabIndex = 15;
            this.label6.Text = "Como Llegar";
            // 
            // btn_como_llegar
            // 
            this.btn_como_llegar.Image = ((System.Drawing.Image)(resources.GetObject("btn_como_llegar.Image")));
            this.btn_como_llegar.Location = new System.Drawing.Point(669, 7);
            this.btn_como_llegar.Name = "btn_como_llegar";
            this.btn_como_llegar.Size = new System.Drawing.Size(97, 67);
            this.btn_como_llegar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btn_como_llegar.TabIndex = 14;
            this.btn_como_llegar.TabStop = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Arial Rounded MT Bold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(536, 79);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(106, 18);
            this.label5.TabIndex = 13;
            this.label5.Text = "Tipo Cambio";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial Rounded MT Bold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(414, 79);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(82, 18);
            this.label4.TabIndex = 11;
            this.label4.Text = "Reportes";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial Rounded MT Bold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(284, 79);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(80, 18);
            this.label3.TabIndex = 12;
            this.label3.Text = "Facturas";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial Rounded MT Bold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(165, 79);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(63, 18);
            this.label2.TabIndex = 11;
            this.label2.Text = "Tarifas";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial Rounded MT Bold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(32, 79);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(84, 18);
            this.label1.TabIndex = 10;
            this.label1.Text = "Reservas";
            // 
            // btn_tipo_cambio
            // 
            this.btn_tipo_cambio.Image = ((System.Drawing.Image)(resources.GetObject("btn_tipo_cambio.Image")));
            this.btn_tipo_cambio.Location = new System.Drawing.Point(539, 7);
            this.btn_tipo_cambio.Name = "btn_tipo_cambio";
            this.btn_tipo_cambio.Size = new System.Drawing.Size(97, 67);
            this.btn_tipo_cambio.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btn_tipo_cambio.TabIndex = 9;
            this.btn_tipo_cambio.TabStop = false;
            // 
            // btn_facturas
            // 
            this.btn_facturas.Image = ((System.Drawing.Image)(resources.GetObject("btn_facturas.Image")));
            this.btn_facturas.Location = new System.Drawing.Point(277, 7);
            this.btn_facturas.Name = "btn_facturas";
            this.btn_facturas.Size = new System.Drawing.Size(97, 67);
            this.btn_facturas.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btn_facturas.TabIndex = 8;
            this.btn_facturas.TabStop = false;
            // 
            // btn_reportes
            // 
            this.btn_reportes.Image = ((System.Drawing.Image)(resources.GetObject("btn_reportes.Image")));
            this.btn_reportes.Location = new System.Drawing.Point(406, 7);
            this.btn_reportes.Name = "btn_reportes";
            this.btn_reportes.Size = new System.Drawing.Size(97, 67);
            this.btn_reportes.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btn_reportes.TabIndex = 6;
            this.btn_reportes.TabStop = false;
            // 
            // btn_tarifas
            // 
            this.btn_tarifas.Image = ((System.Drawing.Image)(resources.GetObject("btn_tarifas.Image")));
            this.btn_tarifas.Location = new System.Drawing.Point(149, 7);
            this.btn_tarifas.Name = "btn_tarifas";
            this.btn_tarifas.Size = new System.Drawing.Size(97, 67);
            this.btn_tarifas.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btn_tarifas.TabIndex = 7;
            this.btn_tarifas.TabStop = false;
            // 
            // btn_salir
            // 
            this.btn_salir.Image = ((System.Drawing.Image)(resources.GetObject("btn_salir.Image")));
            this.btn_salir.Location = new System.Drawing.Point(813, 12);
            this.btn_salir.Name = "btn_salir";
            this.btn_salir.Size = new System.Drawing.Size(33, 23);
            this.btn_salir.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btn_salir.TabIndex = 3;
            this.btn_salir.TabStop = false;
            // 
            // btn_reservas
            // 
            this.btn_reservas.Image = ((System.Drawing.Image)(resources.GetObject("btn_reservas.Image")));
            this.btn_reservas.Location = new System.Drawing.Point(24, 7);
            this.btn_reservas.Name = "btn_reservas";
            this.btn_reservas.Size = new System.Drawing.Size(97, 67);
            this.btn_reservas.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btn_reservas.TabIndex = 5;
            this.btn_reservas.TabStop = false;
            // 
            // panel_principal
            // 
            this.panel_principal.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel_principal.Location = new System.Drawing.Point(0, 111);
            this.panel_principal.Name = "panel_principal";
            this.panel_principal.Size = new System.Drawing.Size(778, 369);
            this.panel_principal.TabIndex = 8;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.ForestGreen;
            this.panel3.Controls.Add(this.label7);
            this.panel3.Controls.Add(this.btn_com_ll);
            this.panel3.Controls.Add(this.label8);
            this.panel3.Controls.Add(this.label12);
            this.panel3.Controls.Add(this.btn_tip_cam);
            this.panel3.Controls.Add(this.btn_exit);
            this.panel3.Controls.Add(this.btn_reservar);
            this.panel3.Location = new System.Drawing.Point(163, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(615, 106);
            this.panel3.TabIndex = 7;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Arial Rounded MT Bold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.White;
            this.label7.Location = new System.Drawing.Point(414, 79);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(109, 18);
            this.label7.TabIndex = 15;
            this.label7.Text = "Como Llegar";
            // 
            // btn_com_ll
            // 
            this.btn_com_ll.Image = ((System.Drawing.Image)(resources.GetObject("btn_com_ll.Image")));
            this.btn_com_ll.Location = new System.Drawing.Point(417, 7);
            this.btn_com_ll.Name = "btn_com_ll";
            this.btn_com_ll.Size = new System.Drawing.Size(97, 67);
            this.btn_com_ll.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btn_com_ll.TabIndex = 14;
            this.btn_com_ll.TabStop = false;
            this.btn_com_ll.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Arial Rounded MT Bold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.White;
            this.label8.Location = new System.Drawing.Point(223, 79);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(106, 18);
            this.label8.TabIndex = 13;
            this.label8.Text = "Tipo Cambio";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Arial Rounded MT Bold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.White;
            this.label12.Location = new System.Drawing.Point(32, 79);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(84, 18);
            this.label12.TabIndex = 10;
            this.label12.Text = "Reservas";
            // 
            // btn_tip_cam
            // 
            this.btn_tip_cam.Image = ((System.Drawing.Image)(resources.GetObject("btn_tip_cam.Image")));
            this.btn_tip_cam.Location = new System.Drawing.Point(226, 7);
            this.btn_tip_cam.Name = "btn_tip_cam";
            this.btn_tip_cam.Size = new System.Drawing.Size(97, 67);
            this.btn_tip_cam.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btn_tip_cam.TabIndex = 9;
            this.btn_tip_cam.TabStop = false;
            this.btn_tip_cam.Click += new System.EventHandler(this.pictureBox2_Click);
            // 
            // btn_exit
            // 
            this.btn_exit.Image = ((System.Drawing.Image)(resources.GetObject("btn_exit.Image")));
            this.btn_exit.Location = new System.Drawing.Point(570, 12);
            this.btn_exit.Name = "btn_exit";
            this.btn_exit.Size = new System.Drawing.Size(33, 23);
            this.btn_exit.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btn_exit.TabIndex = 3;
            this.btn_exit.TabStop = false;
            this.btn_exit.Click += new System.EventHandler(this.pictureBox6_Click);
            // 
            // btn_reservar
            // 
            this.btn_reservar.Image = ((System.Drawing.Image)(resources.GetObject("btn_reservar.Image")));
            this.btn_reservar.Location = new System.Drawing.Point(24, 7);
            this.btn_reservar.Name = "btn_reservar";
            this.btn_reservar.Size = new System.Drawing.Size(97, 67);
            this.btn_reservar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btn_reservar.TabIndex = 5;
            this.btn_reservar.TabStop = false;
            this.btn_reservar.Click += new System.EventHandler(this.pictureBox7_Click);
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox3.Image")));
            this.pictureBox3.Location = new System.Drawing.Point(12, 7);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(134, 99);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox3.TabIndex = 9;
            this.pictureBox3.TabStop = false;
            // 
            // Form_Cliente
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(778, 480);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.panel_principal);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel_barra_menu);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form_Cliente";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form_Cliente";
            this.panel_barra_menu.ResumeLayout(false);
            this.panel_barra_menu.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btn_como_llegar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_tipo_cambio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_facturas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_reportes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_tarifas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_salir)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_reservas)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btn_com_ll)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_tip_cam)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_exit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_reservar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel panel_barra_menu;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.PictureBox btn_como_llegar;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox btn_tipo_cambio;
        private System.Windows.Forms.PictureBox btn_facturas;
        private System.Windows.Forms.PictureBox btn_reportes;
        private System.Windows.Forms.PictureBox btn_tarifas;
        private System.Windows.Forms.PictureBox btn_salir;
        private System.Windows.Forms.PictureBox btn_reservas;
        private System.Windows.Forms.Panel panel_principal;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.PictureBox btn_com_ll;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.PictureBox btn_tip_cam;
        private System.Windows.Forms.PictureBox btn_exit;
        private System.Windows.Forms.PictureBox btn_reservar;
        private System.Windows.Forms.PictureBox pictureBox3;
    }
}