﻿using Capa_Negocio;
using Objetos;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Capa_Presentacion
{
    public partial class Form_Reservas : Form
    {
        Reserva reserva = new Reserva();
        Cliente cliente = new Cliente();
        NG_Reserva ng_reserva = new NG_Reserva();
        NG_Cliente ng_cliente = new NG_Cliente();
        NG_Tarifa ng_tarifa = new NG_Tarifa();
        List<Cliente> datos_cliente = new List<Cliente>();
        public Form_Reservas()
        {
            InitializeComponent();
            Cargar_Combo_Clientes();
        }
        public string Obtener_Genero()
        {
            string genero = "";

            if (rdb_masculino.Checked)
            {
                genero = "Masculino";
            }
            else if (rdb_femenino.Checked)
            {
                genero = "Femenino";
            }

            return genero;
        }
        public void Cargar_Combo_Tarifa()
        {
            cbx_tarifa.DataSource = ng_tarifa.Obtener_Tarifas();
            cbx_tarifa.DisplayMember = "monto_tarifa";
            cbx_tarifa.ValueMember = "codigo_tarifa";
        }
        public void Cargar_Combo_Clientes()
        {
            cbx_clientes.DataSource = ng_cliente.Obtener_Clientes();
            cbx_clientes.DisplayMember = "nombre_cliente";
            cbx_clientes.ValueMember = "cedula_cliente";
        }
        public int Resta(int a, int b)
        {
            com.dneonline.www.Calculator calculadora = new com.dneonline.www.Calculator();
            int resultado = calculadora.Subtract(a, b);
            return resultado;
        }
        public int Multiplicacion(int a, int b)
        {
            com.dneonline.www.Calculator calculadora = new com.dneonline.www.Calculator();
            int resultado = calculadora.Multiply(a, b);
            return resultado;
        }
        public int Division(int a, int b)
        {
            com.dneonline.www.Calculator calculadora = new com.dneonline.www.Calculator();
            int resultado = calculadora.Divide(a, b);
            return resultado;
        }
        public int Suma(int a, int b)
        {
            com.dneonline.www.Calculator calculadora = new com.dneonline.www.Calculator();
            int resultado = calculadora.Add(a, b);
            return resultado;
        }
        private int Consultar_Tarifa(int cod_ta)
        {
            int tarifa = ng_tarifa.Obtener_Monto_Tarifa(cod_ta);
            return tarifa;
        }
        public void Obtener_Datos()
        {
            reserva.codigo_usuario = Form_Inicio.codigo_usuario;
            reserva.fecha_entrada = dtp_fecha_entrada.Value;
            reserva.fecha_salida = dtp_fecha_salida.Value;
            reserva.cantidad_noches = Convert.ToInt32((dtp_fecha_salida.Value - dtp_fecha_entrada.Value).TotalDays);
            reserva.cantidad_personas = Convert.ToInt32(cbx_cantidad_personas.SelectedItem);
            reserva.tarifa = Convert.ToInt32(cbx_tarifa.SelectedValue);
            int tarifa = Consultar_Tarifa(Convert.ToInt32(cbx_tarifa.SelectedValue));
            reserva.monto_inicial = Multiplicacion(tarifa, reserva.cantidad_noches);
            reserva.monto_impuesto = Multiplicacion(reserva.monto_inicial, 13);
            reserva.monto_impuesto = Division(reserva.monto_impuesto, 100);
            reserva.monto_final = Suma(reserva.monto_inicial, reserva.monto_impuesto);
            cliente.cedula_cliente = Convert.ToInt32(txt_cedula.Text);
            cliente.nombre_cliente = txt_nombre.Text;
            cliente.genero = Obtener_Genero();
            cliente.telefono_movil = Convert.ToInt32(txt_telefono_movil.Text);
            cliente.correo_electronico = txt_correo_electronico.Text;
        }
        private void btn_consultar_Click(object sender, EventArgs e)
        {
            if (dtp_fecha_entrada.Value == null || dtp_fecha_salida.Value == null || cbx_cantidad_personas.SelectedItem == null 
                || cbx_tarifa.SelectedItem == null)
            {
                MessageBox.Show("Debe de ingresar información en los campos correspondientes", "Advertencia", MessageBoxButtons.OK, 
                    MessageBoxIcon.Warning);
            }
            else if (dtp_fecha_salida.Value < dtp_fecha_entrada.Value)
            {
                MessageBox.Show("La fecha de entradad debe ser menor a la de la salida", "Error", MessageBoxButtons.OK,
                   MessageBoxIcon.Error);
            }
            else
            {
                int resultado = ng_reserva.Consulta_Disponibilidad(dtp_fecha_entrada.Value);
                if (resultado == 0)
                {
                    MessageBox.Show("Proceda a seleccionar los datos del cliente", "Información", MessageBoxButtons.OK,
                        MessageBoxIcon.Information);
                    cbx_clientes.Enabled = true;
                    txt_cedula.Enabled = true;
                    txt_nombre.Enabled = true;
                    rdb_masculino.Enabled = true;
                    rdb_femenino.Enabled = true;
                    txt_telefono_movil.Enabled = true;
                    txt_correo_electronico.Enabled = true;
                }
                else
                {
                    MessageBox.Show("Ya existe una reservación en las fechas seleccionadas", "Advertencia", MessageBoxButtons.OK, 
                        MessageBoxIcon.Warning);
                }
                
            }
        }
        private void txt_cedula_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar >= 32 && e.KeyChar <= 47) || (e.KeyChar >= 58 && e.KeyChar <= 255))
            {
                MessageBox.Show("Solo se pueden ingresar números", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                e.Handled = true;
                return;
            }
        }
        private void btn_confirmar_reserva_Click(object sender, EventArgs e)
        {
            if(txt_cedula.Text.Equals("") && txt_nombre.Text.Equals("") && txt_telefono_movil.Text.Equals("") 
                && txt_correo_electronico.Text.Equals(""))
            {
                MessageBox.Show("Debe de ingresar información en los campos correspondientes", "Advertencia", 
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                Obtener_Datos();
                int resultado = ng_cliente.Consultar_Existencia_Cliente(cliente);
                if(resultado == 0)
                {
                    ng_cliente.Agregar_Cliente(cliente);
                    ng_reserva.Agregar_Reserva(reserva, cliente);
                    MessageBox.Show("Reserva realizada correctamente", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else if(resultado > 0)
                {
                    ng_reserva.Agregar_Reserva(reserva, cliente);
                    MessageBox.Show("Reserva realizada correctamente", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }
        private void Form_Reservas_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.Hide();
        }
        private void Form_Reservas_Load(object sender, EventArgs e)
        {
            Cargar_Combo_Tarifa();
            
        }
        private void rbt_clientes_existentes_CheckedChanged(object sender, EventArgs e)
        {
            
        }
        private void cbx_clientes_SelectedIndexChanged(object sender, EventArgs e)
        {
            int index = Convert.ToInt32(cbx_clientes.SelectedIndex);

            datos_cliente = ng_cliente.Obtener_Clientes();

            txt_cedula.Text = Convert.ToString(datos_cliente[index].cedula_cliente);
            txt_nombre.Text = datos_cliente[index].nombre_cliente;
            //txt_telefono_fijo.Text = Convert.ToString(datos_cliente[index].telefono_fijo);
            txt_telefono_movil.Text = Convert.ToString(datos_cliente[index].telefono_movil);
            txt_correo_electronico.Text = datos_cliente[index].correo_electronico;
            if (datos_cliente[index].genero.Equals("Masculino"))
            {
                rdb_masculino.Checked = true;
            }
            else if (datos_cliente[index].genero.Equals("Femenino"))
            {
                rdb_femenino.Checked = true;
            }
        }
    }
}
