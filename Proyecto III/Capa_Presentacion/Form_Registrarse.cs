﻿using Capa_Negocio;
using Capa_Objetos;
using Objetos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace Capa_Presentacion
{
    public partial class Form_Registrarse : Form
    {
        public Form_Registrarse()
        {
            InitializeComponent();
            Consultar_Informacion();
            Llenar_Combo_Paises();
        }

        //Declaración de las instancias a utilizar en la clase
        NG_Cliente ng_cliente = new NG_Cliente();
        NG_Usuario ng_usuario = new NG_Usuario();
        Cliente cliente = new Cliente();
        Usuario usuario = new Usuario();
        List<Aeropuerto> informacion = new List<Aeropuerto>();
        object[] arreglo_aeropuertos;
        //Método con el cual se realiza el llenado del combo box de países
        private void Llenar_Combo_Paises()
        {
            cbx_pais_residencia.DataSource = informacion;
            cbx_pais_residencia.DisplayMember = "pais";
            cbx_pais_residencia.ValueMember = "pais";
        }
        //Método en el cual se realiza la consulta de la información en el Web Service
        private void Consultar_Informacion()
        {
            service_reference_airports.AirportsV1SoapService aa = new service_reference_airports.AirportsV1SoapService();
            arreglo_aeropuertos = aa.allAirports("18c94536", "071833e8d57b8998533358a15660f375", "useHttpErrors");

            for (int i = 0; i < arreglo_aeropuertos.Length; i++)
            {
                var item = arreglo_aeropuertos[i];
                var propertyInfoActive = item.GetType().GetProperty("active");
                var activo = propertyInfoActive.GetValue(item, null);

                if (activo.Equals(true))
                {
                    var propertyInfoCountry = item.GetType().GetProperty("countryName");

                    Aeropuerto aeropuerto = new Aeropuerto();

                    aeropuerto.pais = propertyInfoCountry.GetValue(item, null).ToString();

                    bool existe = informacion.Any(x => x.pais.Equals(aeropuerto.pais));

                    if (existe == false)
                    {
                        informacion.Add(aeropuerto);
                    }
                }
            }
        }
        //Método que retorna el valor seleccionado de los RadioButtons de género
        private string Genero()
        {
            string genero = "";
            if (rbt_masculino.Checked == true)
            {
                genero = "Masculino";
            }
            if (rbt_femenino.Checked == true)
            {
                genero = "Femenino";
            }
            return genero;
        }
        //Método el cual realiza la obtención de datos de la interfaz grafica y
        //los asignas a los respectivos objetos
        private void Obtener_Datos()
        {
            //Datos Personales
            cliente.cedula_cliente = Convert.ToInt32(txt_cedula.Text);
            cliente.nombre_cliente = txt_nombre.Text;
            cliente.genero = Genero();
            cliente.telefono_movil = Convert.ToInt32(txt_telefono.Text);
            cliente.pais_residencia = cbx_pais_residencia.SelectedValue.ToString();
            cliente.correo_electronico = txt_correo_electronico.Text;
            //Datos de Inicio Sesión
            usuario.usuario = txt_usuario.Text;
            usuario.contrasenna = txt_contrasenna.Text;
            usuario.rol = "Cliente";
        }
        //Método el cual realiza una limpieza de los datos ingresados en la in-
        //terfaz gráfica
        private void Limpiar_Datos()
        {
            txt_cedula.Clear();
            txt_nombre.Clear();
            txt_telefono.Clear();
            txt_usuario.Clear();
            txt_contrasenna.Clear();
            txt_correo_electronico.Clear();
        }
        //Método en el cual se realiza el registro de los datos respectivos
        private void btn_registrarse_Click(object sender, EventArgs e)
        {
            if (txt_cedula.Text.Equals("") && txt_nombre.Text.Equals("") && txt_telefono.Text.Equals("")
               && txt_correo_electronico.Text.Equals("") && txt_usuario.Text.Equals("")
               && txt_contrasenna.Text.Equals(""))
            {
                MessageBox.Show("Error, no puedes dejar campos vacíos", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                Obtener_Datos();
                ng_cliente.Agregar_Cliente(cliente);
                ng_usuario.Agregar_Usuario(usuario);
                MessageBox.Show("Datos Ingresados Correctamente", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information);
                Limpiar_Datos();
            }
        }

        private void btn_salir_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form_Inicio frm_i = new Form_Inicio();
            frm_i.Show();
        }
    }
}
