﻿using Capa_Negocio;
using Capa_Objetos;
using Objetos;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace Capa_Presentacion
{
    public partial class Form_Gestion_Clientes : Form
    {

        List<Cliente> clientes = new List<Cliente>();
        NG_Cliente ng_cliente = new NG_Cliente();
        Cliente cliente = new Cliente();
        List<Aeropuerto> informacion = new List<Aeropuerto>();
        object[] arreglo_aeropuertos;

        public Form_Gestion_Clientes()
        {
            InitializeComponent();
            Consultar_Informacion();
            Cargar_Combo_Tarifa();
            Mostrar_Clientes();
        }
        public void Cargar_Combo_Tarifa()
        {
            DataGridViewComboBoxColumn cbx_tarifa = dgv_clientes.Columns["column_pais_residencia"] as DataGridViewComboBoxColumn;
            cbx_tarifa.DataSource = informacion;
            cbx_tarifa.DisplayMember = "pais";
            cbx_tarifa.ValueMember = "pais";
        }
        //Método en el cual se realiza la consulta de la información en el Web Service
        private void Consultar_Informacion()
        {
            service_reference_airports.AirportsV1SoapService aa = new service_reference_airports.AirportsV1SoapService();
            arreglo_aeropuertos = aa.allAirports("18c94536", "071833e8d57b8998533358a15660f375", "useHttpErrors");

            for (int i = 0; i < arreglo_aeropuertos.Length; i++)
            {
                var item = arreglo_aeropuertos[i];
                var propertyInfoActive = item.GetType().GetProperty("active");
                var activo = propertyInfoActive.GetValue(item, null);

                if (activo.Equals(true))
                {
                    var propertyInfoCountry = item.GetType().GetProperty("countryName");

                    Aeropuerto aeropuerto = new Aeropuerto();

                    aeropuerto.pais = propertyInfoCountry.GetValue(item, null).ToString();

                    bool existe = informacion.Any(x => x.pais.Equals(aeropuerto.pais));

                    if (existe == false)
                    {
                        informacion.Add(aeropuerto);
                    }
                }
            }
        }
        public void Mostrar_Clientes()
        {
            clientes = ng_cliente.Obtener_Clientes();
            for (int i = 0; i < clientes.Count; i++)
            {
                DataGridViewRow fila = new DataGridViewRow();
                fila.CreateCells(dgv_clientes);
                fila.Cells[0].Value = clientes[i].codigo_cliente;
                fila.Cells[1].Value = clientes[i].cedula_cliente;
                fila.Cells[2].Value = clientes[i].nombre_cliente;
                fila.Cells[3].Value = clientes[i].genero;
                fila.Cells[4].Value = clientes[i].pais_residencia;
                fila.Cells[5].Value = clientes[i].telefono_movil;
                fila.Cells[6].Value = clientes[i].correo_electronico;
                dgv_clientes.Rows.Add(fila);
            }
        }
        public void Obtener_Datos(DataGridViewCellEventArgs e)
        {
            DataGridViewRow row = dgv_clientes.Rows[e.RowIndex];
            cliente.codigo_cliente = Convert.ToInt32(row.Cells["column_codigo_cliente"].Value.ToString());
            cliente.cedula_cliente = Convert.ToInt32(row.Cells["column_cedula_cliente"].Value);
            cliente.nombre_cliente = row.Cells["column_nombre"].Value.ToString();
            cliente.genero = row.Cells["column_genero"].Value.ToString();
            cliente.pais_residencia = row.Cells["column_pais_residencia"].Value.ToString();
            cliente.telefono_movil = Convert.ToInt32(row.Cells["column_telefono_movil"].Value);
            cliente.correo_electronico = row.Cells["column_correo_electronico"].Value.ToString();
        }
        private void Form_Gestion_Clientes_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.Hide();
        }
        private void dgv_tarifas_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 7)
            {
                DialogResult respuesta = MessageBox.Show("¿DESEA MODIFICAR EL CLIENTE?", "¿MODIFICAR CLIENTE?", MessageBoxButtons.YesNo, MessageBoxIcon.Asterisk);

                if (respuesta == DialogResult.Yes)
                {
                    Obtener_Datos(e);
                    ng_cliente.Modificar_Cliente(cliente);
                    dgv_clientes.Rows.Clear();
                    Mostrar_Clientes();
                }
            }
            if (e.ColumnIndex == 8)
            {
                DialogResult respuesta = MessageBox.Show("¿DESEA ELIMINAR EL CLIENTE?", "¿ELIMINAR CLIENTE?", MessageBoxButtons.YesNo, MessageBoxIcon.Asterisk);

                if(respuesta == DialogResult.Yes)
                {
                    Obtener_Datos(e);
                    ng_cliente.Eliminar_Cliente(cliente);
                    dgv_clientes.Rows.Clear();
                    Mostrar_Clientes();
                }
            }
        }
        private void dgv_tarifas_CellPainting(object sender, DataGridViewCellPaintingEventArgs e)
        {
            if (e.ColumnIndex >= 0 && this.dgv_clientes.Columns[e.ColumnIndex].Name == "Modificar" && e.RowIndex >= 0)
            {
                e.Paint(e.CellBounds, DataGridViewPaintParts.All);

                DataGridViewButtonCell btn_modificar = this.dgv_clientes.Rows[e.RowIndex].Cells["Modificar"] as DataGridViewButtonCell;
                Icon modificar = new Icon(Environment.CurrentDirectory + @"\icono_modificar.ico");
                e.Graphics.DrawIcon(modificar, e.CellBounds.Left + 3, e.CellBounds.Top + 3);


                this.dgv_clientes.Rows[e.RowIndex].Height = modificar.Height + 10;
                this.dgv_clientes.Columns[e.ColumnIndex].Width = modificar.Width + 10;

                e.Handled = true;
            }

            if (e.ColumnIndex >= 0 && this.dgv_clientes.Columns[e.ColumnIndex].Name == "Eliminar" && e.RowIndex >= 0)
            {
                e.Paint(e.CellBounds, DataGridViewPaintParts.All);

                DataGridViewButtonCell btn_eliminar = this.dgv_clientes.Rows[e.RowIndex].Cells["Eliminar"] as DataGridViewButtonCell;
                Icon eliminar = new Icon(Environment.CurrentDirectory + @"\icono_eliminar.ico");
                e.Graphics.DrawIcon(eliminar, e.CellBounds.Left + 3, e.CellBounds.Top + 3);

                this.dgv_clientes.Rows[e.RowIndex].Height = eliminar.Height + 10;
                this.dgv_clientes.Columns[e.ColumnIndex].Width = eliminar.Width + 10;

                e.Handled = true;
            }
        }
        private void dgv_clientes_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {

        }
    }
}
