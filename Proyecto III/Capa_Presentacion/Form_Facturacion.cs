﻿using Capa_Negocio;
using Objetos;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Windows.Forms;
using Rectangle = System.Drawing.Rectangle;

namespace Capa_Presentacion
{
    public partial class Form_Facturacion : Form
    {
        private Bitmap memoria_imagen;
        NG_Reserva ng_reservas = new NG_Reserva();
        List<Reporte> reservas = new List<Reporte>();
        Tipo_Cambio tipo_cambio = new Tipo_Cambio();
        int c = 00001;
        public Form_Facturacion()
        {
            InitializeComponent();
            Obtener_Tipo_Cambio_DataSet();
        }
        public void Obtener_Tipo_Cambio_DataSet()
        {
            cr.fi.bccr.gee.wsindicadoreseconomicos dato = new cr.fi.bccr.gee.wsindicadoreseconomicos();
            DataSet tipoCambio = dato.ObtenerIndicadoresEconomicos("317", DateTime.Now.ToString("dd/MM/yyyy"), DateTime.Now.ToString("dd/MM/yyyy"),
                "Randy Corea", "N", "randycoreag27@gmail.com", "OC74000PGR");
            tipo_cambio.tipo_cambio = Convert.ToInt32(tipoCambio.Tables[0].Rows[0].ItemArray[2]);
        }
        public int Multiplicacion(int a, int b)
        {
            com.dneonline.www.Calculator calculadora = new com.dneonline.www.Calculator();
            int resultado = calculadora.Multiply(a, b);
            return resultado;
        }
        public int Division(int a, int b)
        {
            com.dneonline.www.Calculator calculadora = new com.dneonline.www.Calculator();
            int resultado = calculadora.Divide(a, b);
            return resultado;
        }
        public int Suma(int a, int b)
        {
            com.dneonline.www.Calculator calculadora = new com.dneonline.www.Calculator();
            int resultado = calculadora.Add(a, b);
            return resultado;
        }
        public void Cargar_Combo_Reservas()
        {
            cbx_reservas.DataSource = ng_reservas.Facturacion();
            cbx_reservas.DisplayMember = "codigo_reserva";
            cbx_reservas.ValueMember = "codigo_reserva";
        }
        private void Imprimir(Panel p)
        {
            PrinterSettings ps = new PrinterSettings();
            this.panel_factura = p;
            Area_Imprimir(p);
            printPreviewDialog1.Document = printDocument1;
            printDocument1.PrintPage += new PrintPageEventHandler(printDocument1_PrintPage);
            printPreviewDialog1.ShowDialog();
        }
        private void Area_Imprimir(Panel p)
        {
            memoria_imagen = new Bitmap(panel_factura.Width, panel_factura.Height);
            panel_factura.DrawToBitmap(memoria_imagen, new Rectangle(0, 0, panel_factura.Width, panel_factura.Height));
        }
        private void Form_Facturacion_Load_1(object sender, EventArgs e)
        {
            Cargar_Combo_Reservas();
            Obtener_Tipo_Cambio_DataSet();
            label_num_factura.Text = c.ToString();
            this.hora.Text = DateTime.Now.ToString("hh:mm:ss");
            this.fecha.Text = DateTime.Now.ToLongDateString();
        }
        private void printDocument1_PrintPage(object sender, PrintPageEventArgs e)
        {
            Rectangle area = e.PageBounds;
            e.Graphics.DrawImage(memoria_imagen, (area.Width / 2) - (panel_factura.Width / 2), panel_factura.Location.Y);
        }
        private void btn_imprimir_Click(object sender, EventArgs e)
        {
            Imprimir(this.panel_factura);
            c++;
        }
        private void cbx_reservas_SelectedIndexChanged(object sender, EventArgs e)
        {
            Obtener_Tipo_Cambio_DataSet();
            int index = cbx_reservas.SelectedIndex;
            reservas = ng_reservas.Facturacion();
            label_nom_cliente.Text = reservas[index].nombre_cliente;
            label_ced_cliente.Text = reservas[index].cedula.ToString();
            fecha_entrada.Text = reservas[index].fecha_entrada.ToString("dd/MM/yyyy");
            fecha_salida.Text = reservas[index].fecha_salida.ToString("dd/MM/yyyy");
            cantidad_noches.Text = reservas[index].cantidad_noches.ToString();
            tarifa.Text = reservas[index].monto_tarifa.ToString();
            precio.Text = Multiplicacion(Convert.ToInt32(cantidad_noches.Text), Convert.ToInt32(tarifa.Text)).ToString();
            subtotal.Text = precio.Text;
            iva.Text = Multiplicacion(Convert.ToInt32(subtotal.Text), 13).ToString();
            iva.Text = Division(Convert.ToInt32(iva.Text), 100).ToString();
            total.Text = Suma(Convert.ToInt32(subtotal.Text), Convert.ToInt32(iva.Text)).ToString();

            tipo_cambio.colones = Multiplicacion(tipo_cambio.tipo_cambio, Convert.ToInt32(total.Text));
            colones.Text = tipo_cambio.colones.ToString();
        }
    }
}
