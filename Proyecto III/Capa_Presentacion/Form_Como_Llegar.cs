﻿using Capa_Objetos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace Capa_Presentacion
{
    public partial class Form_Como_Llegar : Form
    {
        public Form_Como_Llegar()
        {
            InitializeComponent();
            Consultar_Informacion();
            Llenar_Combo_Paises();
            Aeropuertos_Nacionales();
        }
        List<Aeropuerto> informacion = new List<Aeropuerto>();
        List<Aeropuerto> aeropuertos = new List<Aeropuerto>();
        List<Aeropuerto> aeropuertos_nacionales = new List<Aeropuerto>();
        object[] arreglo_aeropuertos;
        private void Llenar_Combo_Paises()
        {
            cbx_pais_origen.DataSource = informacion;
            cbx_pais_origen.DisplayMember = "pais";
            cbx_pais_origen.ValueMember = "pais";
        }
        private void Consultar_Informacion()
        {
            service_reference_airports.AirportsV1SoapService aa = new service_reference_airports.AirportsV1SoapService();
            arreglo_aeropuertos = aa.allAirports("18c94536", "071833e8d57b8998533358a15660f375", "");

            for (int i = 0; i < arreglo_aeropuertos.Length; i++)
            {
                var item = arreglo_aeropuertos[i];
                var propertyInfoActive = item.GetType().GetProperty("active");
                var activo = propertyInfoActive.GetValue(item, null);

                if (activo.Equals(true))
                {
                    var propertyInfoCountry = item.GetType().GetProperty("countryName");

                    Aeropuerto aeropuerto = new Aeropuerto();

                    aeropuerto.pais = propertyInfoCountry.GetValue(item, null).ToString();

                    bool existe = informacion.Any(x => x.pais.Equals(aeropuerto.pais));

                    if (existe == false)
                    {
                        informacion.Add(aeropuerto);
                    }
                }
            }
        }
        private void Aeropuertos_Nacionales()
        {
            service_reference_airports.AirportsV1SoapService aa = new service_reference_airports.AirportsV1SoapService();
            arreglo_aeropuertos = aa.allAirports("18c94536", "071833e8d57b8998533358a15660f375", "");

            for (int i = 0; i < arreglo_aeropuertos.Length; i++)
            {
                var item = arreglo_aeropuertos[i];
                var propertyInfoActive = item.GetType().GetProperty("active");
                var activo = propertyInfoActive.GetValue(item, null);
                var propertyInfoCountry1 = item.GetType().GetProperty("countryName");
                var pais = propertyInfoCountry1.GetValue(item, null);

                if (activo.Equals(true) && pais.Equals("Costa Rica"))
                {
                    var propertyInfoName = item.GetType().GetProperty("name");
                    var propertyInfoCity = item.GetType().GetProperty("city");
                    var propertyInfoCountry = item.GetType().GetProperty("countryName");
                    var propertyInfoCode = item.GetType().GetProperty("fs");

                    Aeropuerto aeropuerto = new Aeropuerto();

                    aeropuerto.nombre = propertyInfoName.GetValue(item, null).ToString();
                    aeropuerto.ciudad = propertyInfoCity.GetValue(item, null).ToString();
                    aeropuerto.pais = propertyInfoCountry.GetValue(item, null).ToString();
                    aeropuerto.fs = propertyInfoCode.GetValue(item, null).ToString();

                    aeropuertos_nacionales.Add(aeropuerto);
                }
            }
        }
        private void Extraer_Aeropuertos(String pais_seleccionado)
        {
            service_reference_airports.AirportsV1SoapService aa = new service_reference_airports.AirportsV1SoapService();
            arreglo_aeropuertos = aa.allAirports("18c94536", "071833e8d57b8998533358a15660f375", "useHttpErrors");

            for (int i = 0; i < arreglo_aeropuertos.Length; i++)
            {
                var item = arreglo_aeropuertos[i];
                var propertyInfoActive = item.GetType().GetProperty("active");
                var activo = propertyInfoActive.GetValue(item, null);
                var propertyInfoCountry1 = item.GetType().GetProperty("countryName");
                var pais = propertyInfoCountry1.GetValue(item, null);

                if (activo.Equals(true) && pais.Equals(pais_seleccionado))
                {
                    var propertyInfoName = item.GetType().GetProperty("name");
                    var propertyInfoCity = item.GetType().GetProperty("city");
                    var propertyInfoCountry = item.GetType().GetProperty("countryName");
                    var propertyInfoCode = item.GetType().GetProperty("fs");

                    Aeropuerto aeropuerto = new Aeropuerto();

                    aeropuerto.nombre = propertyInfoName.GetValue(item, null).ToString();
                    aeropuerto.ciudad = propertyInfoCity.GetValue(item, null).ToString();
                    aeropuerto.pais = propertyInfoCountry.GetValue(item, null).ToString();
                    aeropuerto.fs = propertyInfoCode.GetValue(item, null).ToString();

                    aeropuertos.Add(aeropuerto);
                }
            }
        }
        private void Mostrar_Datos()
        {
            dgv_aeropuertos.Rows.Clear();
            for (int i = 0; i < aeropuertos.Count; i++)
            {
                DataGridViewRow fila = new DataGridViewRow();
                fila.CreateCells(dgv_aeropuertos);
                fila.Cells[0].Value = aeropuertos[i].nombre;
                fila.Cells[1].Value = aeropuertos[i].ciudad;
                fila.Cells[2].Value = aeropuertos[i].pais;
                dgv_aeropuertos.Rows.Add(fila);
            }
        }
        private void Mostrar_Datos_Aeropuerto_Llegada(string pais)
        {
            if (pais != "Costa Rica")
            {
                for (int i = 0; i < aeropuertos_nacionales.Count; i++)
                {
                   if (aeropuertos_nacionales[i].nombre.Equals("Juan Santamaria International Airport"))
                    {
                        DataGridViewRow fila = new DataGridViewRow();
                        fila.CreateCells(dgv_aeropuerto_llegada);
                        fila.Cells[0].Value = aeropuertos_nacionales[i].nombre;
                        fila.Cells[1].Value = aeropuertos_nacionales[i].ciudad;
                        fila.Cells[2].Value = aeropuertos_nacionales[i].pais;
                        dgv_aeropuerto_llegada.Rows.Add(fila);
                    }
                }
            }
            else
            {
                for (int i = 0; i < aeropuertos_nacionales.Count; i++)
                {
                    if (aeropuertos[i].nombre.Equals("Tambor Airport"))
                    {
                        DataGridViewRow fila = new DataGridViewRow();
                        fila.CreateCells(dgv_aeropuerto_llegada);
                        fila.Cells[0].Value = aeropuertos_nacionales[i].nombre;
                        fila.Cells[1].Value = aeropuertos_nacionales[i].ciudad;
                        fila.Cells[2].Value = aeropuertos_nacionales[i].pais;
                        dgv_aeropuerto_llegada.Rows.Add(fila);
                    }
                }
            }

        }
        private void cbx_pais_origen_SelectedIndexChanged(object sender, EventArgs e)
        {
            Extraer_Aeropuertos(cbx_pais_origen.SelectedValue.ToString());
            Mostrar_Datos();
            Mostrar_Datos_Aeropuerto_Llegada(cbx_pais_origen.SelectedValue.ToString());
        }

        private void dgv_aeropuertos_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
