﻿using System;
using System.Windows.Forms;

namespace Capa_Presentacion
{
    public partial class Form_Opciones_Reservas : Form
    {

        Form_Principal frm_p = new Form_Principal();

        public Form_Opciones_Reservas()
        {
            InitializeComponent();
        }

        private void Form_Opciones_Reservas_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.Hide();
        }

        private void btn_reservas_Click(object sender, EventArgs e)
        {
            Form_Reservas frm_r = new Form_Reservas();
            Frm_Reservas(frm_r);
            this.Hide();
        }

        private void btn_gestion_clientes_Click(object sender, EventArgs e)
        {
            Form_Gestion_Clientes frm_gc = new Form_Gestion_Clientes();
            Frm_Gestion_Clientes(frm_gc);
            this.Hide();
        }

        private void btn_gestion_reservas_Click(object sender, EventArgs e)
        {
            Form_Gestion_Reservas frm_gr = new Form_Gestion_Reservas();
            Frm_Gestion_Reservas(frm_gr);
            this.Hide();
        }
        public void Frm_Reservas(Form frm_re) 
        {
            frm_re.TopLevel = false;
            Form_Principal.panel.Controls.Add(frm_re);
            frm_re.Show();
        }
        public void Frm_Gestion_Reservas(Form frm_gre)
        {
            frm_gre.TopLevel = false;
            Form_Principal.panel.Controls.Add(frm_gre);
            frm_gre.Show();
        }
        public void Frm_Gestion_Clientes(Form frm_gc)
        {
            frm_gc.TopLevel = false;
            Form_Principal.panel.Controls.Add(frm_gc);
            frm_gc.Show();
        }

    }
}
