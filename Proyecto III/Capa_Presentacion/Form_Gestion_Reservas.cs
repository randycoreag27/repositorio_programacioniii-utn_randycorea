﻿using Capa_Negocio;
using Objetos;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace Capa_Presentacion
{
    public partial class Form_Gestion_Reservas : Form
    {
        DateTimePicker dtp = new DateTimePicker();
        Rectangle rtg;
        NG_Reserva ng_reserva = new NG_Reserva();
        NG_Tarifa ng_tarifa = new NG_Tarifa();
        List<Reserva> reservas = new List<Reserva>();
        Reserva reserva = new Reserva();
        public Form_Gestion_Reservas()
        {
            InitializeComponent();
            Agregar_DTP_a_DataGrid();
            Mostrar_Reservas();
            Cargar_Combo_Tarifa();
        }
        public void Agregar_DTP_a_DataGrid()
        {
            dgv_reservas.Controls.Add(dtp);
            dtp.Visible = false;
            dtp.Format = DateTimePickerFormat.Custom;
            dtp.TextChanged += new EventHandler(dtp_TextChange);
        }
        private int Consultar_Tarifa(int cod_ta)
        {
            int tarifa = ng_tarifa.Obtener_Monto_Tarifa(cod_ta);
            return tarifa;
        }
        public void Cargar_Combo_Tarifa()
        {
            DataGridViewComboBoxColumn cbx_tarifa = dgv_reservas.Columns["column_tarifa"] as DataGridViewComboBoxColumn;
            cbx_tarifa.DataSource = ng_tarifa.Obtener_Tarifas();
            cbx_tarifa.DisplayMember = "monto_tarifa";
            cbx_tarifa.ValueMember = "codigo_tarifa";
        }
        public void Mostrar_Reservas()
        {
            reservas = ng_reserva.Obtener_Reservas();
            for (int i = 0; i < reservas.Count; i++)
            {
                DataGridViewRow fila = new DataGridViewRow();
                fila.CreateCells(dgv_reservas);
                fila.Cells[0].Value = reservas[i].codigo_reserva;
                fila.Cells[1].Value = reservas[i].fecha_entrada.ToString("dd/MM/yyyy");
                fila.Cells[2].Value = reservas[i].fecha_salida.ToString("dd/MM/yyyy");
                fila.Cells[3].Value = reservas[i].cantidad_noches;
                fila.Cells[4].Value = reservas[i].cantidad_personas.ToString();
                fila.Cells[5].Value = reservas[i].tarifa;
                fila.Cells[6].Value = reservas[i].monto_inicial;
                fila.Cells[7].Value = reservas[i].monto_impuesto;
                fila.Cells[8].Value = reservas[i].monto_final;
                dgv_reservas.Rows.Add(fila);
            }
        }
        public int Resta(int a, int b)
        {
            com.dneonline.www.Calculator calculadora = new com.dneonline.www.Calculator();
            int resultado = calculadora.Subtract(a, b);
            return resultado;
        }
        public int Multiplicacion(int a, int b)
        {
            com.dneonline.www.Calculator calculadora = new com.dneonline.www.Calculator();
            int resultado = calculadora.Multiply(a, b);
            return resultado;
        }
        public int Division(int a, int b)
        {
            com.dneonline.www.Calculator calculadora = new com.dneonline.www.Calculator();
            int resultado = calculadora.Divide(a, b);
            return resultado;
        }
        public int Suma(int a, int b)
        {
            com.dneonline.www.Calculator calculadora = new com.dneonline.www.Calculator();
            int resultado = calculadora.Add(a, b);
            return resultado;
        }
        public void Obtener_Datos(DataGridViewCellEventArgs e)
        {
            DataGridViewRow row = dgv_reservas.Rows[e.RowIndex];
            reserva.codigo_reserva = Convert.ToInt32(row.Cells["column_codigo_reserva"].Value);
            reserva.fecha_entrada = Convert.ToDateTime(row.Cells["column_fecha_entrada"].Value);
            reserva.fecha_salida = Convert.ToDateTime(row.Cells["column_fecha_salida"].Value);
            reserva.cantidad_noches = Convert.ToInt32((reserva.fecha_salida.Date - reserva.fecha_entrada.Date).TotalDays);
            reserva.cantidad_personas = Convert.ToInt32(row.Cells["column_cantidad_personas"].Value.ToString());
            reserva.tarifa = Convert.ToInt32(row.Cells["column_tarifa"].Value);
            int tarifa = Consultar_Tarifa(Convert.ToInt32(row.Cells["column_tarifa"].Value));
            reserva.monto_inicial = Multiplicacion(tarifa, reserva.cantidad_noches);
            reserva.monto_impuesto = Multiplicacion(reserva.monto_inicial, 13);
            reserva.monto_impuesto = Division(reserva.monto_impuesto, 100);
            reserva.monto_final = Suma(reserva.monto_inicial, reserva.monto_impuesto);
        }
        private void dgv_tarifas_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 9)
            {
                DialogResult respuesta = MessageBox.Show("¿Desea modificar la reserva?", "¿Modificar Reserva?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (respuesta == DialogResult.Yes)
                {
                    Obtener_Datos(e);
                    ng_reserva.Modificar_Reserva(reserva);
                    dgv_reservas.Rows.Clear();
                    Mostrar_Reservas();
                }
            }
            if (e.ColumnIndex == 10)
            {
                DialogResult respuesta = MessageBox.Show("¿Desea eliminar la reserva?", "¿Eliminar Reserva?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (respuesta == DialogResult.Yes)
                {
                    Obtener_Datos(e);
                    ng_reserva.Eliminar_Reserva(reserva);
                    dgv_reservas.Rows.Clear();
                    Mostrar_Reservas();
                }
            }
        }
        public void dtp_TextChange(Object sender, EventArgs e)
        {
            dgv_reservas.CurrentCell.Value = dtp.Text.ToString();
        }
        private void dgv_reservas_Scroll(object sender, ScrollEventArgs e)
        {
            dtp.Visible = false;
        }
        private void dgv_reservas_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            switch (dgv_reservas.Columns[e.ColumnIndex].Name)
            {
                case "column_fecha_entrada":

                    rtg = dgv_reservas.GetCellDisplayRectangle(e.ColumnIndex, e.RowIndex, true);
                    dtp.Size = new Size(rtg.Width, rtg.Height);
                    dtp.Location = new Point(rtg.X, rtg.Y);
                    dtp.Visible = true;
                    break;

                case "column_fecha_salida":

                    rtg = dgv_reservas.GetCellDisplayRectangle(e.ColumnIndex, e.RowIndex, true);
                    dtp.Size = new Size(rtg.Width, rtg.Height);
                    dtp.Location = new Point(rtg.X, rtg.Y);
                    dtp.Visible = true;
                    break;
            }
        }
        private void dgv_reservas_CellPainting(object sender, DataGridViewCellPaintingEventArgs e)
        {
            if (e.ColumnIndex >= 0 && this.dgv_reservas.Columns[e.ColumnIndex].Name == "Modificar" && e.RowIndex >= 0)
            {
                e.Paint(e.CellBounds, DataGridViewPaintParts.All);

                DataGridViewButtonCell btn_modificar = this.dgv_reservas.Rows[e.RowIndex].Cells["Modificar"] as DataGridViewButtonCell;
                Icon modificar = new Icon(Environment.CurrentDirectory + @"\icono_modificar.ico");
                e.Graphics.DrawIcon(modificar, e.CellBounds.Left + 3, e.CellBounds.Top + 3);


                this.dgv_reservas.Rows[e.RowIndex].Height = modificar.Height + 10;
                this.dgv_reservas.Columns[e.ColumnIndex].Width = modificar.Width + 10;

                e.Handled = true;
            }

            if (e.ColumnIndex >= 0 && this.dgv_reservas.Columns[e.ColumnIndex].Name == "Eliminar" && e.RowIndex >= 0)
            {
                e.Paint(e.CellBounds, DataGridViewPaintParts.All);

                DataGridViewButtonCell btn_eliminar = this.dgv_reservas.Rows[e.RowIndex].Cells["Eliminar"] as DataGridViewButtonCell;
                Icon eliminar = new Icon(Environment.CurrentDirectory + @"\icono_eliminar.ico");
                e.Graphics.DrawIcon(eliminar, e.CellBounds.Left + 3, e.CellBounds.Top + 3);

                this.dgv_reservas.Rows[e.RowIndex].Height = eliminar.Height + 10;
                this.dgv_reservas.Columns[e.ColumnIndex].Width = eliminar.Width + 10;

                e.Handled = true;
            }
        }
        private void dgv_reservas_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {

        }
    }
}
