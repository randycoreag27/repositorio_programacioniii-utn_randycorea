﻿using System;
using System.Windows.Forms;

namespace Capa_Presentacion
{
    public partial class Form_Principal : Form
    {
        public static Panel panel; 
        public Form_Principal()
        {
            InitializeComponent();
        }
        private void btn_salir_Click_1(object sender, EventArgs e)
        {
            DialogResult respuesta = MessageBox.Show("¿Desea salir de la Aplicación?", "Pregunta", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (respuesta == DialogResult.Yes)
            {
                Application.Exit();
            }
        }
        private void timer_fecha_hora_Tick(object sender, EventArgs e)
        {
            label_fecha.Text = DateTime.Now.ToLongDateString();
            label_hora.Text = DateTime.Now.ToString("hh:mm:ss");
        }
        private void btn_tipo_cambio_Click(object sender, EventArgs e)
        {
            Form_Tipo_Cambio frm_tc = new Form_Tipo_Cambio();
            Frm_Tipo_Cambio(frm_tc);
        }
        private void btn_reservas_Click(object sender, EventArgs e)
        {
            Form_Opciones_Reservas frm_or = new Form_Opciones_Reservas();
            Frm_Opciones_Reservas(frm_or);
        }
        private void btn_tarifas_Click(object sender, EventArgs e)
        {
            Form_Tarifas frm_t = new Form_Tarifas();
            Frm_Tarifas (frm_t);
        }
        private void btn_reportes_Click(object sender, EventArgs e)
        {
            Form_Reportes frm_r = new Form_Reportes();
            Frm_Reportes(frm_r);
        }
        private void btn_facturas_Click(object sender, EventArgs e)
        {
            Form_Facturacion frm_f = new Form_Facturacion();
            frm_f.Show();
        }
        private void btn_como_llegar_Click(object sender, EventArgs e)
        {
            Form_Como_Llegar form_cll = new Form_Como_Llegar();
            Frm_Como_Llegar(form_cll);
        }
        public void Frm_Tarifas(Form frm_t)
        {
            frm_t.TopLevel = false;
            this.panel_principal.Controls.Add(frm_t);
            frm_t.Show();
        }
        public void Frm_Como_Llegar(Form frm_cll)
        {
            frm_cll.TopLevel = false;
            this.panel_principal.Controls.Add(frm_cll);
            frm_cll.Show();
        }
        public void Frm_Tipo_Cambio(Form frm_tc)
        {
            frm_tc.TopLevel = false;
            this.panel_principal.Controls.Add(frm_tc);
            frm_tc.Show();
        }
        public void Frm_Opciones_Reservas(Form frm_r)
        {
            frm_r.TopLevel = false;
            this.panel_principal.Controls.Add(frm_r);
            panel = panel_principal;
            frm_r.Show();
        }
        public void Frm_Reportes(Form frm_r)
        {
            frm_r.TopLevel = false;
            this.panel_principal.Controls.Add(frm_r);
            panel = panel_principal;
            frm_r.Show();
        }


    }
}
