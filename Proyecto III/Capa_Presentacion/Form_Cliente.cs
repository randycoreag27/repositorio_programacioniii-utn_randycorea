﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Capa_Presentacion
{
    public partial class Form_Cliente : Form
    {
        public Form_Cliente()
        {
            InitializeComponent();
        }

        private void pictureBox6_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            Form_Tipo_Cambio frm_tp = new Form_Tipo_Cambio();
            frm_tp.Show();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            Form_Como_Llegar frm_cll = new Form_Como_Llegar();
            frm_cll.Show();
        }

        private void pictureBox7_Click(object sender, EventArgs e)
        {
            Form_Reservas frm_r = new Form_Reservas();
            frm_r.Show();
        }
    }
}
