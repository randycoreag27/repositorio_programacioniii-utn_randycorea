﻿
namespace Capa_Presentacion
{
    partial class Form_Opciones_Reservas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form_Opciones_Reservas));
            this.panel_principal = new System.Windows.Forms.Panel();
            this.pic = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btn_reservas = new System.Windows.Forms.Button();
            this.btn_gestion_clientes = new System.Windows.Forms.Button();
            this.btn_gestion_reservas = new System.Windows.Forms.Button();
            this.panel_principal.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pic)).BeginInit();
            this.SuspendLayout();
            // 
            // panel_principal
            // 
            this.panel_principal.BackColor = System.Drawing.Color.ForestGreen;
            this.panel_principal.Controls.Add(this.pic);
            this.panel_principal.Controls.Add(this.label1);
            this.panel_principal.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel_principal.Location = new System.Drawing.Point(0, 0);
            this.panel_principal.Name = "panel_principal";
            this.panel_principal.Size = new System.Drawing.Size(323, 69);
            this.panel_principal.TabIndex = 7;
            // 
            // pic
            // 
            this.pic.Image = ((System.Drawing.Image)(resources.GetObject("pic.Image")));
            this.pic.Location = new System.Drawing.Point(57, 12);
            this.pic.Name = "pic";
            this.pic.Size = new System.Drawing.Size(86, 49);
            this.pic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pic.TabIndex = 12;
            this.pic.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial Rounded MT Bold", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(136, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(139, 32);
            this.label1.TabIndex = 11;
            this.label1.Text = "Reservas";
            // 
            // btn_reservas
            // 
            this.btn_reservas.BackColor = System.Drawing.Color.ForestGreen;
            this.btn_reservas.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DarkGreen;
            this.btn_reservas.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DarkGreen;
            this.btn_reservas.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_reservas.Font = new System.Drawing.Font("Arial Rounded MT Bold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_reservas.ForeColor = System.Drawing.Color.White;
            this.btn_reservas.Location = new System.Drawing.Point(81, 95);
            this.btn_reservas.Name = "btn_reservas";
            this.btn_reservas.Size = new System.Drawing.Size(160, 32);
            this.btn_reservas.TabIndex = 17;
            this.btn_reservas.Text = "Reservas";
            this.btn_reservas.UseVisualStyleBackColor = false;
            this.btn_reservas.Click += new System.EventHandler(this.btn_reservas_Click);
            // 
            // btn_gestion_clientes
            // 
            this.btn_gestion_clientes.BackColor = System.Drawing.Color.ForestGreen;
            this.btn_gestion_clientes.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DarkGreen;
            this.btn_gestion_clientes.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DarkGreen;
            this.btn_gestion_clientes.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_gestion_clientes.Font = new System.Drawing.Font("Arial Rounded MT Bold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_gestion_clientes.ForeColor = System.Drawing.Color.White;
            this.btn_gestion_clientes.Location = new System.Drawing.Point(81, 139);
            this.btn_gestion_clientes.Name = "btn_gestion_clientes";
            this.btn_gestion_clientes.Size = new System.Drawing.Size(160, 32);
            this.btn_gestion_clientes.TabIndex = 18;
            this.btn_gestion_clientes.Text = "Gestión Clientes";
            this.btn_gestion_clientes.UseVisualStyleBackColor = false;
            this.btn_gestion_clientes.Click += new System.EventHandler(this.btn_gestion_clientes_Click);
            // 
            // btn_gestion_reservas
            // 
            this.btn_gestion_reservas.BackColor = System.Drawing.Color.ForestGreen;
            this.btn_gestion_reservas.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DarkGreen;
            this.btn_gestion_reservas.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DarkGreen;
            this.btn_gestion_reservas.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_gestion_reservas.Font = new System.Drawing.Font("Arial Rounded MT Bold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_gestion_reservas.ForeColor = System.Drawing.Color.White;
            this.btn_gestion_reservas.Location = new System.Drawing.Point(81, 183);
            this.btn_gestion_reservas.Name = "btn_gestion_reservas";
            this.btn_gestion_reservas.Size = new System.Drawing.Size(160, 32);
            this.btn_gestion_reservas.TabIndex = 19;
            this.btn_gestion_reservas.Text = "Gestión Reservas";
            this.btn_gestion_reservas.UseVisualStyleBackColor = false;
            this.btn_gestion_reservas.Click += new System.EventHandler(this.btn_gestion_reservas_Click);
            // 
            // Form_Opciones_Reservas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(323, 227);
            this.Controls.Add(this.btn_gestion_reservas);
            this.Controls.Add(this.btn_gestion_clientes);
            this.Controls.Add(this.btn_reservas);
            this.Controls.Add(this.panel_principal);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Location = new System.Drawing.Point(375, 90);
            this.Name = "Form_Opciones_Reservas";
            this.Opacity = 0.9D;
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form_Opciones_Reservas_FormClosed);
            this.panel_principal.ResumeLayout(false);
            this.panel_principal.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pic)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel_principal;
        private System.Windows.Forms.PictureBox pic;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btn_reservas;
        private System.Windows.Forms.Button btn_gestion_clientes;
        private System.Windows.Forms.Button btn_gestion_reservas;
    }
}