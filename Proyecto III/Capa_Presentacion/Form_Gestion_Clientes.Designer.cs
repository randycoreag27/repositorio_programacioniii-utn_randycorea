﻿
namespace Capa_Presentacion
{
    partial class Form_Gestion_Clientes
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form_Gestion_Clientes));
            this.dgv_clientes = new System.Windows.Forms.DataGridView();
            this.panel_principal = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.btn_reservas = new System.Windows.Forms.PictureBox();
            this.column_codigo_cliente = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.column_cedula_cliente = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.column_nombre = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.column_genero = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.column_pais_residencia = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.column_telefono_movil = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.column_correo_electronico = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Modificar = new System.Windows.Forms.DataGridViewButtonColumn();
            this.Eliminar = new System.Windows.Forms.DataGridViewButtonColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_clientes)).BeginInit();
            this.panel_principal.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btn_reservas)).BeginInit();
            this.SuspendLayout();
            // 
            // dgv_clientes
            // 
            this.dgv_clientes.AllowUserToAddRows = false;
            this.dgv_clientes.AllowUserToDeleteRows = false;
            this.dgv_clientes.BackgroundColor = System.Drawing.Color.White;
            this.dgv_clientes.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgv_clientes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_clientes.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.column_codigo_cliente,
            this.column_cedula_cliente,
            this.column_nombre,
            this.column_genero,
            this.column_pais_residencia,
            this.column_telefono_movil,
            this.column_correo_electronico,
            this.Modificar,
            this.Eliminar});
            this.dgv_clientes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv_clientes.Location = new System.Drawing.Point(0, 69);
            this.dgv_clientes.Name = "dgv_clientes";
            this.dgv_clientes.RowHeadersVisible = false;
            this.dgv_clientes.Size = new System.Drawing.Size(805, 162);
            this.dgv_clientes.TabIndex = 11;
            this.dgv_clientes.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_tarifas_CellContentClick);
            this.dgv_clientes.CellPainting += new System.Windows.Forms.DataGridViewCellPaintingEventHandler(this.dgv_tarifas_CellPainting);
            this.dgv_clientes.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dgv_clientes_DataError);
            // 
            // panel_principal
            // 
            this.panel_principal.BackColor = System.Drawing.Color.ForestGreen;
            this.panel_principal.Controls.Add(this.label1);
            this.panel_principal.Controls.Add(this.btn_reservas);
            this.panel_principal.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel_principal.Location = new System.Drawing.Point(0, 0);
            this.panel_principal.Name = "panel_principal";
            this.panel_principal.Size = new System.Drawing.Size(805, 69);
            this.panel_principal.TabIndex = 10;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial Rounded MT Bold", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(313, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(270, 32);
            this.label1.TabIndex = 11;
            this.label1.Text = "Gestión de Clientes";
            // 
            // btn_reservas
            // 
            this.btn_reservas.BackColor = System.Drawing.Color.Transparent;
            this.btn_reservas.Image = ((System.Drawing.Image)(resources.GetObject("btn_reservas.Image")));
            this.btn_reservas.Location = new System.Drawing.Point(233, 12);
            this.btn_reservas.Name = "btn_reservas";
            this.btn_reservas.Size = new System.Drawing.Size(86, 49);
            this.btn_reservas.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btn_reservas.TabIndex = 12;
            this.btn_reservas.TabStop = false;
            // 
            // column_codigo_cliente
            // 
            this.column_codigo_cliente.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.column_codigo_cliente.Frozen = true;
            this.column_codigo_cliente.HeaderText = "Código de Cliente";
            this.column_codigo_cliente.Name = "column_codigo_cliente";
            this.column_codigo_cliente.ReadOnly = true;
            this.column_codigo_cliente.Width = 115;
            // 
            // column_cedula_cliente
            // 
            this.column_cedula_cliente.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.column_cedula_cliente.Frozen = true;
            this.column_cedula_cliente.HeaderText = "Cédula";
            this.column_cedula_cliente.Name = "column_cedula_cliente";
            this.column_cedula_cliente.Width = 65;
            // 
            // column_nombre
            // 
            this.column_nombre.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.column_nombre.Frozen = true;
            this.column_nombre.HeaderText = "Nombre Completo";
            this.column_nombre.Name = "column_nombre";
            this.column_nombre.Width = 118;
            // 
            // column_genero
            // 
            this.column_genero.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.column_genero.Frozen = true;
            this.column_genero.HeaderText = "Género";
            this.column_genero.Items.AddRange(new object[] {
            "Masculino",
            "Femenino"});
            this.column_genero.Name = "column_genero";
            this.column_genero.Width = 75;
            // 
            // column_pais_residencia
            // 
            this.column_pais_residencia.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.column_pais_residencia.Frozen = true;
            this.column_pais_residencia.HeaderText = "Pais Residencia";
            this.column_pais_residencia.Name = "column_pais_residencia";
            this.column_pais_residencia.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.column_pais_residencia.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.column_pais_residencia.Width = 120;
            // 
            // column_telefono_movil
            // 
            this.column_telefono_movil.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.column_telefono_movil.Frozen = true;
            this.column_telefono_movil.HeaderText = "Telefono Movil";
            this.column_telefono_movil.Name = "column_telefono_movil";
            this.column_telefono_movil.Width = 105;
            // 
            // column_correo_electronico
            // 
            this.column_correo_electronico.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.column_correo_electronico.Frozen = true;
            this.column_correo_electronico.HeaderText = "Correo Electronico";
            this.column_correo_electronico.Name = "column_correo_electronico";
            this.column_correo_electronico.Width = 135;
            // 
            // Modificar
            // 
            this.Modificar.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.Modificar.Frozen = true;
            this.Modificar.HeaderText = "";
            this.Modificar.Name = "Modificar";
            this.Modificar.Width = 36;
            // 
            // Eliminar
            // 
            this.Eliminar.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.Eliminar.Frozen = true;
            this.Eliminar.HeaderText = "";
            this.Eliminar.Name = "Eliminar";
            this.Eliminar.Width = 36;
            // 
            // Form_Gestion_Clientes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(805, 231);
            this.Controls.Add(this.dgv_clientes);
            this.Controls.Add(this.panel_principal);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Location = new System.Drawing.Point(100, 70);
            this.Name = "Form_Gestion_Clientes";
            this.Opacity = 0.9D;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form_Gestion_Clientes_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_clientes)).EndInit();
            this.panel_principal.ResumeLayout(false);
            this.panel_principal.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btn_reservas)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgv_clientes;
        private System.Windows.Forms.Panel panel_principal;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox btn_reservas;
        private System.Windows.Forms.DataGridViewTextBoxColumn column_codigo_cliente;
        private System.Windows.Forms.DataGridViewTextBoxColumn column_cedula_cliente;
        private System.Windows.Forms.DataGridViewTextBoxColumn column_nombre;
        private System.Windows.Forms.DataGridViewComboBoxColumn column_genero;
        private System.Windows.Forms.DataGridViewComboBoxColumn column_pais_residencia;
        private System.Windows.Forms.DataGridViewTextBoxColumn column_telefono_movil;
        private System.Windows.Forms.DataGridViewTextBoxColumn column_correo_electronico;
        private System.Windows.Forms.DataGridViewButtonColumn Modificar;
        private System.Windows.Forms.DataGridViewButtonColumn Eliminar;
    }
}