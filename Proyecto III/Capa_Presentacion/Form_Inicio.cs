﻿using Capa_Negocio;
using Objetos;
using System;
using System.Windows.Forms;

namespace Capa_Presentacion
{
    public partial class Form_Inicio : Form
    {

        NG_Usuario ng_usuario = new NG_Usuario();
        Usuario usuario = new Usuario(); 
        public static int codigo_usuario = 0;

        public Form_Inicio()
        {
            InitializeComponent();
        }
        private void Obtener_Datos()
        {
            usuario.usuario = txt_user.Text;
            usuario.contrasenna = txt_password.Text;
        }
        private void Validacion_Iniciar_Sesion()
        {
            if (txt_user.Text.Equals("") && txt_password.Text.Equals(""))
            {
                MessageBox.Show("Debe de Ingresar Datos", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                Obtener_Datos();
                ng_usuario.Consulta_Inicio_Sesion(usuario);
                if (usuario.rol == null)
                {
                    MessageBox.Show("El usuario NO se encuentra registrado, procede a registrarse ", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else if ((usuario.usuario.Equals(txt_user.Text) && usuario.contrasenna != txt_password.Text) 
                    || (usuario.contrasenna.Equals(txt_password.Text) && usuario.usuario != txt_user.Text)
                    || (usuario.usuario != txt_user.Text && usuario.contrasenna != txt_password.Text))
                {
                    MessageBox.Show("Usuario o Contraseña Incorrectos", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else if (txt_user.Text.Equals(usuario.usuario) && txt_password.Text.Equals(usuario.contrasenna) && usuario.rol.Equals("Administrador"))
                {
                    codigo_usuario = usuario.codigo_usuario;
                    MessageBox.Show("Bienvenido", "Bienvenida", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Form_Principal frm_a = new Form_Principal();
                    frm_a.Show();
                    this.Hide();
                }
                else if (txt_user.Text.Equals(usuario.usuario) && txt_password.Text.Equals(usuario.contrasenna) && usuario.rol.Equals("Cliente"))
                {
                    codigo_usuario = usuario.codigo_usuario;
                    MessageBox.Show("Bienvenido", "Bienvenida", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Form_Cliente frm_a = new Form_Cliente();
                    frm_a.Show();
                    this.Hide();
                }
            }
        }

        private void btn_iniciar_sesion_Click(object sender, EventArgs e)
        {
            Validacion_Iniciar_Sesion();
        }

        private void btn_register_Click(object sender, EventArgs e)
        {
            Form_Registrarse frm_r = new Form_Registrarse();
            frm_r.Show();
            this.Hide();
        }
        private void btn_exit_Click(object sender, EventArgs e)
        {
            DialogResult respuesta = MessageBox.Show("¿Desea salir de la Aplicación?", "Advertencia", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);

            if (respuesta == DialogResult.Yes)
            {
                Application.Exit();
            }
        }
    }
}
