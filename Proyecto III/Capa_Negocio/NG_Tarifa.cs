﻿using Capa_Datos;
using Objetos;
using System.Collections.Generic;


namespace Capa_Negocio
{
    public class NG_Tarifa
    {
        BD_Tarifa bd_tarifa = new BD_Tarifa();

        public void Agregar_Tarifa(Tarifa ta)
        {
            bd_tarifa.Agregar_Tarifa(ta);
        }
        public void Modificar_Tarifa(Tarifa ta)
        {
            bd_tarifa.Modificar_Tarifa(ta);
        }
        public void Eliminar_Tarifa(Tarifa ta)
        {
            bd_tarifa.Eliminar_Tarifa(ta);
        }
        public List<Tarifa> Obtener_Tarifas()
        {
            return new BD_Tarifa().Obtener_Tarifas();
        }
        public int Obtener_Monto_Tarifa(int cod)
        {
            return new BD_Tarifa().Obtener_Monto_Tarifa(cod);
        }


    }
}
