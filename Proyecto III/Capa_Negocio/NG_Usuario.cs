﻿using Capa_Datos;
using Objetos;
using System.Collections.Generic;


namespace Capa_Negocio
{
    public class NG_Usuario
    {
        BD_Usuario bd_usuario = new BD_Usuario();
        public void Consulta_Inicio_Sesion(Usuario u)
        {
            bd_usuario.Consulta_Inicio_Sesion(u);
        }
        public void Agregar_Usuario(Usuario u)
        {
            bd_usuario.Agregar_Usuario(u);
        }
        /*public List<Usuario> Obtener_Reservas()
        {
            return new BD_Usuario().Obtener_Usuarios();
        }*/
    }
}
