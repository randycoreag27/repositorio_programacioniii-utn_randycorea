﻿using Capa_Datos;
using Objetos;
using System.Collections.Generic;

namespace Capa_Negocio
{
    public class NG_Reporte
    {
        public List<Reporte> Reporte1()
        {
            return new BD_Reportes().Reporte1();
        }
        public List<Reporte> Reporte2_1()
        {
            return new BD_Reportes().Reporte2_1();
        }
        public List<Reporte> Reporte2_2()
        {
            return new BD_Reportes().Reporte2_2();
        }
        public List<Reporte> Reporte3(Reporte r)
        {
            return new BD_Reportes().Reporte3(r);
        }
    }
}
