﻿using Capa_Datos;
using Objetos;
using System.Collections.Generic;


namespace Capa_Negocio
{
    public class NG_Cliente
    {
        BD_Cliente bd_cliente = new BD_Cliente();
        public void Agregar_Cliente(Cliente cli)
        {
            bd_cliente.Agregar_Cliente(cli);
        }
        public void Modificar_Cliente(Cliente cli)
        {
            bd_cliente.Modificar_Cliente(cli);
        }
        public void Eliminar_Cliente(Cliente cli)
        {
            bd_cliente.Eliminar_Cliente(cli);
        }
        public int Consultar_Existencia_Cliente(Cliente cli)
        {
            return new BD_Cliente().Consulta_Existencia_Cliente(cli);
        }
        public List<Cliente> Obtener_Clientes()
        {
            return new BD_Cliente().Obtener_Clientes();
        }
    }
}
