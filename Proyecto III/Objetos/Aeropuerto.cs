﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Capa_Objetos
{
    public class Aeropuerto
    {
        public string iata { get; set; }
        public string fs { get; set; }
        public string nombre { get; set; }
        public string ciudad { get; set; }
        public string pais { get; set; }
        public Boolean activo { get; set; }
    }
}
