﻿namespace Objetos
{
    public class Usuario
    {
        public int codigo_usuario { get; set; }
        public string usuario { get; set; }
        public string contrasenna { get; set; }
        public string rol { get; set; }
    }
}
