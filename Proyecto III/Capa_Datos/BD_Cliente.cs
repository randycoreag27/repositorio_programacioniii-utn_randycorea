﻿using Npgsql;
using Objetos;
using System.Collections.Generic;

namespace Capa_Datos
{
    public class BD_Cliente
    {
        static NpgsqlConnection conexion;
        static NpgsqlCommand cmd;
        public void Agregar_Cliente(Cliente cli)
        {
            conexion = Conexion_BD.connection();
            conexion.Open();
            cmd = new NpgsqlCommand("INSERT INTO CLIENTE (CEDULA, NOMBRE, GENERO, PAIS_RESIDENCIA) VALUES " +
            "('" + cli.cedula_cliente + "', '" + cli.nombre_cliente + "', '" + cli.genero + "', '" + cli.pais_residencia + "')", conexion);
            cmd.ExecuteNonQuery();
            conexion.Close();
            int codigo_cliente = Obtener_Codigo_Cliente(cli);
            Agregar_Contacto(cli, codigo_cliente);
        }
        public void Agregar_Contacto(Cliente cli, int codigo_cliente)
        {
            conexion = Conexion_BD.connection();
            conexion.Open();
            cmd = new NpgsqlCommand("INSERT INTO CONTACTO (CODIGO_CLIENTE, TELEFONO_MOVIL, CORREO_ELECTRONICO) VALUES " +
            "('" + codigo_cliente + "', '"  + cli.telefono_movil + "', '" + cli.correo_electronico + "')", conexion);
            cmd.ExecuteNonQuery();
            conexion.Close();
        }
        public void Modificar_Cliente(Cliente cli)
        {
            conexion = Conexion_BD.connection();
            conexion.Open();
            cmd = new NpgsqlCommand("UPDATE CLIENTE SET CEDULA = '" + cli.cedula_cliente + "', " + "PAIS_RESIDENCIA = '" + cli.pais_residencia + 
                "', " + "NOMBRE = '" + cli.nombre_cliente + "', " + "GENERO = '" + cli.genero + "' WHERE CODIGO_CLIENTE = '" 
                + cli.codigo_cliente + "'", conexion);
            cmd.ExecuteNonQuery();
            conexion.Close();
            Modificar_Contacto(cli);
        }
        public void Modificar_Contacto(Cliente cli)
        {
            conexion = Conexion_BD.connection();
            conexion.Open();
            cmd = new NpgsqlCommand("UPDATE CONTACTO SET TELEFONO_MOVIL = '" + cli.telefono_movil + "', " + "CORREO_ELECTRONICO = '" 
                + cli.correo_electronico + "' WHERE CODIGO_CLIENTE = '" + cli.codigo_cliente + "'", conexion);
            cmd.ExecuteNonQuery();
            conexion.Close();
        }
        public void Eliminar_Cliente(Cliente cli)
        {
            conexion = Conexion_BD.connection();
            conexion.Open();
            cmd = new NpgsqlCommand("DELETE FROM CLIENTE WHERE CODIGO_CLIENTE = '" + cli.codigo_cliente + "'", conexion);
            cmd.ExecuteNonQuery();
            conexion.Close();
            Eliminar_Contacto(cli);
        }
        public void Eliminar_Contacto(Cliente cli)
        {
            conexion = Conexion_BD.connection();
            conexion.Open();
            cmd = new NpgsqlCommand("DELETE FROM CONTACTO WHERE CODIGO_CLIENTE = '" + cli.codigo_cliente + "'", conexion);
            cmd.ExecuteNonQuery();
            conexion.Close();
        }
        public int Obtener_Codigo_Cliente(Cliente cli)
        {
            int codigo_cliente = 0;

            conexion = Conexion_BD.connection();
            conexion.Open();
            cmd = new NpgsqlCommand("SELECT CODIGO_CLIENTE FROM CLIENTE WHERE CEDULA = '" + cli.cedula_cliente + "'", conexion);
            NpgsqlDataReader rd = cmd.ExecuteReader();

            while (rd.Read())
            {
                codigo_cliente = rd.GetInt32(0);
            }
            conexion.Close();

            return codigo_cliente;
        }
        public int Consulta_Existencia_Cliente(Cliente cli)
        {
            int resultado = 0;
            conexion = Conexion_BD.connection();
            conexion.Open();
            cmd = new NpgsqlCommand("SELECT COUNT(NOMBRE) FROM CLIENTE WHERE CEDULA = '" + cli.cedula_cliente + "'", conexion);
            NpgsqlDataReader rd = cmd.ExecuteReader();
            while (rd.Read())
            {
                resultado = rd.GetInt32(0);
            }
            conexion.Close();
            return resultado;
        }
        public List<Cliente> Obtener_Clientes()
        {

            List<Cliente> clientes = new List<Cliente>();

            conexion = Conexion_BD.connection();
            conexion.Open();
            cmd = new NpgsqlCommand("SELECT C.CODIGO_CLIENTE, C.CEDULA, C.NOMBRE, C.GENERO, C.PAIS_RESIDENCIA, " +
                " CT.TELEFONO_MOVIL, CT.CORREO_ELECTRONICO FROM CLIENTE C " +
                        "INNER JOIN CONTACTO CT ON C.CODIGO_CLIENTE = CT.CODIGO_CLIENTE", conexion);
            NpgsqlDataReader rd = cmd.ExecuteReader();

            while (rd.Read())
            {
                Cliente c = new Cliente();
                c.codigo_cliente = rd.GetInt32(0);
                c.cedula_cliente = rd.GetInt32(1);
                c.nombre_cliente = rd.GetString(2);
                c.genero = rd.GetString(3);
                c.pais_residencia = rd.GetString(4);
                c.telefono_movil = rd.GetInt32(5);
                c.correo_electronico = rd.GetString(6);
                clientes.Add(c);
            }
            conexion.Close();

            return clientes;
        }
    }
}
