﻿using Npgsql;
using Objetos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Capa_Datos
{
    public class BD_Tarifa
    {

        static NpgsqlConnection conexion;
        static NpgsqlCommand cmd;

        public void Agregar_Tarifa(Tarifa ta)
        {
            conexion = Conexion_BD.connection();
            conexion.Open();
            cmd = new NpgsqlCommand("INSERT INTO TARIFA (NOMBRE, MONTO) VALUES " +
            "('" + ta.nombre_tarifa + "', '" + ta.monto_tarifa +  "')", conexion);
            cmd.ExecuteNonQuery();
            conexion.Close();
        }

        public void Modificar_Tarifa(Tarifa ta)
        {
            conexion = Conexion_BD.connection();
            conexion.Open();
            cmd = new NpgsqlCommand("UPDATE TARIFA SET NOMBRE = '" + ta.nombre_tarifa+ "', " + "MONTO = '" + ta.monto_tarifa +
                "' WHERE CODIGO_TARIFA = '" + ta.codigo_tarifa + "'", conexion);
            cmd.ExecuteNonQuery();
            conexion.Close();
        }

        public void Eliminar_Tarifa(Tarifa ta)
        {
            conexion = Conexion_BD.connection();
            conexion.Open();
            cmd = new NpgsqlCommand("DELETE FROM TARIFA WHERE CODIGO_TARIFA = '" + ta.codigo_tarifa + "'", conexion);
            cmd.ExecuteNonQuery();
            conexion.Close();
        }


        public List<Tarifa> Obtener_Tarifas()
        {
            List<Tarifa> tarifas = new List<Tarifa>();

            conexion = Conexion_BD.connection();
            conexion.Open();
            cmd = new NpgsqlCommand("SELECT * FROM TARIFA ORDER BY CODIGO_TARIFA ASC", conexion);
            NpgsqlDataReader rd = cmd.ExecuteReader();

            while (rd.Read())
            {
                Tarifa ta = new Tarifa();
                ta.codigo_tarifa = rd.GetInt32(0);
                ta.nombre_tarifa = rd.GetString(1);
                ta.monto_tarifa = rd.GetDouble(2);
                tarifas.Add(ta);
            }
            conexion.Close();
            return tarifas;
        }

        public int Obtener_Monto_Tarifa(int cod)
        {
            conexion = Conexion_BD.connection();
            conexion.Open();
            cmd = new NpgsqlCommand("SELECT MONTO FROM TARIFA WHERE CODIGO_TARIFA = " + cod + ";", conexion);
            NpgsqlDataReader rd = cmd.ExecuteReader();
            int tarifa = 0;
            while (rd.Read())
            {
                tarifa = Convert.ToInt32(rd.GetDouble(0));
            }
            conexion.Close();
            return tarifa;
        }
    }
}
