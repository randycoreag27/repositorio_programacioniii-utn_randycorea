﻿using Npgsql;
using Objetos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Capa_Datos
{
    public class BD_Usuario
    {
        //Declaración de variables a utilizar 
        static NpgsqlConnection conexion;
        static NpgsqlCommand cmd;

        //Método en el cual se consultan los datos para realizar el inicio de sesión
        public void Consulta_Inicio_Sesion(Usuario u)
        {
            conexion = Conexion_BD.connection();
            conexion.Open();
            cmd = new NpgsqlCommand("SELECT * FROM USUARIO WHERE USUARIO = '" + u.usuario + "'", conexion);
            NpgsqlDataReader rd = cmd.ExecuteReader();

            if (rd.Read())
            {
                u.codigo_usuario = rd.GetInt32(0);
                u.usuario = rd.GetString(1);
                u.contrasenna = rd.GetString(2);
                u.rol = rd.GetString(3);
            }
            conexion.Close();
        }
        //Método en el cual se realiza la agregación de los usuario a la base de datos
        public void Agregar_Usuario(Usuario usu)
        {
            conexion = Conexion_BD.connection();
            conexion.Open();
            cmd = new NpgsqlCommand("INSERT INTO USUARIO (USUARIO, CONTRASENNA, ROL) VALUES " +
            "('" + usu.usuario + "', '" + usu.contrasenna + "', '" + usu.rol + "')", conexion);
            cmd.ExecuteNonQuery();
            conexion.Close();
        }
    }
}
