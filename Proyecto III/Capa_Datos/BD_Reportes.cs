﻿using Npgsql;
using Objetos;
using System;
using System.Collections.Generic;

namespace Capa_Datos
{
    public class BD_Reportes
    {
        static NpgsqlConnection conexion;
        static NpgsqlCommand cmd;
        public List<Reporte> Reporte1()
        {
            List<Reporte> datos = new List<Reporte>();
            conexion = Conexion_BD.connection();
            conexion.Open();
            cmd = new NpgsqlCommand("SELECT COUNT(R.CODIGO_USUARIO), E.NOMBRE FROM RESERVA R  " +
                "INNER JOIN EMPLEADO E " +
                "ON R.CODIGO_USUARIO = E.CODIGO_USUARIO GROUP BY E.NOMBRE", conexion);
            NpgsqlDataReader rd = cmd.ExecuteReader();
            while (rd.Read())
            {
                Reporte re = new Reporte();
                re.cantidad_reservas = rd.GetInt32(0);
                re.nombre_usuario = rd.GetString(1);
                datos.Add(re);
            }
            conexion.Close();
            return datos;
        }
        public List<Reporte> Reporte2_1()
        {
            List<Reporte> datos = new List<Reporte>();
            conexion = Conexion_BD.connection();
            conexion.Open();
            cmd = new NpgsqlCommand("SELECT COUNT(R.CODIGO_CLIENTE), C.PAIS_RESIDENCIA FROM RESERVA R INNER JOIN CLIENTE C " +
                "ON R.CODIGO_CLIENTE = C.CODIGO_CLIENTE WHERE C.PAIS_RESIDENCIA = 'Costa Rica' GROUP BY C.PAIS_RESIDENCIA", conexion);
            NpgsqlDataReader rd = cmd.ExecuteReader();
            while (rd.Read())
            {
                Reporte re = new Reporte();
                re.cantidad_reservas = rd.GetInt32(0);
                re.nombre_usuario = rd.GetString(1);
                datos.Add(re);
            }
            conexion.Close();
            return datos;
        }
        public List<Reporte> Reporte2_2()
        {
            List<Reporte> datos = new List<Reporte>();
            conexion = Conexion_BD.connection();
            conexion.Open();
            cmd = new NpgsqlCommand("SELECT COUNT(R.CODIGO_CLIENTE), C.PAIS_RESIDENCIA FROM RESERVA R INNER JOIN CLIENTE C " +
                "ON R.CODIGO_CLIENTE = C.CODIGO_CLIENTE WHERE C.PAIS_RESIDENCIA != 'Costa Rica' GROUP BY C.PAIS_RESIDENCIA", conexion);
            NpgsqlDataReader rd = cmd.ExecuteReader();
            while (rd.Read())
            {
                Reporte re = new Reporte();
                re.cantidad_reservas = rd.GetInt32(0);
                re.nombre_usuario = rd.GetString(1);
                datos.Add(re);
            }
            conexion.Close();
            return datos;
        }
        public List<Reporte> Reporte3(Reporte r)
        {
            List<Reporte> datos = new List<Reporte>();
            conexion = Conexion_BD.connection();
            conexion.Open();
            cmd = new NpgsqlCommand("SELECT C.CEDULA, C.NOMBRE, R.FECHA_ENTRADA, R.FECHA_SALIDA, R.CANTIDAD_NOCHES,  " +
                "R.CANTIDAD_PERSONAS, R.MONTO_TOTAL, E.NOMBRE FROM RESERVA R INNER JOIN EMPLEADO E " +
                "ON R.CODIGO_USUARIO = E.CODIGO_USUARIO INNER JOIN TARIFA T ON R.CODIGO_TARIFA = T.CODIGO_TARIFA " +
                "INNER JOIN CLIENTE C ON R.CODIGO_CLIENTE = C.CODIGO_CLIENTE " +
                "WHERE FECHA_ENTRADA >= " + "'" + r.fecha_entrada + "'" + " AND FECHA_SALIDA <= '" + r.fecha_salida + "'", conexion);
            NpgsqlDataReader rd = cmd.ExecuteReader();

            while (rd.Read())
            {
                Reporte re = new Reporte();
                re.cedula = rd.GetInt32(0);
                re.nombre_cliente = rd.GetString(1);
                re.fecha_entrada = rd.GetDateTime(2);
                re.fecha_salida = rd.GetDateTime(3);
                re.cantidad_personas = rd.GetInt32(4);
                re.cantidad_noches = rd.GetInt32(5);
                re.monto_total = Convert.ToInt32(rd.GetDouble(6));
                re.nombre_usuario = rd.GetString(7);
                datos.Add(re);
            }
            conexion.Close();
            return datos;
        }
    }
}
