﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Capa_Objetos
{
    public class Cliente_Habitacion
    {
        public int id_cliente_habitacion { get; set; }
        public int cedula { get; set; }
        public string id_habitacion { get; set; }
        public DateTime fecha_reserva { get; set; }
    }
}
