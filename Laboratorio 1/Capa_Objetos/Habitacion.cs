﻿using System;


namespace Capa_Objetos
{
    public class Habitacion
    {
        public string id_habitacion { get; set; }
        public string planta { get; set; }
        public string tipo { get; set; }
        public Boolean balcon { get; set; }
        public DateTime fecha_ingreso { get; set; }
        public DateTime fecha_salida { get; set; }
        public string estado { get; set; }
        public string foto { get; set; }

    }
}
