﻿using Capa_Datos;
using Capa_Objetos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Capa_Negocio
{
    public class NG_Habitacion
    {

        BD_Habitacion bd_habitacion = new BD_Habitacion();

        public void Agregar_Habitacion(Habitacion ha)
        {
            bd_habitacion.Agregar_Habitacion(ha);
        }

        public void Eliminar_Habitaciones(Habitacion ha)
        {
            bd_habitacion.Eliminar_Habitacion(ha);
        }

        public void Modificar_Habitacion(Habitacion ha)
        {
            bd_habitacion.Modificar_Habitacion(ha);
        }

        public List<Habitacion> Extraer_Habitacion()
        {
            return new BD_Habitacion().Extraer_Habitaciones();
        }

        public List<Habitacion> Extraer_Habitacion_Disponible()
        {
            return new BD_Habitacion().Extraer_Habitaciones_Disponibles();
        }

        public void Reservar_Habitacion(Habitacion ha)
        {
            bd_habitacion.Reservar_Habitacion(ha);
        }

    }
}
