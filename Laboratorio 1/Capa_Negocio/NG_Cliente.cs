﻿using Capa_Datos;
using Capa_Objetos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Capa_Negocio
{
    public class NG_Cliente
    {
        BD_Cliente bd_cliente = new BD_Cliente();

        public void Consulta_Inicio_Sesion(Cliente c)
        {
            bd_cliente.Consulta_Inicio_Sesion(c);
        }

        public List<Cliente> Obtener()
        {
            return new BD_Cliente().Obtener_Clientes();
        }

        public void Reporte(Cliente cliente,  DataGridView dgv)
        {
            bd_cliente.Reporte(cliente, dgv);
        }
    }
}
