﻿
namespace Capa_Presentacion
{
    partial class Frm_Administrador
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.dgv_habitaciones = new System.Windows.Forms.DataGridView();
            this.ID_HABITACION = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PLANTA = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.TIPO = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.BALCON = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.FECHA_INGRESO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FECHA_SALIDA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FOTO = new System.Windows.Forms.DataGridViewImageColumn();
            this.btn_agregar = new System.Windows.Forms.DataGridViewButtonColumn();
            this.btn_modificar = new System.Windows.Forms.DataGridViewButtonColumn();
            this.btn_eliminar = new System.Windows.Forms.DataGridViewButtonColumn();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btn_cerrar_sesion = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.button1 = new System.Windows.Forms.Button();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_habitaciones)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_cerrar_sesion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.DimGray;
            this.panel2.Controls.Add(this.button1);
            this.panel2.Controls.Add(this.btn_cerrar_sesion);
            this.panel2.Controls.Add(this.pictureBox2);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(235, 540);
            this.panel2.TabIndex = 1;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.Black;
            this.panel3.Controls.Add(this.dgv_habitaciones);
            this.panel3.Controls.Add(this.pictureBox1);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(235, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(998, 540);
            this.panel3.TabIndex = 2;
            this.panel3.Paint += new System.Windows.Forms.PaintEventHandler(this.panel3_Paint);
            // 
            // dgv_habitaciones
            // 
            this.dgv_habitaciones.AllowUserToDeleteRows = false;
            this.dgv_habitaciones.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgv_habitaciones.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgv_habitaciones.BackgroundColor = System.Drawing.Color.DimGray;
            this.dgv_habitaciones.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgv_habitaciones.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.DimGray;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.DimGray;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_habitaciones.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv_habitaciones.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_habitaciones.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ID_HABITACION,
            this.PLANTA,
            this.TIPO,
            this.BALCON,
            this.FECHA_INGRESO,
            this.FECHA_SALIDA,
            this.FOTO,
            this.btn_agregar,
            this.btn_modificar,
            this.btn_eliminar});
            this.dgv_habitaciones.EnableHeadersVisualStyles = false;
            this.dgv_habitaciones.GridColor = System.Drawing.Color.DimGray;
            this.dgv_habitaciones.Location = new System.Drawing.Point(21, 35);
            this.dgv_habitaciones.Name = "dgv_habitaciones";
            this.dgv_habitaciones.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.DimGray;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_habitaciones.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dgv_habitaciones.RowHeadersVisible = false;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.DimGray;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.LightGray;
            this.dgv_habitaciones.RowsDefaultCellStyle = dataGridViewCellStyle4;
            this.dgv_habitaciones.Size = new System.Drawing.Size(965, 492);
            this.dgv_habitaciones.TabIndex = 1;
            this.dgv_habitaciones.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_habitaciones_CellClick);
            this.dgv_habitaciones.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_habitaciones_CellContentClick);
            this.dgv_habitaciones.Scroll += new System.Windows.Forms.ScrollEventHandler(this.dgv_habitaciones_Scroll);
            // 
            // ID_HABITACION
            // 
            this.ID_HABITACION.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.ID_HABITACION.Frozen = true;
            this.ID_HABITACION.HeaderText = "ID_HABITACION";
            this.ID_HABITACION.MinimumWidth = 80;
            this.ID_HABITACION.Name = "ID_HABITACION";
            this.ID_HABITACION.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.ID_HABITACION.Width = 116;
            // 
            // PLANTA
            // 
            this.PLANTA.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.PLANTA.Frozen = true;
            this.PLANTA.HeaderText = "PLANTA";
            this.PLANTA.Items.AddRange(new object[] {
            "PLANTA1",
            "PLANTA2",
            "PLANTA3"});
            this.PLANTA.Name = "PLANTA";
            this.PLANTA.Width = 53;
            // 
            // TIPO
            // 
            this.TIPO.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.TIPO.Frozen = true;
            this.TIPO.HeaderText = "TIPO HABITACION";
            this.TIPO.Items.AddRange(new object[] {
            "JUNIOR",
            "SUPERIOR",
            "FAMILIAR"});
            this.TIPO.Name = "TIPO";
            this.TIPO.Width = 97;
            // 
            // BALCON
            // 
            this.BALCON.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.BALCON.Frozen = true;
            this.BALCON.HeaderText = "BALCON";
            this.BALCON.Name = "BALCON";
            this.BALCON.Width = 59;
            // 
            // FECHA_INGRESO
            // 
            this.FECHA_INGRESO.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.FECHA_INGRESO.Frozen = true;
            this.FECHA_INGRESO.HeaderText = "FECHA INGRESO";
            this.FECHA_INGRESO.Name = "FECHA_INGRESO";
            this.FECHA_INGRESO.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.FECHA_INGRESO.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.FECHA_INGRESO.Width = 92;
            // 
            // FECHA_SALIDA
            // 
            this.FECHA_SALIDA.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.FECHA_SALIDA.Frozen = true;
            this.FECHA_SALIDA.HeaderText = "FECHA_SALIDA";
            this.FECHA_SALIDA.Name = "FECHA_SALIDA";
            this.FECHA_SALIDA.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.FECHA_SALIDA.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.FECHA_SALIDA.Width = 91;
            // 
            // FOTO
            // 
            this.FOTO.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.FOTO.HeaderText = "FOTO HABITACION";
            this.FOTO.Name = "FOTO";
            this.FOTO.Width = 102;
            // 
            // btn_agregar
            // 
            this.btn_agregar.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.DimGray;
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.DimGray;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.White;
            this.btn_agregar.DefaultCellStyle = dataGridViewCellStyle2;
            this.btn_agregar.HeaderText = "AGREGAR";
            this.btn_agregar.Name = "btn_agregar";
            this.btn_agregar.Text = "AGREGAR";
            this.btn_agregar.ToolTipText = "AGREGAR";
            // 
            // btn_modificar
            // 
            this.btn_modificar.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.btn_modificar.HeaderText = "MODIFICAR";
            this.btn_modificar.Name = "btn_modificar";
            // 
            // btn_eliminar
            // 
            this.btn_eliminar.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.btn_eliminar.HeaderText = "ELIMINAR";
            this.btn_eliminar.Name = "btn_eliminar";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Capa_Presentacion.Properties.Resources.iconmonstr_x_mark_1_240;
            this.pictureBox1.Location = new System.Drawing.Point(971, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(15, 15);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // btn_cerrar_sesion
            // 
            this.btn_cerrar_sesion.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.btn_cerrar_sesion.Image = global::Capa_Presentacion.Properties.Resources.iniciar_sesion;
            this.btn_cerrar_sesion.Location = new System.Drawing.Point(3, 487);
            this.btn_cerrar_sesion.Name = "btn_cerrar_sesion";
            this.btn_cerrar_sesion.Size = new System.Drawing.Size(226, 50);
            this.btn_cerrar_sesion.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btn_cerrar_sesion.TabIndex = 1;
            this.btn_cerrar_sesion.TabStop = false;
            this.btn_cerrar_sesion.Click += new System.EventHandler(this.btn_cerrar_sesion_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::Capa_Presentacion.Properties.Resources.iconmonstr_building_21_2401;
            this.pictureBox2.Location = new System.Drawing.Point(-29, 22);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(277, 193);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 0;
            this.pictureBox2.TabStop = false;
            // 
            // button1
            // 
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button1.Location = new System.Drawing.Point(13, 320);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(205, 39);
            this.button1.TabIndex = 2;
            this.button1.Text = "REPORTE";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Frm_Administrador
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1233, 540);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Frm_Administrador";
            this.Opacity = 0.9D;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Frm_Administrador";
            this.Load += new System.EventHandler(this.Frm_Administrador_Load);
            this.panel2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_habitaciones)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_cerrar_sesion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.DataGridView dgv_habitaciones;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID_HABITACION;
        private System.Windows.Forms.DataGridViewComboBoxColumn PLANTA;
        private System.Windows.Forms.DataGridViewComboBoxColumn TIPO;
        private System.Windows.Forms.DataGridViewCheckBoxColumn BALCON;
        private System.Windows.Forms.DataGridViewTextBoxColumn FECHA_INGRESO;
        private System.Windows.Forms.DataGridViewTextBoxColumn FECHA_SALIDA;
        private System.Windows.Forms.DataGridViewImageColumn FOTO;
        private System.Windows.Forms.DataGridViewButtonColumn btn_agregar;
        private System.Windows.Forms.DataGridViewButtonColumn btn_modificar;
        private System.Windows.Forms.DataGridViewButtonColumn btn_eliminar;
        private System.Windows.Forms.PictureBox btn_cerrar_sesion;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Button button1;
    }
}