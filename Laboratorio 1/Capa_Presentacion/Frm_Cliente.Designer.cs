﻿
namespace Capa_Presentacion
{
    partial class Frm_Cliente
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.btn_cerrar_sesion = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.dgv_habitaciones = new System.Windows.Forms.DataGridView();
            this.btn_salir = new System.Windows.Forms.PictureBox();
            this.ID_HABITACION = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PLANTA = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.TIPO = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.BALCON = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.FOTO = new System.Windows.Forms.DataGridViewImageColumn();
            this.btn_reservar = new System.Windows.Forms.DataGridViewButtonColumn();
            ((System.ComponentModel.ISupportInitialize)(this.btn_cerrar_sesion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_habitaciones)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_salir)).BeginInit();
            this.SuspendLayout();
            // 
            // btn_cerrar_sesion
            // 
            this.btn_cerrar_sesion.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.btn_cerrar_sesion.Image = global::Capa_Presentacion.Properties.Resources.iniciar_sesion;
            this.btn_cerrar_sesion.Location = new System.Drawing.Point(3, 487);
            this.btn_cerrar_sesion.Name = "btn_cerrar_sesion";
            this.btn_cerrar_sesion.Size = new System.Drawing.Size(226, 50);
            this.btn_cerrar_sesion.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btn_cerrar_sesion.TabIndex = 1;
            this.btn_cerrar_sesion.TabStop = false;
            this.btn_cerrar_sesion.Click += new System.EventHandler(this.btn_cerrar_sesion_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::Capa_Presentacion.Properties.Resources.iconmonstr_building_21_2401;
            this.pictureBox2.Location = new System.Drawing.Point(-29, 22);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(277, 193);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 0;
            this.pictureBox2.TabStop = false;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.DimGray;
            this.panel2.Controls.Add(this.btn_cerrar_sesion);
            this.panel2.Controls.Add(this.pictureBox2);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(235, 547);
            this.panel2.TabIndex = 2;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.Black;
            this.panel3.Controls.Add(this.dgv_habitaciones);
            this.panel3.Controls.Add(this.btn_salir);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(235, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(599, 547);
            this.panel3.TabIndex = 3;
            // 
            // dgv_habitaciones
            // 
            this.dgv_habitaciones.AllowUserToAddRows = false;
            this.dgv_habitaciones.AllowUserToDeleteRows = false;
            this.dgv_habitaciones.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgv_habitaciones.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgv_habitaciones.BackgroundColor = System.Drawing.Color.DimGray;
            this.dgv_habitaciones.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgv_habitaciones.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.DimGray;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.DimGray;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_habitaciones.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv_habitaciones.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_habitaciones.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ID_HABITACION,
            this.PLANTA,
            this.TIPO,
            this.BALCON,
            this.FOTO,
            this.btn_reservar});
            this.dgv_habitaciones.EnableHeadersVisualStyles = false;
            this.dgv_habitaciones.GridColor = System.Drawing.Color.DimGray;
            this.dgv_habitaciones.Location = new System.Drawing.Point(19, 35);
            this.dgv_habitaciones.Name = "dgv_habitaciones";
            this.dgv_habitaciones.ReadOnly = true;
            this.dgv_habitaciones.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.DimGray;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_habitaciones.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dgv_habitaciones.RowHeadersVisible = false;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.DimGray;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.LightGray;
            this.dgv_habitaciones.RowsDefaultCellStyle = dataGridViewCellStyle4;
            this.dgv_habitaciones.Size = new System.Drawing.Size(568, 492);
            this.dgv_habitaciones.TabIndex = 1;
            this.dgv_habitaciones.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_habitaciones_CellContentClick);
            // 
            // btn_salir
            // 
            this.btn_salir.Image = global::Capa_Presentacion.Properties.Resources.iconmonstr_x_mark_1_240;
            this.btn_salir.Location = new System.Drawing.Point(572, 12);
            this.btn_salir.Name = "btn_salir";
            this.btn_salir.Size = new System.Drawing.Size(15, 15);
            this.btn_salir.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btn_salir.TabIndex = 0;
            this.btn_salir.TabStop = false;
            this.btn_salir.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // ID_HABITACION
            // 
            this.ID_HABITACION.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.ID_HABITACION.Frozen = true;
            this.ID_HABITACION.HeaderText = "ID_HABITACION";
            this.ID_HABITACION.MinimumWidth = 80;
            this.ID_HABITACION.Name = "ID_HABITACION";
            this.ID_HABITACION.ReadOnly = true;
            this.ID_HABITACION.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.ID_HABITACION.Width = 116;
            // 
            // PLANTA
            // 
            this.PLANTA.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.PLANTA.Frozen = true;
            this.PLANTA.HeaderText = "PLANTA";
            this.PLANTA.Items.AddRange(new object[] {
            "PLANTA1",
            "PLANTA2",
            "PLANTA3"});
            this.PLANTA.Name = "PLANTA";
            this.PLANTA.ReadOnly = true;
            this.PLANTA.Width = 53;
            // 
            // TIPO
            // 
            this.TIPO.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.TIPO.Frozen = true;
            this.TIPO.HeaderText = "TIPO HABITACION";
            this.TIPO.Items.AddRange(new object[] {
            "JUNIOR",
            "SUPERIOR",
            "FAMILIAR"});
            this.TIPO.Name = "TIPO";
            this.TIPO.ReadOnly = true;
            this.TIPO.Width = 97;
            // 
            // BALCON
            // 
            this.BALCON.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.BALCON.Frozen = true;
            this.BALCON.HeaderText = "BALCON";
            this.BALCON.Name = "BALCON";
            this.BALCON.ReadOnly = true;
            this.BALCON.Width = 59;
            // 
            // FOTO
            // 
            this.FOTO.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.FOTO.HeaderText = "FOTO HABITACION";
            this.FOTO.Name = "FOTO";
            this.FOTO.ReadOnly = true;
            this.FOTO.Width = 102;
            // 
            // btn_reservar
            // 
            this.btn_reservar.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.DimGray;
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.DimGray;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.White;
            this.btn_reservar.DefaultCellStyle = dataGridViewCellStyle2;
            this.btn_reservar.HeaderText = "RESERVAR";
            this.btn_reservar.Name = "btn_reservar";
            this.btn_reservar.ReadOnly = true;
            this.btn_reservar.Text = "RESERVAR";
            this.btn_reservar.ToolTipText = "RESERVAR";
            // 
            // Frm_Cliente
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(834, 547);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Frm_Cliente";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Frm_Cliente";
            this.Load += new System.EventHandler(this.Frm_Cliente_Load);
            ((System.ComponentModel.ISupportInitialize)(this.btn_cerrar_sesion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_habitaciones)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_salir)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox btn_cerrar_sesion;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.DataGridView dgv_habitaciones;
        private System.Windows.Forms.PictureBox btn_salir;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID_HABITACION;
        private System.Windows.Forms.DataGridViewComboBoxColumn PLANTA;
        private System.Windows.Forms.DataGridViewComboBoxColumn TIPO;
        private System.Windows.Forms.DataGridViewCheckBoxColumn BALCON;
        private System.Windows.Forms.DataGridViewImageColumn FOTO;
        private System.Windows.Forms.DataGridViewButtonColumn btn_reservar;
    }
}