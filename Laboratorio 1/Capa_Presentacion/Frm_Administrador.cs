﻿using Capa_Negocio;
using Capa_Objetos;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Capa_Presentacion
{
    public partial class Frm_Administrador : Form
    {

        Habitacion habitacion = new Habitacion();
        List<Habitacion> lista_habitacion = new List<Habitacion>();
        NG_Habitacion ng_habitacion = new NG_Habitacion();
        DateTimePicker dtp = new DateTimePicker();
        OpenFileDialog ofd = new OpenFileDialog();
        Rectangle rtg;

        public Frm_Administrador()
        {
            InitializeComponent();
            Agregar_Objetos_a_DataGrid();
        }

        public void Agregar_Objetos_a_DataGrid()
        {
            dgv_habitaciones.Controls.Add(dtp);
            dtp.Visible = false;
            dtp.Format = DateTimePickerFormat.Custom;
            dtp.TextChanged += new EventHandler(dtp_TextChange);
        }

        public void Obtener_Datos(DataGridViewCellEventArgs e)
        {
            DataGridViewRow row = dgv_habitaciones.Rows[e.RowIndex];
            habitacion.id_habitacion = row.Cells["ID_HABITACION"].Value.ToString();
            habitacion.planta = row.Cells["PLANTA"].Value.ToString();
            habitacion.tipo = row.Cells["TIPO"].Value.ToString();
            habitacion.balcon = Convert.ToBoolean(row.Cells["BALCON"].Value);
            habitacion.fecha_ingreso = Convert.ToDateTime(row.Cells["FECHA_INGRESO"].Value);
            habitacion.fecha_salida = Convert.ToDateTime(row.Cells["FECHA_SALIDA"].Value);
            habitacion.estado = "DISPONIBLE";
        }

        public void Mostrar_Habitaciones()
        {
            

            lista_habitacion = ng_habitacion.Extraer_Habitacion();

            

            for(int i = 0; i < lista_habitacion.Count; i++)
            {

                DataGridViewRow fila = new DataGridViewRow();
                fila.CreateCells(dgv_habitaciones);

                fila.Cells[0].Value = lista_habitacion[i].id_habitacion;
                fila.Cells[1].Value = lista_habitacion[i].planta;
                fila.Cells[2].Value = lista_habitacion[i].tipo;
                fila.Cells[3].Value = Convert.ToBoolean(lista_habitacion[i].balcon);
                fila.Cells[4].Value = Convert.ToDateTime(lista_habitacion[i].fecha_ingreso); 
                fila.Cells[5].Value = Convert.ToDateTime(lista_habitacion[i].fecha_salida);
                fila.Cells[6].Value = Image.FromFile(lista_habitacion[i].foto);

                dgv_habitaciones.Rows.Add(fila);

            }
            

        }


        public void dtp_TextChange(Object sender, EventArgs e)
        {
            dgv_habitaciones.CurrentCell.Value = dtp.Text.ToString();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

      
        private void dgv_habitaciones_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

            switch (e.ColumnIndex)
            {

                case 6:

                    if ( ofd.ShowDialog() == DialogResult.OK)
                    {
                        string imagen = ofd.FileName;
                        dgv_habitaciones.CurrentRow.Cells[6].Value = Image.FromFile(imagen);
                        habitacion.foto = imagen;
                    }
                    break;
            }

            if (e.ColumnIndex == 7)
            {
                Obtener_Datos(e);
                ng_habitacion.Agregar_Habitacion(habitacion);
                dgv_habitaciones.Rows.Clear();
                Mostrar_Habitaciones();
            } 

            if(e.ColumnIndex == 8)
            {
                DialogResult respuesta = MessageBox.Show("¿DESEA MODIFICAR LA HABITACIÓN?", "¿MODIFICAR HABITACIÓN?", MessageBoxButtons.YesNo);

                if (respuesta == DialogResult.Yes)
                {
                    Obtener_Datos(e);
                    ng_habitacion.Modificar_Habitacion(habitacion);
                    dgv_habitaciones.Rows.Clear();
                    Mostrar_Habitaciones();

                }

            }

            if(e.ColumnIndex == 9)
            {

                DialogResult respuesta = MessageBox.Show("¿DESEA ELIMINAR LA HABITACIÓN?", "¿ELIMINAR HABITACIÓN?", MessageBoxButtons.YesNo);

                if (respuesta == DialogResult.Yes)
                {
                    Obtener_Datos(e);
                    ng_habitacion.Eliminar_Habitaciones(habitacion);
                    dgv_habitaciones.Rows.Clear();
                    Mostrar_Habitaciones();

                }

            }

        }

        private void dgv_habitaciones_Scroll(object sender, ScrollEventArgs e)
        {
            dtp.Visible = false;
        }

        private void dgv_habitaciones_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            switch (dgv_habitaciones.Columns[e.ColumnIndex].Name)
            {
                case "FECHA_INGRESO":

                    rtg = dgv_habitaciones.GetCellDisplayRectangle(e.ColumnIndex, e.RowIndex, true);
                    dtp.Size = new Size(rtg.Width,  rtg.Height);
                    dtp.Location = new Point(rtg.X, rtg.Y);
                    dtp.Visible = true;
                    break;

                case "FECHA_SALIDA":

                    rtg = dgv_habitaciones.GetCellDisplayRectangle(e.ColumnIndex, e.RowIndex, true);
                    dtp.Size = new Size(rtg.Width, rtg.Height);
                    dtp.Location = new Point(rtg.X, rtg.Y);
                    dtp.Visible = true;
                    break;
            }
        }

        private void openFileDialog1_FileOk(object sender, CancelEventArgs e)
        {

        }

        private void panel3_Paint(object sender, PaintEventArgs e)
        {

        }

        private void Frm_Administrador_Load(object sender, EventArgs e)
        {
            Mostrar_Habitaciones();
        }

        private void btn_cerrar_sesion_Click(object sender, EventArgs e)
        {
            DialogResult respuesta = MessageBox.Show("¿DESEA CERRAR SESIÓN?", "¿CERRAR SESIÓN?",
               MessageBoxButtons.YesNo);

            if (respuesta == DialogResult.Yes)
            {
                Frm_Inicio_Sesion frm_is = new Frm_Inicio_Sesion();
                frm_is.Show();
                this.Hide();
                
            }

        }

        private void button1_Click(object sender, EventArgs e)
        {
            Frm_Reporte frm_r = new Frm_Reporte();
            frm_r.Show();
        }
    }
}
