﻿using Capa_Negocio;
using Capa_Objetos;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Capa_Presentacion
{
    public partial class Frm_Reporte : Form
    {

        NG_Cliente ng_cliente = new NG_Cliente();
        Cliente cliente = new Cliente();

        public Frm_Reporte()
        {
            InitializeComponent();
        }

        public void Cargar_Combo()
        {
            cbx_clientes.DataSource = ng_cliente.Obtener();
            cbx_clientes.DisplayMember = "nombre";
            cbx_clientes.ValueMember = "nombre";

        }

        private void cbx_clientes_SelectedIndexChanged(object sender, EventArgs e)
        {
            cliente.nombre = Convert.ToString(cbx_clientes.SelectedValue);
            ng_cliente.Reporte(cliente, dgv_reporte);

        }

        private void btn_cerrar_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void Frm_Reporte_Load(object sender, EventArgs e)
        {
            Cargar_Combo();
        }
    }
}
