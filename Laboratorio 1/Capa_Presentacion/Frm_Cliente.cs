﻿using Capa_Negocio;
using Capa_Objetos;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Capa_Presentacion
{
    public partial class Frm_Cliente : Form
    {

        Habitacion habitacion = new Habitacion();
        List<Habitacion> lista_habitacion = new List<Habitacion>();
        NG_Habitacion ng_habitacion = new NG_Habitacion();
        NG_Cliente_Habitacion ng_cliente_habitacion = new NG_Cliente_Habitacion();
        Cliente cliente = new Cliente();
        Cliente_Habitacion cl_ha = new Cliente_Habitacion();

        public Frm_Cliente()
        {
            InitializeComponent();
        }


        public void Mostrar_Habitaciones()
        {

            lista_habitacion = ng_habitacion.Extraer_Habitacion_Disponible();

            for (int i = 0; i < lista_habitacion.Count; i++)
            {

                DataGridViewRow fila = new DataGridViewRow();
                fila.CreateCells(dgv_habitaciones);

                fila.Cells[0].Value = lista_habitacion[i].id_habitacion;
                fila.Cells[1].Value = lista_habitacion[i].planta;
                fila.Cells[2].Value = lista_habitacion[i].tipo;
                fila.Cells[3].Value = Convert.ToBoolean(lista_habitacion[i].balcon);
                fila.Cells[4].Value = Image.FromFile(lista_habitacion[i].foto);

                dgv_habitaciones.Rows.Add(fila);

            }

        }

        public void Obtener_Datos(DataGridViewCellEventArgs e)
        {
            DataGridViewRow row = dgv_habitaciones.Rows[e.RowIndex];
            habitacion.id_habitacion = row.Cells["ID_HABITACION"].Value.ToString();

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btn_cerrar_sesion_Click(object sender, EventArgs e)
        {
            DialogResult respuesta = MessageBox.Show("¿DESEA CERRAR SESIÓN?", "¿CERRAR SESIÓN?", MessageBoxButtons.YesNo);

            if (respuesta == DialogResult.Yes)
            {
                Frm_Inicio_Sesion frm_is = new Frm_Inicio_Sesion();
                frm_is.Show();
                this.Hide();

            }
        }

        private void Frm_Cliente_Load(object sender, EventArgs e)
        {
            Mostrar_Habitaciones();
        }

        private void dgv_habitaciones_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 5)
            {

                DialogResult respuesta = MessageBox.Show("¿DESEA RESERVAR ESTA HABITACION?", "¿DESEA RESERVAR?", MessageBoxButtons.YesNo);

                if (respuesta == DialogResult.Yes)
                {
                    Obtener_Datos(e);
                    ng_habitacion.Reservar_Habitacion(habitacion);
                    cl_ha.cedula = Frm_Inicio_Sesion.cedula;
                    cl_ha.id_habitacion = habitacion.id_habitacion;
                    cl_ha.fecha_reserva = DateTime.Now;
                    ng_cliente_habitacion.Realizar_Reserva(cl_ha);
                    dgv_habitaciones.Rows.Clear();
                    Mostrar_Habitaciones();
                }
                //Obtener_Datos(e);
                //ng_habitacion.Agregar_Habitacion(habitacion);
                //dgv_habitaciones.Rows.Clear();
                //Mostrar_Habitaciones();
            }
        }
    }
}
