﻿using Capa_Negocio;
using Capa_Objetos;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Capa_Presentacion
{
    public partial class Frm_Inicio_Sesion : Form
    {

        NG_Cliente ng_cliente = new NG_Cliente();
        Cliente cliente = new Cliente();
        String contraseña;
        public static int cedula;
        Frm_Administrador frm_admin = new Frm_Administrador();
        Frm_Cliente frm_cliente = new Frm_Cliente();

        public Frm_Inicio_Sesion()
        {
            InitializeComponent();
        }

        public void Obtener_Datos()
        {
            cliente.cedula = Convert.ToInt32(txt_usuario.Text);
            contraseña = txt_contraseña.Text;
        }

        public void Validar_Inicio_Sesion()
        {
            if(cliente.rol == "ADMIN" && contraseña == "Admin123")
            {
                MessageBox.Show("BIENVENIDO AL SISTEMA: SR(A) " + cliente.nombre , "INFORMACIÓN");
                frm_admin.Show();
                this.Hide();
            }
            else if (cliente.rol == "CLIENTE" && contraseña == "Cliente123")
            {
                MessageBox.Show("BIENVENIDO AL SISTEMA: SR(A) " + cliente.nombre, "INFORMACIÓN");
                cedula = cliente.cedula;
                frm_cliente.Show();
                this.Hide();
            }
            else
            {
                MessageBox.Show("ERROR, USUARIO O CONTRASEÑA INCORRECTOS", "ERROR");
            }
        }

        private void btn_iniciar_sesion_Click(object sender, EventArgs e)
        {
            Obtener_Datos();
            ng_cliente.Consulta_Inicio_Sesion(cliente);
            Validar_Inicio_Sesion();
        }

        private void lineShape1_Click(object sender, EventArgs e)
        {

        }

        private void txt_usuario_Enter(object sender, EventArgs e)
        {
            if (txt_usuario.Text == "USUARIO")
            {
                txt_usuario.Text = "";
                txt_usuario.ForeColor = Color.DimGray;
            }
        }

        private void txt_usuario_Leave(object sender, EventArgs e)
        {
            if (txt_usuario.Text == "")
            {
                txt_usuario.Text = "USUARIO";
                txt_usuario.ForeColor = Color.DimGray;
            }
        }

        private void txt_contraseña_Enter(object sender, EventArgs e)
        {
            if (txt_contraseña.Text == "CONTRASEÑA")
            {
                txt_contraseña.Text = "";
                txt_contraseña.ForeColor = Color.DimGray;
                txt_contraseña.UseSystemPasswordChar = true;
            }
        }

        private void txt_contraseña_Leave(object sender, EventArgs e)
        {
            if (txt_contraseña.Text == "")
            {
                txt_contraseña.Text = "CONTRASEÑA";
                txt_contraseña.ForeColor = Color.DimGray;
                txt_contraseña.UseSystemPasswordChar = false;
            }
        }

        private void btn_cerrar_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
