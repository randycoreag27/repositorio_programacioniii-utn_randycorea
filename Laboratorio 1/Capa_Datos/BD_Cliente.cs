﻿using Capa_Objetos;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Capa_Datos
{
    public class BD_Cliente
    {

        static NpgsqlConnection conexion;
        static NpgsqlCommand cmd;


        public void Consulta_Inicio_Sesion(Cliente c)
        {
            conexion = Conexion_BD.connection();
            conexion.Open();
            cmd = new NpgsqlCommand("SELECT NOMBRE, CEDULA, ROL FROM CLIENTE WHERE CEDULA = '" + c.cedula + "'", conexion);
            NpgsqlDataReader rd = cmd.ExecuteReader();

            if (rd.Read())
            {

                c.nombre = rd.GetString(0);
                c.cedula = rd.GetInt32(1);
                c.rol = rd.GetString(2);

            }
            else
            {
                MessageBox.Show("EL CLIENTE NO SE ENCUENTRA REGISTRADO EN NUESTRA BASE DE DATOS", "ERROR");
            }

            conexion.Close();
        }


        public List<Cliente> Obtener_Clientes()
        {
            List<Cliente> lista_clientes = new List<Cliente>();

            conexion = Conexion_BD.connection();
            conexion.Open();
            cmd = new NpgsqlCommand("SELECT * FROM CLIENTE WHERE ROL= 'CLIENTE' ", conexion);
            NpgsqlDataReader rd = cmd.ExecuteReader();

            while (rd.Read())
            {
                Cliente cli = new Cliente();

                cli.id_cliente = rd.GetInt32(0);
                cli.cedula = rd.GetInt32(1);
                cli.nombre = rd.GetString(2);
                lista_clientes.Add(cli);
            }

            conexion.Close();

            return lista_clientes;
        }

        public void Reporte(Cliente cliente, DataGridView dgv)
        {
            conexion = Conexion_BD.connection();
            DataTable data_table = new DataTable();
            NpgsqlDataAdapter adapter = new NpgsqlDataAdapter("SELECT C.NOMBRE, H.PLANTA, H.TIPO, H.BALCON, CH.FECHA_RESERVA FROM HABITACION H " +
                "INNER JOIN CLIENTE_HABITACION CH ON H.ID_HABITACION = CH.ID_HABITACION INNER JOIN CLIENTE C ON CH.CEDULA = C.CEDULA " +
                "WHERE C.NOMBRE = '" + cliente.nombre + "'", conexion);
            adapter.Fill(data_table);
            dgv.DataSource = data_table;

        }

    }
}
