﻿using Capa_Objetos;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Capa_Datos
{
    public class BD_Habitacion
    {

        static NpgsqlConnection conexion;
        static NpgsqlCommand cmd;


        public void Agregar_Habitacion(Habitacion ha)
        {
            conexion = Conexion_BD.connection();
            conexion.Open();
            cmd = new NpgsqlCommand("INSERT INTO HABITACION (ID_HABITACION, PLANTA, TIPO, BALCON, FECHA_INGRESO, FECHA_SALIDA, ESTADO, FOTO) VALUES " +
            "('"+ ha.id_habitacion + "', '" + ha.planta + "', '" + ha.tipo + "', '" + ha.balcon + "', '" + ha.fecha_ingreso + "', '" + ha.fecha_salida + "', '" + ha.estado + "', '" + ha.foto + "')", conexion);
            cmd.ExecuteNonQuery();
            conexion.Close();

        }

        public List<Habitacion> Extraer_Habitaciones()
        {

            List<Habitacion> habitaciones = new List<Habitacion>();

            conexion = Conexion_BD.connection();
            conexion.Open();
            cmd = new NpgsqlCommand("SELECT * FROM HABITACION", conexion);
            NpgsqlDataReader rd = cmd.ExecuteReader();
            //cmd.ExecuteNonQuery();
            
            while (rd.Read())
            {
                Habitacion hab = new Habitacion();

                hab.id_habitacion = rd.GetString(0);
                hab.planta = rd.GetString(1);
                hab.tipo = rd.GetString(2);
                hab.balcon = rd.GetBoolean(3);
                hab.fecha_ingreso = (System.DateTime)rd.GetValue(4);
                hab.fecha_salida = (System.DateTime)rd.GetValue(5);
                hab.estado = rd.GetString(6);
                hab.foto = rd.GetString(7);

                habitaciones.Add(hab);

            }

            conexion.Close();

            return habitaciones;
        }

        public List<Habitacion> Extraer_Habitaciones_Disponibles()
        {

            List<Habitacion> habitaciones = new List<Habitacion>();

            conexion = Conexion_BD.connection();
            conexion.Open();
            cmd = new NpgsqlCommand("SELECT * FROM HABITACION WHERE ESTADO = 'DISPONIBLE' ", conexion);
            NpgsqlDataReader rd = cmd.ExecuteReader();
            //cmd.ExecuteNonQuery();

            while (rd.Read())
            {
                Habitacion hab = new Habitacion();

                hab.id_habitacion = rd.GetString(0);
                hab.planta = rd.GetString(1);
                hab.tipo = rd.GetString(2);
                hab.balcon = rd.GetBoolean(3);
                hab.fecha_ingreso = (System.DateTime)rd.GetValue(4);
                hab.fecha_salida = (System.DateTime)rd.GetValue(5);
                hab.estado = rd.GetString(6);
                hab.foto = rd.GetString(7);

                habitaciones.Add(hab);

            }

            conexion.Close();

            return habitaciones;
        }


        public void Eliminar_Habitacion(Habitacion ha)
        {
            conexion = Conexion_BD.connection();
            conexion.Open();
            cmd = new NpgsqlCommand("DELETE FROM HABITACION WHERE ID_HABITACION =  '" + ha.id_habitacion + "'", conexion);
            cmd.ExecuteNonQuery();
            conexion.Close();
        }

        public void Modificar_Habitacion(Habitacion ha)
        {
            conexion = Conexion_BD.connection();
            conexion.Open();
            cmd = new NpgsqlCommand("UPDATE HABITACION SET PLANTA = '" + ha.planta + "', " + "TIPO = '" + ha.tipo + "', " +
                "BALCON = '" + ha.balcon + "', " + "FECHA_INGRESO = '" + ha.fecha_ingreso + "', " + "FECHA_SALIDA = '" + ha.fecha_salida
                + "', " + "FOTO = '" + ha.foto + "'" + "' WHERE ID_HABITACION = '" + ha.id_habitacion + "'", conexion);
            cmd.ExecuteNonQuery();
            conexion.Close();
            MessageBox.Show("DATOS MODIFICADOS CORRECTAMENTE");

        }

        public void Reservar_Habitacion(Habitacion ha)
        {
            conexion = Conexion_BD.connection();
            conexion.Open();
            cmd = new NpgsqlCommand("UPDATE HABITACION SET ESTADO = 'RESERVADO' WHERE ID_HABITACION = '" + ha.id_habitacion + "'" , conexion);
            cmd.ExecuteNonQuery();
            conexion.Close();
        }


    }
}
