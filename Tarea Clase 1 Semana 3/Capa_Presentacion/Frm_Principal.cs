﻿using System;
using System.Windows.Forms;

namespace Capa_Presentacion
{
    public partial class Frm_Principal : Form
    {

        public Frm_Principal()
        {
            InitializeComponent();
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void añadirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Frm_Añadir_Colaborador frm_ac = new Frm_Añadir_Colaborador();
            Form_Añadir_Colaborador(frm_ac);
        }

        public void Form_Añadir_Colaborador(Form Frm_AC)
        {
            Frm_AC.TopLevel = false;
            this.Panel_From.Controls.Add(Frm_AC);
            Frm_AC.Show();
        }

        public void Form_Editar_Colaborador(Form Frm_EC)
        {
            Frm_EC.TopLevel = false;
            this.Panel_From.Controls.Add(Frm_EC);
            Frm_EC.Show();
        }

        public void Form_Eliminar_Colaborador(Form Frm_ELC)
        {
            Frm_ELC.TopLevel = false;
            this.Panel_From.Controls.Add(Frm_ELC);
            Frm_ELC.Show();
        }

        public void Form_Lista_Colaboradores(Form Frm_LC)
        {
            Frm_LC.TopLevel = false;
            this.Panel_From.Controls.Add(Frm_LC);
            Frm_LC.Show();
        }

        public void Form_Reporte1(Form Frm_R1)
        {
            Frm_R1.TopLevel = false;
            this.Panel_From.Controls.Add(Frm_R1);
            Frm_R1.Show();
        }

        public void Form_Reporte2(Form Frm_R2)
        {
            Frm_R2.TopLevel = false;
            this.Panel_From.Controls.Add(Frm_R2);
            Frm_R2.Show();
        }

        public void Form_Reporte3(Form Frm_R3)
        {
            Frm_R3.TopLevel = false;
            this.Panel_From.Controls.Add(Frm_R3);
            Frm_R3.Show();
        }

        public void Form_Reporte4(Form Frm_R4)
        {
            Frm_R4.TopLevel = false;
            this.Panel_From.Controls.Add(Frm_R4);
            Frm_R4.Show();
        }

        private void editarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Frm_Editar_Colaborador frm_ec = new Frm_Editar_Colaborador();
            Form_Editar_Colaborador(frm_ec);
        }

        private void eliminarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Frm_Eliminar_Colaborador frm_elc = new Frm_Eliminar_Colaborador();
            Form_Eliminar_Colaborador(frm_elc);
        }

        private void listaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Frm_Lista_Colaboradores frm_lc = new Frm_Lista_Colaboradores();
            Form_Lista_Colaboradores(frm_lc);
        }

        private void Frm_Principal_Load(object sender, EventArgs e)
        {

        }

        private void reporte1ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Frm_Reporte1 frm_r1 = new Frm_Reporte1();
            Form_Reporte1(frm_r1);
        }

        private void reporte2ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Frm_Reporte2 frm_r2 = new Frm_Reporte2();
            Form_Reporte2(frm_r2);
        }

        private void reporte3ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Frm_Reporte3 frm_r3 = new Frm_Reporte3();
            Form_Reporte3(frm_r3);
        }

        private void reporte4ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Frm_Reporte4 frm_r4 = new Frm_Reporte4();
            Form_Reporte4(frm_r4);
        }
    }
}
