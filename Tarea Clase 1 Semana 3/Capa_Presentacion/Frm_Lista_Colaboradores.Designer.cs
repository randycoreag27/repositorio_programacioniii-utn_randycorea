﻿
namespace Capa_Presentacion
{
    partial class Frm_Lista_Colaboradores
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgv_colaboradores = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_colaboradores)).BeginInit();
            this.SuspendLayout();
            // 
            // dgv_colaboradores
            // 
            this.dgv_colaboradores.AllowUserToAddRows = false;
            this.dgv_colaboradores.AllowUserToDeleteRows = false;
            this.dgv_colaboradores.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_colaboradores.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv_colaboradores.Enabled = false;
            this.dgv_colaboradores.Location = new System.Drawing.Point(0, 0);
            this.dgv_colaboradores.Name = "dgv_colaboradores";
            this.dgv_colaboradores.ReadOnly = true;
            this.dgv_colaboradores.Size = new System.Drawing.Size(1024, 157);
            this.dgv_colaboradores.TabIndex = 0;
            // 
            // Frm_Lista_Colaboradores
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1024, 157);
            this.Controls.Add(this.dgv_colaboradores);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Frm_Lista_Colaboradores";
            this.Text = "Lista Colaboradores";
            this.Load += new System.EventHandler(this.Frm_Lista_Colaboradores_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_colaboradores)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgv_colaboradores;
    }
}