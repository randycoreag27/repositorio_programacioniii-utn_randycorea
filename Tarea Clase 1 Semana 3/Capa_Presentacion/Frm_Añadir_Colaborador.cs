﻿using Capa_Negocio;
using Capa_Objetos;
using System;
using System.Windows.Forms;

namespace Capa_Presentacion
{
    public partial class Frm_Añadir_Colaborador : Form
    {
        OBJ_Colaborador obj_c = new OBJ_Colaborador();
        Colaborador c = new Colaborador();
        Puesto p = new Puesto();
        Departamento d = new Departamento();
        Nivel_Ingles ni = new Nivel_Ingles();

        public Frm_Añadir_Colaborador()
        {
            InitializeComponent();
        }

        public void Cargar_Combo_Puesto()
        {
            cbx_puesto.DataSource = p.Obtener();
            cbx_puesto.DisplayMember = "nombre";
            cbx_puesto.ValueMember = "id";
        }

        public void Cargar_Combo_Departamento()
        {
            cbx_departamento.DataSource = d.Obtener();
            cbx_departamento.DisplayMember = "nombre";
            cbx_departamento.ValueMember = "id";
        }

        public void Cargar_Combo_Niveles_Ingles()
        {
            cbx_niveles.DataSource = ni.Obtener();
            cbx_niveles.DisplayMember = "nombre";
            cbx_niveles.ValueMember = "nombre";
        }

        public void Obtener()
        {
            obj_c.cedula = Convert.ToInt32(txt_cedula.Text);
            obj_c.nombre = txt_nombre.Text;
            obj_c.genero = Genero();
            obj_c.departamento = Convert.ToInt32(cbx_departamento.SelectedValue);
            obj_c.fecha_nacimiento = dtp_fecha_nacimiento.Value;
            obj_c.edad = Convert.ToInt32(txt_edad.Text); ;
            obj_c.fecha_ingreso = dtp_fecha_ingreso.Value;
            obj_c.nivel_ingles = Convert.ToString(cbx_niveles.SelectedValue);
            obj_c.puesto = Convert.ToInt32(cbx_puesto.SelectedValue);
        }

        public string Genero()
        {
            string genero = "";

            if(rb_masculino.Checked == true)
            {
                genero = "Masculino";
            }
            else if (rb_femenino.Checked == true)
            {
                genero = "Femenino";
            }

            return genero;
        }

        public int Edad()
        {
            int edad = 0;
            DateTime fa = DateTime.Today;

            edad = fa.Year - obj_c.fecha_nacimiento.Year;
            
            if(obj_c.fecha_nacimiento.Month > fa.Month)
            {
                --edad;
            }

            return edad;
        }

        public void Limpiar()
        {
            txt_cedula.Clear();
            txt_nombre.Clear();
            txt_edad.Clear();
            cbx_departamento.ResetText();
            cbx_niveles.ResetText();
            cbx_puesto.ResetText();
            dtp_fecha_nacimiento.ResetText();
            dtp_fecha_ingreso.ResetText();

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void btn_añadir_Click(object sender, EventArgs e)
        {
            Obtener();
            c.Añadir(obj_c);
            Limpiar();
        }

        private void dtp_fecha_nacimiento_ValueChanged(object sender, EventArgs e)
        {
            obj_c.fecha_nacimiento = dtp_fecha_nacimiento.Value;
            txt_edad.Text = Convert.ToString(Edad());
        }

        private void Frm_Añadir_Colaborador_Load(object sender, EventArgs e)
        {
            Cargar_Combo_Puesto();
            Cargar_Combo_Departamento();
            Cargar_Combo_Niveles_Ingles();
        }
    }
}
