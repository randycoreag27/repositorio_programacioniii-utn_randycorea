﻿using Capa_Negocio;
using Capa_Objetos;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Capa_Presentacion
{
    public partial class Frm_Reporte2 : Form
    {

        OBJ_Colaborador obj_c = new OBJ_Colaborador();
        Colaborador c = new Colaborador();
        Nivel_Ingles ni = new Nivel_Ingles();

        public Frm_Reporte2()
        {
            InitializeComponent();
        }

        public void Cargar_Combo_Niveles_Ingles()
        {
            cbx_niveles.DataSource = ni.Obtener();
            cbx_niveles.DisplayMember = "nombre";
            cbx_niveles.ValueMember = "nombre";
        }

        private void txt_buscar_Click(object sender, EventArgs e)
        {
            obj_c.nivel_ingles = Convert.ToString(cbx_niveles.SelectedValue);
            c.Reporte_2(obj_c, dgv_colaboradores);
        }

        private void Frm_Reporte2_Load(object sender, EventArgs e)
        {
            Cargar_Combo_Niveles_Ingles();
        }
    }
}
