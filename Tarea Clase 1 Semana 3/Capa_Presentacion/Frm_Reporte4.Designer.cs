﻿
namespace Capa_Presentacion
{
    partial class Frm_Reporte4
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txt_buscar = new System.Windows.Forms.Button();
            this.label10 = new System.Windows.Forms.Label();
            this.dgv_colaboradores = new System.Windows.Forms.DataGridView();
            this.txt_1 = new System.Windows.Forms.TextBox();
            this.txt_2 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cbx_departamento = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_colaboradores)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 53);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(93, 22);
            this.label1.TabIndex = 84;
            this.label1.Text = "Edad Final:";
            // 
            // txt_buscar
            // 
            this.txt_buscar.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_buscar.Location = new System.Drawing.Point(16, 140);
            this.txt_buscar.Name = "txt_buscar";
            this.txt_buscar.Size = new System.Drawing.Size(841, 34);
            this.txt_buscar.TabIndex = 83;
            this.txt_buscar.Text = "Buscar Colaborador";
            this.txt_buscar.UseVisualStyleBackColor = true;
            this.txt_buscar.Click += new System.EventHandler(this.txt_buscar_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(12, 9);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(102, 22);
            this.label10.TabIndex = 82;
            this.label10.Text = "Edad Inicial:";
            // 
            // dgv_colaboradores
            // 
            this.dgv_colaboradores.AllowUserToAddRows = false;
            this.dgv_colaboradores.AllowUserToDeleteRows = false;
            this.dgv_colaboradores.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_colaboradores.Enabled = false;
            this.dgv_colaboradores.Location = new System.Drawing.Point(16, 194);
            this.dgv_colaboradores.Name = "dgv_colaboradores";
            this.dgv_colaboradores.ReadOnly = true;
            this.dgv_colaboradores.Size = new System.Drawing.Size(841, 113);
            this.dgv_colaboradores.TabIndex = 81;
            // 
            // txt_1
            // 
            this.txt_1.Font = new System.Drawing.Font("Trebuchet MS", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_1.Location = new System.Drawing.Point(135, 11);
            this.txt_1.Name = "txt_1";
            this.txt_1.Size = new System.Drawing.Size(722, 25);
            this.txt_1.TabIndex = 85;
            // 
            // txt_2
            // 
            this.txt_2.Font = new System.Drawing.Font("Trebuchet MS", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_2.Location = new System.Drawing.Point(135, 53);
            this.txt_2.Name = "txt_2";
            this.txt_2.Size = new System.Drawing.Size(722, 25);
            this.txt_2.TabIndex = 86;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(12, 98);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(123, 22);
            this.label2.TabIndex = 87;
            this.label2.Text = "Departamento:";
            // 
            // cbx_departamento
            // 
            this.cbx_departamento.Font = new System.Drawing.Font("Trebuchet MS", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbx_departamento.FormattingEnabled = true;
            this.cbx_departamento.Location = new System.Drawing.Point(135, 98);
            this.cbx_departamento.Name = "cbx_departamento";
            this.cbx_departamento.Size = new System.Drawing.Size(722, 28);
            this.cbx_departamento.TabIndex = 88;
            // 
            // Frm_Reporte4
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(871, 324);
            this.Controls.Add(this.cbx_departamento);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txt_2);
            this.Controls.Add(this.txt_1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txt_buscar);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.dgv_colaboradores);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Frm_Reporte4";
            this.Text = "Frm_Reporte4";
            this.Load += new System.EventHandler(this.Frm_Reporte4_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_colaboradores)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button txt_buscar;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.DataGridView dgv_colaboradores;
        private System.Windows.Forms.TextBox txt_1;
        private System.Windows.Forms.TextBox txt_2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cbx_departamento;
    }
}