﻿using Capa_Negocio;
using Capa_Objetos;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Capa_Presentacion
{
    public partial class Frm_Reporte4 : Form
    {

        Departamento d = new Departamento();
        OBJ_Colaborador obj_c = new OBJ_Colaborador();
        Colaborador c = new Colaborador();

        public Frm_Reporte4()
        {
            InitializeComponent();
        }

        public void Cargar_Combo_Departamento()
        {
            cbx_departamento.DataSource = d.Obtener();
            cbx_departamento.DisplayMember = "nombre";
            cbx_departamento.ValueMember = "id";
        }

        private void txt_buscar_Click(object sender, EventArgs e)
        {
            int edad_inicial = Convert.ToInt32(txt_1.Text);
            int edad_final = Convert.ToInt32(txt_2.Text);
            obj_c.departamento = Convert.ToInt32(cbx_departamento.SelectedValue);
            c.Reporte_4(obj_c, dgv_colaboradores, edad_inicial, edad_final);
        }

        private void Frm_Reporte4_Load(object sender, EventArgs e)
        {
            Cargar_Combo_Departamento();
        }
    }
}
