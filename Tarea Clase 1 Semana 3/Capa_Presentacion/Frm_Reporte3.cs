﻿using Capa_Negocio;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;

namespace Capa_Presentacion
{
    public partial class Frm_Reporte3 : Form
    {

        Colaborador c = new Colaborador();

        public Frm_Reporte3()
        {
            InitializeComponent();
        }

        private void txt_buscar_Click(object sender, EventArgs e)
        {
            DateTime fecha_inicial = dtp_1.Value;
            DateTime fecha_final = dtp_2.Value;

            c.Reporte_3(dgv_colaboradores, fecha_inicial, fecha_final);


        }

        private void dtp_1_ValueChanged(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void dtp_2_ValueChanged(object sender, EventArgs e)
        {

        }

        private void label10_Click(object sender, EventArgs e)
        {

        }

        private void dgv_colaboradores_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
