﻿using Capa_Negocio;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;

namespace Capa_Presentacion
{
    public partial class Frm_Lista_Colaboradores : Form
    {

        Colaborador c = new Colaborador();

        public Frm_Lista_Colaboradores()
        {
            InitializeComponent();
        }

        private void Frm_Lista_Colaboradores_Load(object sender, EventArgs e)
        {
            c.Mostrar(dgv_colaboradores);
        }
    }
}
