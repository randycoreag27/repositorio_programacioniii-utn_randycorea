﻿using Capa_Negocio;
using Capa_Objetos;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Capa_Presentacion
{
    public partial class Frm_Editar_Colaborador : Form
    {

        OBJ_Colaborador obj_c = new OBJ_Colaborador();
        Colaborador c = new Colaborador();
        List<OBJ_Colaborador> datos_colaborador = new List<OBJ_Colaborador>();
        Puesto p = new Puesto();
        Departamento d = new Departamento();
        Nivel_Ingles ni = new Nivel_Ingles();

        public Frm_Editar_Colaborador()
        {
            InitializeComponent();
        }

        public void Colocar()
        {
            txt_cedula.Text = Convert.ToString(datos_colaborador[0].cedula);
            txt_nombre.Text = datos_colaborador[0].nombre;
            Valor_Genero();
            cbx_departamento.SelectedIndex = datos_colaborador[0].departamento-1;
            dtp_fecha_nacimiento.Text = Convert.ToString(datos_colaborador[0].fecha_nacimiento);
            txt_edad.Text = Convert.ToString(datos_colaborador[0].edad);
            dtp_fecha_ingreso.Text = Convert.ToString(datos_colaborador[0].fecha_ingreso);
            cbx_niveles.SelectedIndex = Valor_Indice_Nivel_Ingles()-1;
            cbx_puesto.SelectedIndex = datos_colaborador[0].puesto-1;
        }

        public int Valor_Indice_Nivel_Ingles()
        {
            int index = 0;

            if (datos_colaborador[0].nivel_ingles=="A1")
            {
                index = 1;

            }else if (datos_colaborador[0].nivel_ingles == "A2")
            {
                index = 2;
            }
            else if (datos_colaborador[0].nivel_ingles == "B1")
            {
                index = 3;
            }
            else if (datos_colaborador[0].nivel_ingles == "B2")
            {
                index = 4;
            }
            else if (datos_colaborador[0].nivel_ingles == "C1")
            {
                index = 5;
            }

            return index;
        }

        public void Valor_Genero (){

            if(datos_colaborador[0].genero == "Masculino")
            {
                rb_masculino.Checked = true;
            }
            else if (datos_colaborador[0].genero == "Femenino")
            {
                rb_femenino.Checked = true;
            }

        }

        public void Obtener1()
        {
            obj_c.cedula = Convert.ToInt32(txt_buscar_cedula.Text);
        }

        public void Obtener2()
        {
            obj_c.cedula = Convert.ToInt32(txt_cedula.Text);
            obj_c.nombre = txt_nombre.Text;
            obj_c.genero = Genero();
            obj_c.departamento = Convert.ToInt32(cbx_departamento.SelectedValue);
            obj_c.fecha_nacimiento = dtp_fecha_nacimiento.Value;
            obj_c.edad = Convert.ToInt32(txt_edad.Text); ;
            obj_c.fecha_ingreso = dtp_fecha_ingreso.Value;
            obj_c.nivel_ingles = Convert.ToString(cbx_niveles.SelectedValue);
            obj_c.puesto = Convert.ToInt32(cbx_puesto.SelectedValue);
        }

        public void Cargar_Combo_Puesto()
        {
            cbx_puesto.DataSource = p.Obtener();
            cbx_puesto.DisplayMember = "nombre";
            cbx_puesto.ValueMember = "id";
        }

        public void Cargar_Combo_Departamento()
        {
            cbx_departamento.DataSource = d.Obtener();
            cbx_departamento.DisplayMember = "nombre";
            cbx_departamento.ValueMember = "id";
        }

        public void Cargar_Combo_Niveles_Ingles()
        {
            cbx_niveles.DataSource = ni.Obtener();
            cbx_niveles.DisplayMember = "nombre";
            cbx_niveles.ValueMember = "nombre";
        }

        public string Genero()
        {
            string genero = "";

            if (rb_masculino.Checked == true)
            {
                genero = "Masculino";
            }
            else if (rb_femenino.Checked == true)
            {
                genero = "Femenino";
            }

            return genero;
        }

        public int Edad()
        {
            int edad = 0;
            DateTime fa = DateTime.Today;

            edad = fa.Year - obj_c.fecha_nacimiento.Year;

            if (obj_c.fecha_nacimiento.Month > fa.Month)
            {
                --edad;
            }

            return edad;
        }

        public void Limpiar()
        {
            txt_cedula.Clear();
            txt_nombre.Clear();
            txt_edad.Clear();
            cbx_departamento.ResetText();
            cbx_niveles.ResetText();
            cbx_puesto.ResetText();
            dtp_fecha_nacimiento.ResetText();
            dtp_fecha_ingreso.ResetText();

        }

        private void Frm_Editar_Colaborador_Load(object sender, EventArgs e)
        {
            Cargar_Combo_Niveles_Ingles();
            Cargar_Combo_Departamento();
            Cargar_Combo_Puesto();
        }

        private void txt_buscar_Click(object sender, EventArgs e)
        {
            Obtener1();
            datos_colaborador = c.Buscar(obj_c);
            Colocar();

        }

        private void btn_añadir_Click(object sender, EventArgs e)
        {
            Obtener2();
            c.Modificar(obj_c);
            Limpiar();
        }
    }
}
