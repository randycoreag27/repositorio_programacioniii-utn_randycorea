﻿
namespace Capa_Presentacion
{
    partial class Frm_Reporte2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cbx_niveles = new System.Windows.Forms.ComboBox();
            this.txt_buscar = new System.Windows.Forms.Button();
            this.label10 = new System.Windows.Forms.Label();
            this.dgv_colaboradores = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_colaboradores)).BeginInit();
            this.SuspendLayout();
            // 
            // cbx_niveles
            // 
            this.cbx_niveles.Font = new System.Drawing.Font("Trebuchet MS", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbx_niveles.FormattingEnabled = true;
            this.cbx_niveles.Location = new System.Drawing.Point(248, 7);
            this.cbx_niveles.Name = "cbx_niveles";
            this.cbx_niveles.Size = new System.Drawing.Size(609, 28);
            this.cbx_niveles.TabIndex = 73;
            // 
            // txt_buscar
            // 
            this.txt_buscar.Font = new System.Drawing.Font("Trebuchet MS", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_buscar.Location = new System.Drawing.Point(16, 52);
            this.txt_buscar.Name = "txt_buscar";
            this.txt_buscar.Size = new System.Drawing.Size(841, 34);
            this.txt_buscar.TabIndex = 72;
            this.txt_buscar.Text = "Buscar Colaborador";
            this.txt_buscar.UseVisualStyleBackColor = true;
            this.txt_buscar.Click += new System.EventHandler(this.txt_buscar_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(12, 9);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(230, 22);
            this.label10.TabIndex = 71;
            this.label10.Text = "Busqueda por Nivel de Inglés:";
            // 
            // dgv_colaboradores
            // 
            this.dgv_colaboradores.AllowUserToAddRows = false;
            this.dgv_colaboradores.AllowUserToDeleteRows = false;
            this.dgv_colaboradores.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_colaboradores.Enabled = false;
            this.dgv_colaboradores.Location = new System.Drawing.Point(16, 107);
            this.dgv_colaboradores.Name = "dgv_colaboradores";
            this.dgv_colaboradores.ReadOnly = true;
            this.dgv_colaboradores.Size = new System.Drawing.Size(841, 113);
            this.dgv_colaboradores.TabIndex = 70;
            // 
            // Frm_Reporte2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(871, 233);
            this.Controls.Add(this.cbx_niveles);
            this.Controls.Add(this.txt_buscar);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.dgv_colaboradores);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Frm_Reporte2";
            this.Text = "Frm_Reporte2";
            this.Load += new System.EventHandler(this.Frm_Reporte2_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_colaboradores)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cbx_niveles;
        private System.Windows.Forms.Button txt_buscar;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.DataGridView dgv_colaboradores;
    }
}