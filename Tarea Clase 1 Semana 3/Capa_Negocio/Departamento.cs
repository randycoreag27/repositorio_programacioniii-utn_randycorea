﻿using Capa_Datos;
using Capa_Objetos;
using System.Collections.Generic;


namespace Capa_Negocio
{
    public class Departamento
    {
        public List<OBJ_Departamento> Obtener()
        {
            return new BD_Departamento().Obtener_Departamento();
        }
    }
}
