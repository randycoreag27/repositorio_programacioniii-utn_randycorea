﻿using Capa_Datos;
using Capa_Objetos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Capa_Negocio
{
    public class Nivel_Ingles
    {
        public List<OBJ_Nivel_Ingles> Obtener()
        {
            return new BD_Nivel_Ingles().Obtener_Nivel();
        }
    }
}
