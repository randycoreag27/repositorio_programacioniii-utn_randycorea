﻿using Capa_Datos;
using Capa_Objetos;
using System.Collections.Generic;


namespace Capa_Negocio
{
    public class Puesto
    {
        public List<OBJ_Puesto> Obtener()
        {
            return new BD_Puesto().Obtener_Puesto();
        }
    }
}
