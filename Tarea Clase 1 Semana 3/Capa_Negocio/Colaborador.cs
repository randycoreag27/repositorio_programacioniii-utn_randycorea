﻿using Capa_Datos;
using Capa_Objetos;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Capa_Negocio
{
    public class Colaborador
    {

        BD_Colaborador bd_colaborador = new BD_Colaborador();

        public void Añadir(OBJ_Colaborador obj_c)
        {
            bd_colaborador.Añadir_Colaboradores(obj_c);
        }

        public void Modificar(OBJ_Colaborador obj_c)
        {
            bd_colaborador.Modificar_Colaborador(obj_c);
        }

        public void Eliminar(OBJ_Colaborador obj_c)
        {
            bd_colaborador.Eliminar_Colaborador(obj_c);
        }

        public List<OBJ_Colaborador> Buscar(OBJ_Colaborador obj_c)
        {
            return new BD_Colaborador().Buscar_Colaborador(obj_c);
        }

        public void Mostrar(DataGridView dgv)
        {
            bd_colaborador.Mostrar_Colaborador(dgv);
        }

        public void Reporte_1(OBJ_Colaborador obj_c, DataGridView dgv)
        {
            bd_colaborador.Reporte1(obj_c, dgv);
        }

        public void Reporte_2(OBJ_Colaborador obj_c, DataGridView dgv)
        {
            bd_colaborador.Reporte2(obj_c, dgv);
        }

        public void Reporte_3(DataGridView dgv, DateTime fi, DateTime ff)
        {
            bd_colaborador.Reporte3(dgv, fi, ff);
        }

        public void Reporte_4(OBJ_Colaborador obj_c, DataGridView dgv, int ei, int ef)
        {
            bd_colaborador.Reporte4(obj_c ,dgv, ei, ef);
        }

    }
}
