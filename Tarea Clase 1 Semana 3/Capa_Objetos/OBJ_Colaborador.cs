﻿using System;


namespace Capa_Objetos
{
    public class OBJ_Colaborador
    {
        public int cedula { get; set; }
        public string nombre { get; set; }
        public string genero { get; set; }
        public int departamento { get; set; }
        public DateTime fecha_nacimiento { get; set; }
        public int edad { get; set; }
        public DateTime fecha_ingreso { get; set; }
        public string nivel_ingles { get; set; }
        public int puesto { get; set; }

    }
}
