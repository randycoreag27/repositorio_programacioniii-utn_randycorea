﻿using Capa_Objetos;
using Npgsql;
using System.Collections.Generic;


namespace Capa_Datos
{
    public class BD_Nivel_Ingles
    {

        static NpgsqlConnection conexion;
        static NpgsqlCommand cmd;

        public List<OBJ_Nivel_Ingles> Obtener_Nivel()
        {
            List<OBJ_Nivel_Ingles> lista_nivel = new List<OBJ_Nivel_Ingles>();

            conexion = Conexion_BD.connection();
            conexion.Open();
            cmd = new NpgsqlCommand("SELECT ID_Nivel, Nombre FROM Nivel_Ingles", conexion);
            NpgsqlDataReader rd = cmd.ExecuteReader();

            while (rd.Read())
            {
                OBJ_Nivel_Ingles nivel = new OBJ_Nivel_Ingles();

                nivel.id = rd.GetInt32(0);
                nivel.nombre = rd.GetString(1);
                lista_nivel.Add(nivel);
            }

            conexion.Close();

            return lista_nivel;
        }

    }
}
