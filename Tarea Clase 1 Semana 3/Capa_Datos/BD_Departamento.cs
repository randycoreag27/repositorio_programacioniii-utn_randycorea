﻿using Capa_Objetos;
using Npgsql;
using System.Collections.Generic;

namespace Capa_Datos
{
    public class BD_Departamento
    {

        static NpgsqlConnection conexion;
        static NpgsqlCommand cmd;

        public List<OBJ_Departamento> Obtener_Departamento()
        {
            List<OBJ_Departamento> lista_departamentos = new List<OBJ_Departamento>();

            conexion = Conexion_BD.connection();
            conexion.Open();
            cmd = new NpgsqlCommand("SELECT ID_Departamento, Nombre FROM Departamento", conexion);
            NpgsqlDataReader rd = cmd.ExecuteReader();

            while (rd.Read())
            {
                OBJ_Departamento departamento = new OBJ_Departamento();

                departamento.id = rd.GetInt32(0);
                departamento.nombre = rd.GetString(1);
                lista_departamentos.Add(departamento);
            }

            conexion.Close();

            return lista_departamentos;
        }

    }
}
