﻿using Capa_Objetos;
using Npgsql;
using System.Collections.Generic;


namespace Capa_Datos
{
    public class BD_Puesto
    {
        static NpgsqlConnection conexion;
        static NpgsqlCommand cmd;
        

        public List<OBJ_Puesto> Obtener_Puesto()
        {
            List<OBJ_Puesto> lista_puestos = new List<OBJ_Puesto>();

            conexion = Conexion_BD.connection();
            conexion.Open();
            cmd = new NpgsqlCommand("SELECT ID_Puesto, Nombre FROM Puesto", conexion);
            NpgsqlDataReader rd = cmd.ExecuteReader();

            while (rd.Read())
            {
                OBJ_Puesto puesto = new OBJ_Puesto();

                puesto.id = rd.GetInt32(0);
                puesto.nombre = rd.GetString(1);
                lista_puestos.Add(puesto);
            }

            conexion.Close();

            return lista_puestos;
        }
    }
}
