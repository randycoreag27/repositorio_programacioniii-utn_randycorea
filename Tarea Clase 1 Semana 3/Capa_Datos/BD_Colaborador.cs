﻿using Capa_Objetos;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Windows.Forms;

namespace Capa_Datos
{
    public class BD_Colaborador
    {
        static NpgsqlConnection conexion;
        static NpgsqlCommand cmd_;

        public void Añadir_Colaboradores(OBJ_Colaborador obj_c)
        {
            conexion = Conexion_BD.connection();
            conexion.Open();
            cmd_ = new NpgsqlCommand("INSERT INTO Colaborador(Cedula, Nombre, Genero, Departamento, Fecha_Nacimiento, Edad, Fecha_Ingreso, Nivel_Ingles, Puesto) VALUES " +
            "('" + obj_c.cedula + "', '" + obj_c.nombre + "', '" + obj_c.genero + "', '" + obj_c.departamento + "', '" + obj_c.fecha_nacimiento + "', '" + obj_c.edad + "', '" + obj_c.fecha_ingreso + "', '" + obj_c.nivel_ingles + "', '" + obj_c.puesto + "')", conexion);
            cmd_.ExecuteNonQuery();
            MessageBox.Show("Datos agregados correctamente", "Aviso");
            conexion.Close();
        }

        public List<OBJ_Colaborador> Buscar_Colaborador(OBJ_Colaborador obj_c)
        {

            List<OBJ_Colaborador> datos_colaborador = new List<OBJ_Colaborador>();

            conexion = Conexion_BD.connection();
            conexion.Open();
            cmd_ = new NpgsqlCommand("SELECT Cedula, Nombre, Genero, Departamento, Fecha_Nacimiento, Edad, Fecha_Ingreso, Nivel_Ingles, Puesto FROM Colaborador WHERE Cedula = '" + obj_c.cedula + "'", conexion);
            NpgsqlDataReader rd = cmd_.ExecuteReader();

            while (rd.Read())
            {
                OBJ_Colaborador colaborador = new OBJ_Colaborador();

                colaborador.cedula = rd.GetInt32(0);
                colaborador.nombre = rd.GetString(1);
                colaborador.genero = rd.GetString(2);
                colaborador.departamento = rd.GetInt32(3);
                colaborador.fecha_nacimiento = (System.DateTime)rd.GetValue(4);
                colaborador.edad = rd.GetInt32(5);
                colaborador.fecha_ingreso = (System.DateTime)rd.GetValue(6);
                colaborador.nivel_ingles = rd.GetString(7);
                colaborador.puesto = rd.GetInt32(8);
                datos_colaborador.Add(colaborador);

            }

            conexion.Close();

            return datos_colaborador;

        }

        public void Modificar_Colaborador (OBJ_Colaborador obj_c)
        {
            conexion = Conexion_BD.connection();
            conexion.Open();
            cmd_ = new NpgsqlCommand("UPDATE Colaborador SET Nombre = '" + obj_c.nombre + "', " + "Genero = '" + obj_c.genero + "', " +
                "Departamento = '" + obj_c.departamento + "', " + "Fecha_Nacimiento = '" + obj_c.fecha_nacimiento + "', " + "Edad = '" + obj_c.edad
                + "', " + "Fecha_Ingreso = '" + obj_c.fecha_ingreso + "', " + "Nivel_Ingles = '" + obj_c.nivel_ingles + "', " + "Puesto = '" + obj_c.puesto 
                + "' WHERE Cedula = '" + obj_c.cedula + "'", conexion);
            cmd_.ExecuteNonQuery();
            conexion.Close();
            MessageBox.Show("Datos Modificados Correctamente");

        }

        public void Eliminar_Colaborador(OBJ_Colaborador obj_c)
        {
            conexion = Conexion_BD.connection();
            conexion.Open();
            cmd_ = new NpgsqlCommand("DELETE FROM Colaborador WHERE Cedula = '" + obj_c.cedula + "'", conexion);
            cmd_.ExecuteNonQuery();
            conexion.Close();
            MessageBox.Show("Datos Eliminados Correctamente");

        }

        public void Mostrar_Colaborador(DataGridView dgv)
        {
            conexion = Conexion_BD.connection();
            DataTable data_table = new DataTable();
            NpgsqlDataAdapter adapter = new NpgsqlDataAdapter("SELECT Cedula, C.Nombre, Genero, D.Nombre Departamento, Fecha_Nacimiento, Edad, " +
                "Fecha_Ingreso, Nivel_Ingles, P.Nombre Puesto FROM Colaborador C INNER JOIN Departamento D ON C.Departamento = D.ID_Departamento " +
                "INNER JOIN Puesto P ON C.Puesto = P.ID_Puesto", conexion);
            adapter.Fill(data_table);
            dgv.DataSource = data_table;

        }

        public void Reporte1(OBJ_Colaborador obj_c, DataGridView dgv)
        {
            conexion = Conexion_BD.connection();
            DataTable data_table = new DataTable();
            NpgsqlDataAdapter adapter = new NpgsqlDataAdapter("SELECT C.Cedula CEDULA, C.Nombre NOMBRE, C.Edad EDAD, C.Fecha_Ingreso FECHA_INGRESO," +
                " D.Nombre DEPARTAMENTO, P.Abreviacion PUESTO" +
                " FROM Colaborador C INNER JOIN Departamento D ON C.Departamento = D.ID_Departamento " +
                "INNER JOIN Puesto P ON C.Puesto = P.ID_Puesto WHERE D.ID_Departamento = '" + obj_c.departamento + "'", conexion);
            adapter.Fill(data_table);
            dgv.DataSource = data_table;
        }

        public void Reporte2(OBJ_Colaborador obj_c, DataGridView dgv)
        {
            conexion = Conexion_BD.connection();
            DataTable data_table = new DataTable();
            NpgsqlDataAdapter adapter = new NpgsqlDataAdapter("SELECT C.Nombre, C.Genero, C.Nivel_Ingles, D.Nombre NOMBRE_DEPARTAMENTO, P.Nombre NOMBRE_PUESTO" +
                " FROM Colaborador C INNER JOIN Departamento D ON C.Departamento = D.ID_Departamento " +
                "INNER JOIN Puesto P ON C.Puesto = P.ID_Puesto WHERE C.Genero = 'Femenino' AND C.Nivel_Ingles = '" + obj_c.nivel_ingles + "'", conexion);
            adapter.Fill(data_table);
            dgv.DataSource = data_table;
        }

        public void Reporte3(DataGridView dgv, DateTime fi, DateTime ff)
        {
            conexion = Conexion_BD.connection();
            DataTable data_table = new DataTable();
            NpgsqlDataAdapter adapter = new NpgsqlDataAdapter("SELECT  C.Nombre, C.Fecha_Ingreso FROM Colaborador C WHERE C.Fecha_Ingreso BETWEEN" +
               "'" + fi + "' AND '" + ff + "'" , conexion);
            adapter.Fill(data_table);
            dgv.DataSource = data_table;
        }

        public void Reporte4(OBJ_Colaborador obj_c, DataGridView dgv, int ei, int ef)
        {
            conexion = Conexion_BD.connection();
            DataTable data_table = new DataTable();
            NpgsqlDataAdapter adapter = new NpgsqlDataAdapter("SELECT GENERO, COUNT(*)AS CANTIDAD FROM Colaborador C INNER JOIN Departamento D " +
                "ON C.Departamento = D.ID_Departamento WHERE Edad BETWEEN " +
               "'" + ei + "' AND '" + ef + "' AND D.ID_Departamento = '" + obj_c.departamento + " ' GROUP BY C.Genero", conexion);
            adapter.Fill(data_table);
            dgv.DataSource = data_table;
        }

    }
}
