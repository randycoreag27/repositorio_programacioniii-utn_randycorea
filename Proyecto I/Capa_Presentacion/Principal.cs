﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Capa_Presentacion
{
    public partial class Principal : Form
    {
        public Principal()
        {
            InitializeComponent();
        }

        private void timer_hora_fecha_Tick(object sender, EventArgs e)
        {
            this.lbl_hora.Text = DateTime.Now.ToString("hh:mm:ss");
            this.lbl_fecha.Text = DateTime.Now.ToLongDateString();
        }

        private void btn_salir_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btn_inicar_juego_Click(object sender, EventArgs e)
        {
            this.Hide();
            Quarto quarto = new Quarto();
            quarto.Show();
        }
    }
}
