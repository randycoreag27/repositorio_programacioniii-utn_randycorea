﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Capa_Presentacion
{
    public partial class Quarto : Form
    {

        PictureBox ficha_seleccionada = new PictureBox();
        Stopwatch cronometro = new Stopwatch();
        

        public Quarto()
        {
            InitializeComponent();
        }

        public void Validacion_Victoria()
        {
            //Fila 1
            if (cir_gran_hue_caf.Location.X == 61 && cir_gran_hue_caf.Location.Y == 128
                && cir_peq_hue_caf.Location.X == 142 && cir_peq_hue_caf.Location.Y == 128
                && cir_gran_re_caf.Location.X == 223 && cir_gran_re_caf.Location.Y == 128
                && cir_peq_re_caf.Location.X == 304 && cir_peq_re_caf.Location.Y == 128
                || cua_gran_hue_caf.Location.X == 61 && cua_gran_hue_caf.Location.Y == 128
                && cua_peq_hue_caf.Location.X == 142 && cua_peq_hue_caf.Location.Y == 128)
            {
                MessageBox.Show("AK7");
            }
            else
            {
                MessageBox.Show("COMBINACION INCORRECTA");
            }

        }

        public void Posiciones_Iniciales()
        {
            
        }

        public void Validacion_Nombres_Jugadores()
        {
            if(txt_jugador_1.Text == "" && txt_jugador_2.Text == "")
            {
                MessageBox1 mb1 = new MessageBox1();
                mb1.Show();
            }
            else if(txt_jugador_1.Text == txt_jugador_2.Text)
            {
                MessageBox2 mb2 = new MessageBox2();
                mb2.Show();
            }
            else
            {
                txt_jugador_1.Enabled = false;
                txt_jugador_2.Enabled = false;
                cronometro.Start();
            }
        }

        public void Seleccion(object sender)
        {
            try
            {
                ficha_seleccionada.BackColor = Color.DarkGoldenrod;
            }
            catch { }

            PictureBox ficha = (PictureBox)sender;
            ficha_seleccionada = ficha;
            ficha_seleccionada.BackColor = Color.Lime;
        }

        public void Movimiento(PictureBox c)
        {

            if (ficha_seleccionada != null)
            {
                string color = ficha_seleccionada.Name.ToString().Substring(0, 4);
                Point anterior = ficha_seleccionada.Location;
                ficha_seleccionada.Location = c.Location;
                int avance = anterior.Y - c.Location.Y;
                ficha_seleccionada.BackColor = Color.DarkGoldenrod;

            }
            if (ficha_seleccionada == null)
            {
                ficha_seleccionada.BackColor = Color.DarkGoldenrod;
            }

        }

        private void click_tablero(object sender, MouseEventArgs e)
        {
            Movimiento((PictureBox)sender);
        }

        private void seleccion_fichas_negras(object sender, MouseEventArgs e)
        {
            Seleccion(sender);
        }

        private void seleccion_fichas_cafes(object sender, MouseEventArgs e)
        {
            Seleccion(sender);
        }

        private void Quarto_Load(object sender, EventArgs e)
        {
            
        }

        private void btn_salir_Click_1(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void timer_cronometro_Tick(object sender, EventArgs e)
        {
            TimeSpan time_span = new TimeSpan(0, 0, 0, 0, (int)cronometro.ElapsedMilliseconds);
            this.lbl_cronometro.Text = time_span.Minutes.ToString() + ":" + time_span.Seconds.ToString() + ":" + time_span.Milliseconds.ToString();
        }

        private void btn_iniciar_juego_Click(object sender, EventArgs e)
        {
            Validacion_Nombres_Jugadores();
        }

        private void btn_anunciar_Click(object sender, EventArgs e)
        {
            Validacion_Victoria();
            
            Posiciones_Iniciales();
        }
    }
}
