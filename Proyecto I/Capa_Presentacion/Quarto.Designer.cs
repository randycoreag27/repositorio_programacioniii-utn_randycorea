﻿
namespace Capa_Presentacion
{
    partial class Quarto
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Quarto));
            this.pictureBox11 = new System.Windows.Forms.PictureBox();
            this.pictureBox12 = new System.Windows.Forms.PictureBox();
            this.pictureBox13 = new System.Windows.Forms.PictureBox();
            this.pictureBox14 = new System.Windows.Forms.PictureBox();
            this.pictureBox15 = new System.Windows.Forms.PictureBox();
            this.pictureBox16 = new System.Windows.Forms.PictureBox();
            this.pictureBox17 = new System.Windows.Forms.PictureBox();
            this.pictureBox18 = new System.Windows.Forms.PictureBox();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            this.campo_cuatro = new System.Windows.Forms.PictureBox();
            this.campo_tres = new System.Windows.Forms.PictureBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.campo_dos = new System.Windows.Forms.PictureBox();
            this.campo_uno = new System.Windows.Forms.PictureBox();
            this.pictureBox29 = new System.Windows.Forms.PictureBox();
            this.pictureBox30 = new System.Windows.Forms.PictureBox();
            this.pictureBox31 = new System.Windows.Forms.PictureBox();
            this.pictureBox32 = new System.Windows.Forms.PictureBox();
            this.pictureBox25 = new System.Windows.Forms.PictureBox();
            this.pictureBox26 = new System.Windows.Forms.PictureBox();
            this.pictureBox27 = new System.Windows.Forms.PictureBox();
            this.pictureBox28 = new System.Windows.Forms.PictureBox();
            this.cua_peq_rel_caf = new System.Windows.Forms.PictureBox();
            this.cua_gran_rel_caf = new System.Windows.Forms.PictureBox();
            this.cua_peq_hue_caf = new System.Windows.Forms.PictureBox();
            this.cua_gran_hue_caf = new System.Windows.Forms.PictureBox();
            this.cir_peq_hue_caf = new System.Windows.Forms.PictureBox();
            this.cir_peq_re_caf = new System.Windows.Forms.PictureBox();
            this.cir_gran_hue_caf = new System.Windows.Forms.PictureBox();
            this.cir_gran_re_caf = new System.Windows.Forms.PictureBox();
            this.btn_salir = new System.Windows.Forms.PictureBox();
            this.lbl_cronometro = new System.Windows.Forms.Label();
            this.timer_cronometro = new System.Windows.Forms.Timer(this.components);
            this.txt_jugador_1 = new System.Windows.Forms.TextBox();
            this.txt_jugador_2 = new System.Windows.Forms.TextBox();
            this.btn_iniciar_juego = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.list_view_1 = new System.Windows.Forms.ListView();
            this.list_view_2 = new System.Windows.Forms.ListView();
            this.btn_anunciar = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.campo_cuatro)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.campo_tres)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.campo_dos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.campo_uno)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cua_peq_rel_caf)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cua_gran_rel_caf)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cua_peq_hue_caf)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cua_gran_hue_caf)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cir_peq_hue_caf)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cir_peq_re_caf)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cir_gran_hue_caf)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cir_gran_re_caf)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_salir)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox11
            // 
            this.pictureBox11.BackColor = System.Drawing.Color.DarkGoldenrod;
            this.pictureBox11.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBox11.Location = new System.Drawing.Point(304, 371);
            this.pictureBox11.Name = "pictureBox11";
            this.pictureBox11.Size = new System.Drawing.Size(80, 80);
            this.pictureBox11.TabIndex = 34;
            this.pictureBox11.TabStop = false;
            this.pictureBox11.MouseClick += new System.Windows.Forms.MouseEventHandler(this.click_tablero);
            // 
            // pictureBox12
            // 
            this.pictureBox12.BackColor = System.Drawing.Color.DarkGoldenrod;
            this.pictureBox12.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBox12.Location = new System.Drawing.Point(223, 371);
            this.pictureBox12.Name = "pictureBox12";
            this.pictureBox12.Size = new System.Drawing.Size(80, 80);
            this.pictureBox12.TabIndex = 33;
            this.pictureBox12.TabStop = false;
            this.pictureBox12.MouseClick += new System.Windows.Forms.MouseEventHandler(this.click_tablero);
            // 
            // pictureBox13
            // 
            this.pictureBox13.BackColor = System.Drawing.Color.DarkGoldenrod;
            this.pictureBox13.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBox13.Location = new System.Drawing.Point(304, 290);
            this.pictureBox13.Name = "pictureBox13";
            this.pictureBox13.Size = new System.Drawing.Size(80, 80);
            this.pictureBox13.TabIndex = 32;
            this.pictureBox13.TabStop = false;
            this.pictureBox13.MouseClick += new System.Windows.Forms.MouseEventHandler(this.click_tablero);
            // 
            // pictureBox14
            // 
            this.pictureBox14.BackColor = System.Drawing.Color.DarkGoldenrod;
            this.pictureBox14.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBox14.Location = new System.Drawing.Point(223, 290);
            this.pictureBox14.Name = "pictureBox14";
            this.pictureBox14.Size = new System.Drawing.Size(80, 80);
            this.pictureBox14.TabIndex = 31;
            this.pictureBox14.TabStop = false;
            this.pictureBox14.MouseClick += new System.Windows.Forms.MouseEventHandler(this.click_tablero);
            // 
            // pictureBox15
            // 
            this.pictureBox15.BackColor = System.Drawing.Color.DarkGoldenrod;
            this.pictureBox15.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBox15.Location = new System.Drawing.Point(142, 371);
            this.pictureBox15.Name = "pictureBox15";
            this.pictureBox15.Size = new System.Drawing.Size(80, 80);
            this.pictureBox15.TabIndex = 30;
            this.pictureBox15.TabStop = false;
            this.pictureBox15.MouseClick += new System.Windows.Forms.MouseEventHandler(this.click_tablero);
            // 
            // pictureBox16
            // 
            this.pictureBox16.BackColor = System.Drawing.Color.DarkGoldenrod;
            this.pictureBox16.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBox16.Location = new System.Drawing.Point(61, 371);
            this.pictureBox16.Name = "pictureBox16";
            this.pictureBox16.Size = new System.Drawing.Size(80, 80);
            this.pictureBox16.TabIndex = 29;
            this.pictureBox16.TabStop = false;
            this.pictureBox16.MouseClick += new System.Windows.Forms.MouseEventHandler(this.click_tablero);
            // 
            // pictureBox17
            // 
            this.pictureBox17.BackColor = System.Drawing.Color.DarkGoldenrod;
            this.pictureBox17.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBox17.Location = new System.Drawing.Point(142, 290);
            this.pictureBox17.Name = "pictureBox17";
            this.pictureBox17.Size = new System.Drawing.Size(80, 80);
            this.pictureBox17.TabIndex = 28;
            this.pictureBox17.TabStop = false;
            this.pictureBox17.MouseClick += new System.Windows.Forms.MouseEventHandler(this.click_tablero);
            // 
            // pictureBox18
            // 
            this.pictureBox18.BackColor = System.Drawing.Color.DarkGoldenrod;
            this.pictureBox18.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBox18.Location = new System.Drawing.Point(61, 290);
            this.pictureBox18.Name = "pictureBox18";
            this.pictureBox18.Size = new System.Drawing.Size(80, 80);
            this.pictureBox18.TabIndex = 27;
            this.pictureBox18.TabStop = false;
            this.pictureBox18.MouseClick += new System.Windows.Forms.MouseEventHandler(this.click_tablero);
            // 
            // pictureBox7
            // 
            this.pictureBox7.BackColor = System.Drawing.Color.DarkGoldenrod;
            this.pictureBox7.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBox7.Location = new System.Drawing.Point(304, 209);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(80, 80);
            this.pictureBox7.TabIndex = 26;
            this.pictureBox7.TabStop = false;
            this.pictureBox7.MouseClick += new System.Windows.Forms.MouseEventHandler(this.click_tablero);
            // 
            // pictureBox8
            // 
            this.pictureBox8.BackColor = System.Drawing.Color.DarkGoldenrod;
            this.pictureBox8.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBox8.Location = new System.Drawing.Point(223, 209);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(80, 80);
            this.pictureBox8.TabIndex = 25;
            this.pictureBox8.TabStop = false;
            this.pictureBox8.MouseClick += new System.Windows.Forms.MouseEventHandler(this.click_tablero);
            // 
            // campo_cuatro
            // 
            this.campo_cuatro.BackColor = System.Drawing.Color.DarkGoldenrod;
            this.campo_cuatro.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.campo_cuatro.Location = new System.Drawing.Point(304, 128);
            this.campo_cuatro.Name = "campo_cuatro";
            this.campo_cuatro.Size = new System.Drawing.Size(80, 80);
            this.campo_cuatro.TabIndex = 24;
            this.campo_cuatro.TabStop = false;
            this.campo_cuatro.MouseClick += new System.Windows.Forms.MouseEventHandler(this.click_tablero);
            // 
            // campo_tres
            // 
            this.campo_tres.BackColor = System.Drawing.Color.DarkGoldenrod;
            this.campo_tres.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.campo_tres.Location = new System.Drawing.Point(223, 128);
            this.campo_tres.Name = "campo_tres";
            this.campo_tres.Size = new System.Drawing.Size(80, 80);
            this.campo_tres.TabIndex = 23;
            this.campo_tres.TabStop = false;
            this.campo_tres.MouseClick += new System.Windows.Forms.MouseEventHandler(this.click_tablero);
            // 
            // pictureBox5
            // 
            this.pictureBox5.BackColor = System.Drawing.Color.DarkGoldenrod;
            this.pictureBox5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBox5.Location = new System.Drawing.Point(142, 209);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(80, 80);
            this.pictureBox5.TabIndex = 22;
            this.pictureBox5.TabStop = false;
            this.pictureBox5.MouseClick += new System.Windows.Forms.MouseEventHandler(this.click_tablero);
            // 
            // pictureBox6
            // 
            this.pictureBox6.BackColor = System.Drawing.Color.DarkGoldenrod;
            this.pictureBox6.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBox6.Location = new System.Drawing.Point(61, 209);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(80, 80);
            this.pictureBox6.TabIndex = 21;
            this.pictureBox6.TabStop = false;
            this.pictureBox6.MouseClick += new System.Windows.Forms.MouseEventHandler(this.click_tablero);
            // 
            // campo_dos
            // 
            this.campo_dos.BackColor = System.Drawing.Color.DarkGoldenrod;
            this.campo_dos.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.campo_dos.Location = new System.Drawing.Point(142, 128);
            this.campo_dos.Name = "campo_dos";
            this.campo_dos.Size = new System.Drawing.Size(80, 80);
            this.campo_dos.TabIndex = 20;
            this.campo_dos.TabStop = false;
            this.campo_dos.MouseClick += new System.Windows.Forms.MouseEventHandler(this.click_tablero);
            // 
            // campo_uno
            // 
            this.campo_uno.BackColor = System.Drawing.Color.DarkGoldenrod;
            this.campo_uno.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.campo_uno.Location = new System.Drawing.Point(61, 128);
            this.campo_uno.Name = "campo_uno";
            this.campo_uno.Size = new System.Drawing.Size(80, 80);
            this.campo_uno.TabIndex = 19;
            this.campo_uno.TabStop = false;
            this.campo_uno.MouseClick += new System.Windows.Forms.MouseEventHandler(this.click_tablero);
            // 
            // pictureBox29
            // 
            this.pictureBox29.BackColor = System.Drawing.Color.DarkGoldenrod;
            this.pictureBox29.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBox29.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox29.Image")));
            this.pictureBox29.Location = new System.Drawing.Point(758, 369);
            this.pictureBox29.Name = "pictureBox29";
            this.pictureBox29.Size = new System.Drawing.Size(80, 80);
            this.pictureBox29.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox29.TabIndex = 50;
            this.pictureBox29.TabStop = false;
            this.pictureBox29.MouseClick += new System.Windows.Forms.MouseEventHandler(this.seleccion_fichas_negras);
            // 
            // pictureBox30
            // 
            this.pictureBox30.BackColor = System.Drawing.Color.DarkGoldenrod;
            this.pictureBox30.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBox30.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox30.Image")));
            this.pictureBox30.Location = new System.Drawing.Point(677, 369);
            this.pictureBox30.Name = "pictureBox30";
            this.pictureBox30.Size = new System.Drawing.Size(80, 80);
            this.pictureBox30.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox30.TabIndex = 49;
            this.pictureBox30.TabStop = false;
            this.pictureBox30.MouseClick += new System.Windows.Forms.MouseEventHandler(this.seleccion_fichas_negras);
            // 
            // pictureBox31
            // 
            this.pictureBox31.BackColor = System.Drawing.Color.DarkGoldenrod;
            this.pictureBox31.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBox31.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox31.Image")));
            this.pictureBox31.Location = new System.Drawing.Point(595, 369);
            this.pictureBox31.Name = "pictureBox31";
            this.pictureBox31.Size = new System.Drawing.Size(80, 80);
            this.pictureBox31.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox31.TabIndex = 48;
            this.pictureBox31.TabStop = false;
            this.pictureBox31.MouseClick += new System.Windows.Forms.MouseEventHandler(this.seleccion_fichas_negras);
            // 
            // pictureBox32
            // 
            this.pictureBox32.BackColor = System.Drawing.Color.DarkGoldenrod;
            this.pictureBox32.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBox32.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox32.Image")));
            this.pictureBox32.Location = new System.Drawing.Point(515, 369);
            this.pictureBox32.Name = "pictureBox32";
            this.pictureBox32.Size = new System.Drawing.Size(80, 80);
            this.pictureBox32.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox32.TabIndex = 47;
            this.pictureBox32.TabStop = false;
            this.pictureBox32.MouseClick += new System.Windows.Forms.MouseEventHandler(this.seleccion_fichas_negras);
            // 
            // pictureBox25
            // 
            this.pictureBox25.BackColor = System.Drawing.Color.DarkGoldenrod;
            this.pictureBox25.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBox25.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox25.Image")));
            this.pictureBox25.Location = new System.Drawing.Point(758, 288);
            this.pictureBox25.Name = "pictureBox25";
            this.pictureBox25.Size = new System.Drawing.Size(80, 80);
            this.pictureBox25.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox25.TabIndex = 46;
            this.pictureBox25.TabStop = false;
            this.pictureBox25.MouseClick += new System.Windows.Forms.MouseEventHandler(this.seleccion_fichas_negras);
            // 
            // pictureBox26
            // 
            this.pictureBox26.BackColor = System.Drawing.Color.DarkGoldenrod;
            this.pictureBox26.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBox26.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox26.Image")));
            this.pictureBox26.Location = new System.Drawing.Point(677, 288);
            this.pictureBox26.Name = "pictureBox26";
            this.pictureBox26.Size = new System.Drawing.Size(80, 80);
            this.pictureBox26.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox26.TabIndex = 45;
            this.pictureBox26.TabStop = false;
            this.pictureBox26.MouseClick += new System.Windows.Forms.MouseEventHandler(this.seleccion_fichas_negras);
            // 
            // pictureBox27
            // 
            this.pictureBox27.BackColor = System.Drawing.Color.DarkGoldenrod;
            this.pictureBox27.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBox27.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox27.Image")));
            this.pictureBox27.Location = new System.Drawing.Point(596, 288);
            this.pictureBox27.Name = "pictureBox27";
            this.pictureBox27.Size = new System.Drawing.Size(80, 80);
            this.pictureBox27.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox27.TabIndex = 44;
            this.pictureBox27.TabStop = false;
            this.pictureBox27.MouseClick += new System.Windows.Forms.MouseEventHandler(this.seleccion_fichas_negras);
            // 
            // pictureBox28
            // 
            this.pictureBox28.BackColor = System.Drawing.Color.DarkGoldenrod;
            this.pictureBox28.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBox28.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox28.Image")));
            this.pictureBox28.Location = new System.Drawing.Point(515, 288);
            this.pictureBox28.Name = "pictureBox28";
            this.pictureBox28.Size = new System.Drawing.Size(80, 80);
            this.pictureBox28.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox28.TabIndex = 43;
            this.pictureBox28.TabStop = false;
            this.pictureBox28.MouseClick += new System.Windows.Forms.MouseEventHandler(this.seleccion_fichas_negras);
            // 
            // cua_peq_rel_caf
            // 
            this.cua_peq_rel_caf.BackColor = System.Drawing.Color.DarkGoldenrod;
            this.cua_peq_rel_caf.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.cua_peq_rel_caf.Image = ((System.Drawing.Image)(resources.GetObject("cua_peq_rel_caf.Image")));
            this.cua_peq_rel_caf.Location = new System.Drawing.Point(758, 208);
            this.cua_peq_rel_caf.Name = "cua_peq_rel_caf";
            this.cua_peq_rel_caf.Size = new System.Drawing.Size(80, 80);
            this.cua_peq_rel_caf.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.cua_peq_rel_caf.TabIndex = 42;
            this.cua_peq_rel_caf.TabStop = false;
            this.cua_peq_rel_caf.MouseClick += new System.Windows.Forms.MouseEventHandler(this.seleccion_fichas_cafes);
            // 
            // cua_gran_rel_caf
            // 
            this.cua_gran_rel_caf.BackColor = System.Drawing.Color.DarkGoldenrod;
            this.cua_gran_rel_caf.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.cua_gran_rel_caf.Image = ((System.Drawing.Image)(resources.GetObject("cua_gran_rel_caf.Image")));
            this.cua_gran_rel_caf.Location = new System.Drawing.Point(677, 208);
            this.cua_gran_rel_caf.Name = "cua_gran_rel_caf";
            this.cua_gran_rel_caf.Size = new System.Drawing.Size(80, 80);
            this.cua_gran_rel_caf.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.cua_gran_rel_caf.TabIndex = 41;
            this.cua_gran_rel_caf.TabStop = false;
            this.cua_gran_rel_caf.MouseClick += new System.Windows.Forms.MouseEventHandler(this.seleccion_fichas_cafes);
            // 
            // cua_peq_hue_caf
            // 
            this.cua_peq_hue_caf.BackColor = System.Drawing.Color.DarkGoldenrod;
            this.cua_peq_hue_caf.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.cua_peq_hue_caf.Image = ((System.Drawing.Image)(resources.GetObject("cua_peq_hue_caf.Image")));
            this.cua_peq_hue_caf.Location = new System.Drawing.Point(596, 208);
            this.cua_peq_hue_caf.Name = "cua_peq_hue_caf";
            this.cua_peq_hue_caf.Size = new System.Drawing.Size(80, 80);
            this.cua_peq_hue_caf.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.cua_peq_hue_caf.TabIndex = 40;
            this.cua_peq_hue_caf.TabStop = false;
            this.cua_peq_hue_caf.MouseClick += new System.Windows.Forms.MouseEventHandler(this.seleccion_fichas_cafes);
            // 
            // cua_gran_hue_caf
            // 
            this.cua_gran_hue_caf.BackColor = System.Drawing.Color.DarkGoldenrod;
            this.cua_gran_hue_caf.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.cua_gran_hue_caf.Image = ((System.Drawing.Image)(resources.GetObject("cua_gran_hue_caf.Image")));
            this.cua_gran_hue_caf.Location = new System.Drawing.Point(515, 208);
            this.cua_gran_hue_caf.Name = "cua_gran_hue_caf";
            this.cua_gran_hue_caf.Size = new System.Drawing.Size(80, 80);
            this.cua_gran_hue_caf.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.cua_gran_hue_caf.TabIndex = 39;
            this.cua_gran_hue_caf.TabStop = false;
            this.cua_gran_hue_caf.MouseClick += new System.Windows.Forms.MouseEventHandler(this.seleccion_fichas_cafes);
            // 
            // cir_peq_hue_caf
            // 
            this.cir_peq_hue_caf.BackColor = System.Drawing.Color.DarkGoldenrod;
            this.cir_peq_hue_caf.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.cir_peq_hue_caf.Image = ((System.Drawing.Image)(resources.GetObject("cir_peq_hue_caf.Image")));
            this.cir_peq_hue_caf.Location = new System.Drawing.Point(596, 128);
            this.cir_peq_hue_caf.Name = "cir_peq_hue_caf";
            this.cir_peq_hue_caf.Size = new System.Drawing.Size(80, 80);
            this.cir_peq_hue_caf.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.cir_peq_hue_caf.TabIndex = 38;
            this.cir_peq_hue_caf.TabStop = false;
            this.cir_peq_hue_caf.MouseClick += new System.Windows.Forms.MouseEventHandler(this.seleccion_fichas_cafes);
            // 
            // cir_peq_re_caf
            // 
            this.cir_peq_re_caf.BackColor = System.Drawing.Color.DarkGoldenrod;
            this.cir_peq_re_caf.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.cir_peq_re_caf.Image = ((System.Drawing.Image)(resources.GetObject("cir_peq_re_caf.Image")));
            this.cir_peq_re_caf.Location = new System.Drawing.Point(758, 128);
            this.cir_peq_re_caf.Name = "cir_peq_re_caf";
            this.cir_peq_re_caf.Size = new System.Drawing.Size(80, 80);
            this.cir_peq_re_caf.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.cir_peq_re_caf.TabIndex = 37;
            this.cir_peq_re_caf.TabStop = false;
            this.cir_peq_re_caf.MouseClick += new System.Windows.Forms.MouseEventHandler(this.seleccion_fichas_cafes);
            // 
            // cir_gran_hue_caf
            // 
            this.cir_gran_hue_caf.BackColor = System.Drawing.Color.DarkGoldenrod;
            this.cir_gran_hue_caf.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.cir_gran_hue_caf.Image = ((System.Drawing.Image)(resources.GetObject("cir_gran_hue_caf.Image")));
            this.cir_gran_hue_caf.Location = new System.Drawing.Point(515, 128);
            this.cir_gran_hue_caf.Name = "cir_gran_hue_caf";
            this.cir_gran_hue_caf.Size = new System.Drawing.Size(80, 80);
            this.cir_gran_hue_caf.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.cir_gran_hue_caf.TabIndex = 36;
            this.cir_gran_hue_caf.TabStop = false;
            this.cir_gran_hue_caf.MouseClick += new System.Windows.Forms.MouseEventHandler(this.seleccion_fichas_cafes);
            // 
            // cir_gran_re_caf
            // 
            this.cir_gran_re_caf.BackColor = System.Drawing.Color.DarkGoldenrod;
            this.cir_gran_re_caf.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.cir_gran_re_caf.Image = ((System.Drawing.Image)(resources.GetObject("cir_gran_re_caf.Image")));
            this.cir_gran_re_caf.Location = new System.Drawing.Point(677, 128);
            this.cir_gran_re_caf.Name = "cir_gran_re_caf";
            this.cir_gran_re_caf.Size = new System.Drawing.Size(80, 80);
            this.cir_gran_re_caf.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.cir_gran_re_caf.TabIndex = 35;
            this.cir_gran_re_caf.TabStop = false;
            this.cir_gran_re_caf.MouseClick += new System.Windows.Forms.MouseEventHandler(this.seleccion_fichas_cafes);
            // 
            // btn_salir
            // 
            this.btn_salir.Image = ((System.Drawing.Image)(resources.GetObject("btn_salir.Image")));
            this.btn_salir.Location = new System.Drawing.Point(876, 12);
            this.btn_salir.Name = "btn_salir";
            this.btn_salir.Size = new System.Drawing.Size(27, 34);
            this.btn_salir.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btn_salir.TabIndex = 51;
            this.btn_salir.TabStop = false;
            this.btn_salir.Click += new System.EventHandler(this.btn_salir_Click_1);
            // 
            // lbl_cronometro
            // 
            this.lbl_cronometro.AutoSize = true;
            this.lbl_cronometro.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_cronometro.ForeColor = System.Drawing.Color.Black;
            this.lbl_cronometro.Location = new System.Drawing.Point(389, 30);
            this.lbl_cronometro.Name = "lbl_cronometro";
            this.lbl_cronometro.Size = new System.Drawing.Size(128, 31);
            this.lbl_cronometro.TabIndex = 52;
            this.lbl_cronometro.Text = "00:00:00";
            // 
            // timer_cronometro
            // 
            this.timer_cronometro.Enabled = true;
            this.timer_cronometro.Tick += new System.EventHandler(this.timer_cronometro_Tick);
            // 
            // txt_jugador_1
            // 
            this.txt_jugador_1.BackColor = System.Drawing.Color.DarkGoldenrod;
            this.txt_jugador_1.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_jugador_1.Location = new System.Drawing.Point(141, 47);
            this.txt_jugador_1.Name = "txt_jugador_1";
            this.txt_jugador_1.Size = new System.Drawing.Size(161, 38);
            this.txt_jugador_1.TabIndex = 53;
            // 
            // txt_jugador_2
            // 
            this.txt_jugador_2.BackColor = System.Drawing.Color.DarkGoldenrod;
            this.txt_jugador_2.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_jugador_2.Location = new System.Drawing.Point(596, 47);
            this.txt_jugador_2.Name = "txt_jugador_2";
            this.txt_jugador_2.Size = new System.Drawing.Size(161, 38);
            this.txt_jugador_2.TabIndex = 54;
            // 
            // btn_iniciar_juego
            // 
            this.btn_iniciar_juego.BackColor = System.Drawing.Color.DarkGoldenrod;
            this.btn_iniciar_juego.FlatAppearance.BorderColor = System.Drawing.Color.DarkGoldenrod;
            this.btn_iniciar_juego.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_iniciar_juego.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_iniciar_juego.ForeColor = System.Drawing.Color.Black;
            this.btn_iniciar_juego.Location = new System.Drawing.Point(377, 64);
            this.btn_iniciar_juego.Name = "btn_iniciar_juego";
            this.btn_iniciar_juego.Size = new System.Drawing.Size(150, 38);
            this.btn_iniciar_juego.TabIndex = 55;
            this.btn_iniciar_juego.Text = "Iniciar Juego";
            this.btn_iniciar_juego.UseVisualStyleBackColor = false;
            this.btn_iniciar_juego.Click += new System.EventHandler(this.btn_iniciar_juego_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(605, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(143, 31);
            this.label1.TabIndex = 56;
            this.label1.Text = "Jugador 2";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(151, 13);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(143, 31);
            this.label2.TabIndex = 57;
            this.label2.Text = "Jugador 1";
            // 
            // list_view_1
            // 
            this.list_view_1.BackColor = System.Drawing.Color.DarkGoldenrod;
            this.list_view_1.HideSelection = false;
            this.list_view_1.Location = new System.Drawing.Point(61, 472);
            this.list_view_1.Name = "list_view_1";
            this.list_view_1.Size = new System.Drawing.Size(323, 70);
            this.list_view_1.TabIndex = 58;
            this.list_view_1.UseCompatibleStateImageBehavior = false;
            // 
            // list_view_2
            // 
            this.list_view_2.BackColor = System.Drawing.Color.DarkGoldenrod;
            this.list_view_2.HideSelection = false;
            this.list_view_2.Location = new System.Drawing.Point(515, 472);
            this.list_view_2.Name = "list_view_2";
            this.list_view_2.Size = new System.Drawing.Size(323, 70);
            this.list_view_2.TabIndex = 59;
            this.list_view_2.UseCompatibleStateImageBehavior = false;
            // 
            // btn_anunciar
            // 
            this.btn_anunciar.BackColor = System.Drawing.Color.DarkGoldenrod;
            this.btn_anunciar.FlatAppearance.BorderColor = System.Drawing.Color.DarkGoldenrod;
            this.btn_anunciar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_anunciar.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_anunciar.ForeColor = System.Drawing.Color.Black;
            this.btn_anunciar.Location = new System.Drawing.Point(395, 313);
            this.btn_anunciar.Name = "btn_anunciar";
            this.btn_anunciar.Size = new System.Drawing.Size(106, 38);
            this.btn_anunciar.TabIndex = 60;
            this.btn_anunciar.Text = "Anunciar";
            this.btn_anunciar.UseVisualStyleBackColor = false;
            this.btn_anunciar.Click += new System.EventHandler(this.btn_anunciar_Click);
            // 
            // Quarto
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.NavajoWhite;
            this.ClientSize = new System.Drawing.Size(915, 606);
            this.Controls.Add(this.btn_anunciar);
            this.Controls.Add(this.list_view_2);
            this.Controls.Add(this.list_view_1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btn_iniciar_juego);
            this.Controls.Add(this.txt_jugador_2);
            this.Controls.Add(this.txt_jugador_1);
            this.Controls.Add(this.lbl_cronometro);
            this.Controls.Add(this.btn_salir);
            this.Controls.Add(this.pictureBox29);
            this.Controls.Add(this.pictureBox30);
            this.Controls.Add(this.pictureBox31);
            this.Controls.Add(this.pictureBox32);
            this.Controls.Add(this.pictureBox25);
            this.Controls.Add(this.pictureBox26);
            this.Controls.Add(this.pictureBox27);
            this.Controls.Add(this.pictureBox28);
            this.Controls.Add(this.cua_peq_rel_caf);
            this.Controls.Add(this.cua_gran_rel_caf);
            this.Controls.Add(this.cua_peq_hue_caf);
            this.Controls.Add(this.cua_gran_hue_caf);
            this.Controls.Add(this.cir_peq_hue_caf);
            this.Controls.Add(this.cir_peq_re_caf);
            this.Controls.Add(this.cir_gran_hue_caf);
            this.Controls.Add(this.cir_gran_re_caf);
            this.Controls.Add(this.pictureBox11);
            this.Controls.Add(this.pictureBox12);
            this.Controls.Add(this.pictureBox13);
            this.Controls.Add(this.pictureBox14);
            this.Controls.Add(this.pictureBox15);
            this.Controls.Add(this.pictureBox16);
            this.Controls.Add(this.pictureBox17);
            this.Controls.Add(this.pictureBox18);
            this.Controls.Add(this.pictureBox7);
            this.Controls.Add(this.pictureBox8);
            this.Controls.Add(this.campo_cuatro);
            this.Controls.Add(this.campo_tres);
            this.Controls.Add(this.pictureBox5);
            this.Controls.Add(this.pictureBox6);
            this.Controls.Add(this.campo_dos);
            this.Controls.Add(this.campo_uno);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Quarto";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Quarto";
            this.Load += new System.EventHandler(this.Quarto_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.campo_cuatro)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.campo_tres)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.campo_dos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.campo_uno)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cua_peq_rel_caf)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cua_gran_rel_caf)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cua_peq_hue_caf)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cua_gran_hue_caf)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cir_peq_hue_caf)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cir_peq_re_caf)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cir_gran_hue_caf)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cir_gran_re_caf)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_salir)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox11;
        private System.Windows.Forms.PictureBox pictureBox12;
        private System.Windows.Forms.PictureBox pictureBox13;
        private System.Windows.Forms.PictureBox pictureBox14;
        private System.Windows.Forms.PictureBox pictureBox15;
        private System.Windows.Forms.PictureBox pictureBox16;
        private System.Windows.Forms.PictureBox pictureBox17;
        private System.Windows.Forms.PictureBox pictureBox18;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.PictureBox pictureBox8;
        private System.Windows.Forms.PictureBox campo_cuatro;
        private System.Windows.Forms.PictureBox campo_tres;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.PictureBox campo_dos;
        private System.Windows.Forms.PictureBox campo_uno;
        private System.Windows.Forms.PictureBox pictureBox29;
        private System.Windows.Forms.PictureBox pictureBox30;
        private System.Windows.Forms.PictureBox pictureBox31;
        private System.Windows.Forms.PictureBox pictureBox32;
        private System.Windows.Forms.PictureBox pictureBox25;
        private System.Windows.Forms.PictureBox pictureBox26;
        private System.Windows.Forms.PictureBox pictureBox27;
        private System.Windows.Forms.PictureBox pictureBox28;
        private System.Windows.Forms.PictureBox cua_peq_rel_caf;
        private System.Windows.Forms.PictureBox cua_gran_rel_caf;
        private System.Windows.Forms.PictureBox cua_peq_hue_caf;
        private System.Windows.Forms.PictureBox cua_gran_hue_caf;
        private System.Windows.Forms.PictureBox cir_peq_hue_caf;
        private System.Windows.Forms.PictureBox cir_peq_re_caf;
        private System.Windows.Forms.PictureBox cir_gran_hue_caf;
        private System.Windows.Forms.PictureBox cir_gran_re_caf;
        private System.Windows.Forms.PictureBox btn_salir;
        private System.Windows.Forms.Label lbl_cronometro;
        private System.Windows.Forms.Timer timer_cronometro;
        private System.Windows.Forms.TextBox txt_jugador_1;
        private System.Windows.Forms.TextBox txt_jugador_2;
        private System.Windows.Forms.Button btn_iniciar_juego;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ListView list_view_1;
        private System.Windows.Forms.ListView list_view_2;
        private System.Windows.Forms.Button btn_anunciar;
    }
}