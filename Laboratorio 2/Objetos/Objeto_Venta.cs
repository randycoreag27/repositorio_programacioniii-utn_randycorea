﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Objetos
{
    public class Objeto_Venta
    {
        public string localidades { get; set; }
        public int precio_total { get; set; }
        public int cantidad { get; set; }
        public int cedula { get; set; }
        public string nombre { get; set; }
        public string genero { get; set; }
    }
}
