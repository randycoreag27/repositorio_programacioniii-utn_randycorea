﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Objetos
{
    public class Objeto_Usuario
    {
        public int cedula { get; set; }
        public string nombre { get; set; }
        public string genero { get; set; }
        public string usuario { get; set; }
        public string contraseña { get; set; }
        public string rol { get; set; }
    }
}
