﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Objetos
{
    public class Objeto_Entrada
    {
        public string localidad { get; set; }
        public int precio { get; set; }
        public string patrocinador { get; set; }
        public int cantidad { get; set; }
    }
}
