﻿using Capa_Datos;
using Objetos;
using System;
using System.Collections.Generic;
using System.Xml;

namespace Capa_Negocio
{
    public class Archivo_XML
    {
        XmlDocument doc;
        string rutaXml;

        public void CrearXML(string ruta, string nodoRaiz)
        {
            rutaXml = ruta;
            doc = new XmlDocument();

            XmlDeclaration xmlDeclaration = doc.CreateXmlDeclaration("1.0", "UTF-8", "no");

            XmlNode nodo = doc.DocumentElement;
            doc.InsertBefore(xmlDeclaration, nodo);

            XmlNode elemento = doc.CreateElement(nodoRaiz);
            doc.AppendChild(elemento);

            new Save_XML().guardarDatosXML(doc, ruta);
        }

        public void LeerXML(string ruta)
        {
            rutaXml = ruta;
            doc = new XmlDocument();
            doc.Load(ruta);
        }

        public void Registrar_Usuario(Objeto_Usuario user)
        {
            XmlNode  usuario = Crear_Usuario(user);

            XmlNode nodo = doc.DocumentElement;

            nodo.InsertAfter(usuario, nodo.LastChild);

            new Save_XML().guardarDatosXML(doc, rutaXml);

        }

        public XmlNode Crear_Usuario(Objeto_Usuario user)
        {
            XmlNode usu = doc.CreateElement("usuarios");

            XmlElement xcedula = doc.CreateElement("cedula");
            xcedula.InnerText = user.cedula.ToString();
            usu.AppendChild(xcedula);

            XmlElement xnombre = doc.CreateElement("nombre");
            xnombre.InnerText = user.nombre;
            usu.AppendChild(xnombre);

            XmlElement xgenero = doc.CreateElement("genero");
            xgenero.InnerText = user.genero;
            usu.AppendChild(xgenero);

            XmlElement xrol = doc.CreateElement("rol");
            xrol.InnerText = user.rol;
            usu.AppendChild(xrol);

            XmlElement xusuario = doc.CreateElement("usuario");
            xusuario.InnerText = user.usuario;
            usu.AppendChild(xusuario);

            XmlElement xcontraseña = doc.CreateElement("contraseña");
            xcontraseña.InnerText = user.contraseña;
            usu.AppendChild(xcontraseña);

            return usu;
        }

        public void Registrar_Entrada(Objeto_Entrada entrada)
        {
            XmlNode entra = Crear_Entrada(entrada);

            XmlNode nodo = doc.DocumentElement;

            nodo.InsertAfter(entra, nodo.LastChild);

            new Save_XML().guardarDatosXML(doc, rutaXml);
        }

        public XmlNode Crear_Entrada(Objeto_Entrada entrada)
        {
            XmlNode entra = doc.CreateElement("entradas");

            XmlElement xlocalidad = doc.CreateElement("localidad");
            xlocalidad.InnerText = entrada.localidad;
            entra.AppendChild(xlocalidad);

            XmlElement xprecio = doc.CreateElement("precio");
            xprecio.InnerText = entrada.precio.ToString();
            entra.AppendChild(xprecio);

            XmlElement xpatrocinador = doc.CreateElement("patrocinador");
            xpatrocinador.InnerText = entrada.patrocinador;
            entra.AppendChild(xpatrocinador);

            XmlElement xcantidad = doc.CreateElement("cantidad");
            xcantidad.InnerText = entrada.cantidad.ToString();
            entra.AppendChild(xcantidad);

            return entra;
        }

        public void Registrar_Venta(Objeto_Venta venta)
        {
            XmlNode v = Crear_Venta(venta);

            XmlNode nodo = doc.DocumentElement;

            nodo.InsertAfter(v, nodo.LastChild);

            new Save_XML().guardarDatosXML(doc, rutaXml);
        }

        public XmlNode Crear_Venta(Objeto_Venta venta)
        {
            XmlNode v = doc.CreateElement("venta");

            XmlElement xlocalidades = doc.CreateElement("localidades");
            xlocalidades.InnerText = venta.localidades;
            v.AppendChild(xlocalidades);

            XmlElement xcantidad = doc.CreateElement("cantidad");
            xcantidad.InnerText = venta.cantidad.ToString();
            v.AppendChild(xcantidad);

            XmlElement xpreciototal = doc.CreateElement("precio_total");
            xpreciototal.InnerText = venta.precio_total.ToString();
            v.AppendChild(xpreciototal);

            XmlElement xcedula = doc.CreateElement("cedula");
            xcedula.InnerText = venta.cedula.ToString();
            v.AppendChild(xcedula);

            XmlElement xnombre = doc.CreateElement("nombre");
            xnombre.InnerText = venta.nombre;
            v.AppendChild(xnombre);

            XmlElement xgenero = doc.CreateElement("genero");
            xgenero.InnerText = venta.genero;
            v.AppendChild(xgenero);

            return v;
        }

        public void Modificar_Entrada(Objeto_Entrada entrada)
        {
            XmlNodeList lista_entradas = doc.SelectNodes("Informacion/entradas");
            foreach (XmlNode item in lista_entradas)
            {
                if (item.SelectSingleNode("localidad").InnerText == entrada.localidad)
                {
                    item.SelectSingleNode("localidad").InnerText = entrada.localidad;
                    item.SelectSingleNode("precio").InnerText = entrada.precio.ToString();
                    item.SelectSingleNode("patrocinador").InnerText = entrada.patrocinador;
                    item.SelectSingleNode("cantidad").InnerText = entrada.cantidad.ToString();
                }
            }                            
            new Save_XML().guardarDatosXML(doc, rutaXml);
        }

        public void Modificar_Entrada_Venta(Objeto_Entrada entrada)
        {
            XmlNodeList lista_entradas = doc.SelectNodes("Informacion/entradas");
            foreach (XmlNode item in lista_entradas)
            {
                if (item.SelectSingleNode("localidad").InnerText == entrada.localidad)
                {
                    item.SelectSingleNode("cantidad").InnerText = entrada.cantidad.ToString();
                }
            }
            new Save_XML().guardarDatosXML(doc, rutaXml);
        }


        public void Eliminar_Entrada(Objeto_Entrada entrada)
        {
            XmlNodeList lista_entradas = doc.SelectNodes("Informacion/entradas");
            XmlNode entra = doc.DocumentElement;
            foreach (XmlNode item in lista_entradas)
            {
                if (item.SelectSingleNode("localidad").InnerText == entrada.localidad)
                {
                    XmlNode borrarNodo = item;
                    entra.RemoveChild(borrarNodo);
                }
            }
            new Save_XML().guardarDatosXML(doc, rutaXml);
        }

        public List<Objeto_Entrada> Extraer_Datos_Entradas()
        {

            List<Objeto_Entrada> lista = new List<Objeto_Entrada>();
            XmlNodeList lista_entrada = doc.SelectNodes("Informacion/entradas");
            XmlNode entra;

            for (int i = 0; i < lista_entrada.Count; i++)
            {
                entra = lista_entrada.Item(i);

                Objeto_Entrada entrada = new Objeto_Entrada();

                entrada.localidad = entra.SelectSingleNode("localidad").InnerText;
                entrada.precio = Convert.ToInt32(entra.SelectSingleNode("precio").InnerText);
                entrada.patrocinador = entra.SelectSingleNode("patrocinador").InnerText;
                entrada.cantidad = Convert.ToInt32(entra.SelectSingleNode("cantidad").InnerText);

                lista.Add(entrada);

            }
            return lista;
        }


        public List<Objeto_Usuario> Extraer_Datos(){

            List<Objeto_Usuario> lista = new List<Objeto_Usuario>();
            XmlNodeList lista_usuarios = doc.SelectNodes("Informacion/usuarios");
            XmlNode user;

            for (int i = 0; i < lista_usuarios.Count; i++)
            {
                user = lista_usuarios.Item(i);

                Objeto_Usuario usuario = new Objeto_Usuario();

                usuario.cedula = Convert.ToInt32(user.SelectSingleNode("cedula").InnerText);
                usuario.nombre = user.SelectSingleNode("nombre").InnerText;
                usuario.usuario = user.SelectSingleNode("usuario").InnerText;
                usuario.contraseña = user.SelectSingleNode("contraseña").InnerText;
                usuario.rol = user.SelectSingleNode("rol").InnerText;
                usuario.genero = user.SelectSingleNode("genero").InnerText;

                lista.Add(usuario);

            }
            return lista;
        }

    }
}
