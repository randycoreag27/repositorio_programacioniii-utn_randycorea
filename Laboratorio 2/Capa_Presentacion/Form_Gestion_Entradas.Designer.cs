﻿
namespace Capa_Presentacion
{
    partial class Form_Gestion_Entradas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form_Gestion_Entradas));
            this.dgv_gestion_entradas = new System.Windows.Forms.DataGridView();
            this.btn_salir = new System.Windows.Forms.PictureBox();
            this.column_localidad = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.column_precio = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.column_patrocinador = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.column_cantidad = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.agregar = new System.Windows.Forms.DataGridViewButtonColumn();
            this.modificar = new System.Windows.Forms.DataGridViewButtonColumn();
            this.eliminar = new System.Windows.Forms.DataGridViewButtonColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_gestion_entradas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_salir)).BeginInit();
            this.SuspendLayout();
            // 
            // dgv_gestion_entradas
            // 
            this.dgv_gestion_entradas.AllowUserToDeleteRows = false;
            this.dgv_gestion_entradas.AllowUserToResizeColumns = false;
            this.dgv_gestion_entradas.AllowUserToResizeRows = false;
            this.dgv_gestion_entradas.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCellsExceptHeader;
            this.dgv_gestion_entradas.BackgroundColor = System.Drawing.Color.Black;
            this.dgv_gestion_entradas.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgv_gestion_entradas.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal;
            this.dgv_gestion_entradas.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_gestion_entradas.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv_gestion_entradas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgv_gestion_entradas.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.column_localidad,
            this.column_precio,
            this.column_patrocinador,
            this.column_cantidad,
            this.agregar,
            this.modificar,
            this.eliminar});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgv_gestion_entradas.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgv_gestion_entradas.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv_gestion_entradas.EnableHeadersVisualStyles = false;
            this.dgv_gestion_entradas.GridColor = System.Drawing.Color.White;
            this.dgv_gestion_entradas.Location = new System.Drawing.Point(0, 0);
            this.dgv_gestion_entradas.Name = "dgv_gestion_entradas";
            this.dgv_gestion_entradas.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_gestion_entradas.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dgv_gestion_entradas.RowHeadersVisible = false;
            this.dgv_gestion_entradas.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv_gestion_entradas.Size = new System.Drawing.Size(694, 365);
            this.dgv_gestion_entradas.TabIndex = 0;
            this.dgv_gestion_entradas.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_gestion_entradas_CellContentClick);
            this.dgv_gestion_entradas.CellPainting += new System.Windows.Forms.DataGridViewCellPaintingEventHandler(this.dgv_gestion_entradas_CellPainting_1);
            // 
            // btn_salir
            // 
            this.btn_salir.BackColor = System.Drawing.Color.Transparent;
            this.btn_salir.Image = ((System.Drawing.Image)(resources.GetObject("btn_salir.Image")));
            this.btn_salir.Location = new System.Drawing.Point(662, 0);
            this.btn_salir.Name = "btn_salir";
            this.btn_salir.Size = new System.Drawing.Size(19, 23);
            this.btn_salir.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btn_salir.TabIndex = 1;
            this.btn_salir.TabStop = false;
            this.btn_salir.Click += new System.EventHandler(this.btn_salir_Click);
            // 
            // column_localidad
            // 
            this.column_localidad.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.column_localidad.FillWeight = 150F;
            this.column_localidad.Frozen = true;
            this.column_localidad.HeaderText = "Localidad";
            this.column_localidad.Name = "column_localidad";
            this.column_localidad.Width = 145;
            // 
            // column_precio
            // 
            this.column_precio.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.column_precio.FillWeight = 120F;
            this.column_precio.Frozen = true;
            this.column_precio.HeaderText = "Precio";
            this.column_precio.Name = "column_precio";
            this.column_precio.Width = 145;
            // 
            // column_patrocinador
            // 
            this.column_patrocinador.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.column_patrocinador.FillWeight = 120F;
            this.column_patrocinador.Frozen = true;
            this.column_patrocinador.HeaderText = "Patrocinador";
            this.column_patrocinador.Name = "column_patrocinador";
            this.column_patrocinador.Width = 145;
            // 
            // column_cantidad
            // 
            this.column_cantidad.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.column_cantidad.FillWeight = 120F;
            this.column_cantidad.Frozen = true;
            this.column_cantidad.HeaderText = "Cantidad";
            this.column_cantidad.Name = "column_cantidad";
            this.column_cantidad.Width = 145;
            // 
            // agregar
            // 
            this.agregar.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.agregar.Frozen = true;
            this.agregar.HeaderText = "";
            this.agregar.Name = "agregar";
            this.agregar.Width = 36;
            // 
            // modificar
            // 
            this.modificar.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.modificar.Frozen = true;
            this.modificar.HeaderText = "";
            this.modificar.Name = "modificar";
            this.modificar.Width = 36;
            // 
            // eliminar
            // 
            this.eliminar.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.eliminar.Frozen = true;
            this.eliminar.HeaderText = "";
            this.eliminar.Name = "eliminar";
            this.eliminar.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.eliminar.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.eliminar.Width = 36;
            // 
            // Form_Gestion_Entradas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.ClientSize = new System.Drawing.Size(694, 365);
            this.Controls.Add(this.btn_salir);
            this.Controls.Add(this.dgv_gestion_entradas);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Form_Gestion_Entradas";
            this.Opacity = 0.9D;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1_Gestión_Entradas";
            ((System.ComponentModel.ISupportInitialize)(this.dgv_gestion_entradas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_salir)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgv_gestion_entradas;
        private System.Windows.Forms.PictureBox btn_salir;
        private System.Windows.Forms.DataGridViewTextBoxColumn column_localidad;
        private System.Windows.Forms.DataGridViewTextBoxColumn column_precio;
        private System.Windows.Forms.DataGridViewTextBoxColumn column_patrocinador;
        private System.Windows.Forms.DataGridViewTextBoxColumn column_cantidad;
        private System.Windows.Forms.DataGridViewButtonColumn agregar;
        private System.Windows.Forms.DataGridViewButtonColumn modificar;
        private System.Windows.Forms.DataGridViewButtonColumn eliminar;
    }
}