﻿using System;
using System.Windows.Forms;

namespace Capa_Presentacion
{
    public partial class Form_Administrador : Form
    {
        public Form_Administrador()
        {
            InitializeComponent();
        }

        public void Entradas(Form frm_en)
        {
            frm_en.TopLevel = false;
            this.panel_escritorio.Controls.Add(frm_en);
            frm_en.Show();

        }

        private void btn_salir_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void Form_Administrador_Load(object sender, EventArgs e)
        {
            label_usuario.Text = Form_Inicio_Sesion.nombre;
        }

        private void btn_cerrar_sesion_Click(object sender, EventArgs e)
        {

            Form_Inicio_Sesion frm_i = new Form_Inicio_Sesion();
            this.Hide();
            frm_i.Show();
        }

        private void btn_entradas_Click(object sender, EventArgs e)
        {
            Form_Gestion_Entradas frm_en = new Form_Gestion_Entradas();
            Entradas(frm_en);
        }
    }
}
