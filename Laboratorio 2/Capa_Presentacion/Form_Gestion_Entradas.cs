﻿using Capa_Negocio;
using Objetos;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace Capa_Presentacion
{
    public partial class Form_Gestion_Entradas : Form
    {

        Objeto_Entrada entrada = new Objeto_Entrada();
        Archivo_XML arch_xml = new Archivo_XML();
        List<Objeto_Entrada> lista = new List<Objeto_Entrada>();

        public Form_Gestion_Entradas()
        {
            InitializeComponent();
            Crear_XML();
            Mostrar_Datos();
        }

        public void Crear_XML()
        {
            string ruta = "Informacion.xml";

            if (!File.Exists(ruta))
            {
                arch_xml.CrearXML(ruta, "Informacion");
            }
            else
            {
                arch_xml.LeerXML(ruta);
            }
        }

        public void Obtener_Datos(DataGridViewCellEventArgs e)
        {
            DataGridViewRow row = dgv_gestion_entradas.Rows[e.RowIndex];
            entrada.localidad = row.Cells["column_localidad"].Value.ToString();
            entrada.precio = Convert.ToInt32(row.Cells["column_precio"].Value);
            entrada.patrocinador = row.Cells["column_patrocinador"].Value.ToString();
            entrada.cantidad = Convert.ToInt32(row.Cells["column_cantidad"].Value);
        }

        public void Mostrar_Datos()
        {
            lista = arch_xml.Extraer_Datos_Entradas();
            for(int i = 0; i < lista.Count; i++)
            {
                DataGridViewRow fila = new DataGridViewRow();
                fila.CreateCells(dgv_gestion_entradas);
                fila.Cells[0].Value = lista[i].localidad;
                fila.Cells[1].Value = lista[i].precio;
                fila.Cells[2].Value = lista[i].patrocinador;
                fila.Cells[3].Value = lista[i].cantidad;
                dgv_gestion_entradas.Rows.Add(fila);
            }
        }


        private void dgv_gestion_entradas_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 4)
            {
                Obtener_Datos(e);
                arch_xml.Registrar_Entrada(entrada);
                dgv_gestion_entradas.Rows.Clear();
                Mostrar_Datos();
            }

            if (e.ColumnIndex == 5)
            {
                Obtener_Datos(e);
                arch_xml.Modificar_Entrada(entrada);
                dgv_gestion_entradas.Rows.Clear();
                Mostrar_Datos();
            }

            if (e.ColumnIndex == 6)
            {
                Obtener_Datos(e);
                arch_xml.Eliminar_Entrada(entrada);
                dgv_gestion_entradas.Rows.Clear();
                Mostrar_Datos();
            }

        }

        private void dgv_gestion_entradas_CellPainting_1(object sender, DataGridViewCellPaintingEventArgs e)
        {
            if (e.ColumnIndex >= 0 && this.dgv_gestion_entradas.Columns[e.ColumnIndex].Name == "agregar" && e.RowIndex >= 0)
            {
                e.Paint(e.CellBounds, DataGridViewPaintParts.All);

                DataGridViewButtonCell btn_agregar = this.dgv_gestion_entradas.Rows[e.RowIndex].Cells["agregar"] as DataGridViewButtonCell;
                Icon agregar = new Icon(Environment.CurrentDirectory + @"\icono_agregar.ico");
                e.Graphics.DrawIcon(agregar, e.CellBounds.Left + 3, e.CellBounds.Top + 3);

                this.dgv_gestion_entradas.Rows[e.RowIndex].Height = agregar.Height + 10;
                this.dgv_gestion_entradas.Columns[e.ColumnIndex].Width = agregar.Width + 10;

                e.Handled = true;
            }

            if (e.ColumnIndex >= 0 && this.dgv_gestion_entradas.Columns[e.ColumnIndex].Name == "modificar" && e.RowIndex >= 0)
            {
                e.Paint(e.CellBounds, DataGridViewPaintParts.All);

                DataGridViewButtonCell btn_modificar = this.dgv_gestion_entradas.Rows[e.RowIndex].Cells["modificar"] as DataGridViewButtonCell;
                Icon modificar = new Icon(Environment.CurrentDirectory + @"\icono_modificar.ico");
                e.Graphics.DrawIcon(modificar, e.CellBounds.Left + 3, e.CellBounds.Top + 3);


                this.dgv_gestion_entradas.Rows[e.RowIndex].Height = modificar.Height + 10;
                this.dgv_gestion_entradas.Columns[e.ColumnIndex].Width = modificar.Width + 10;

                e.Handled = true;
            }

            if (e.ColumnIndex >= 0 && this.dgv_gestion_entradas.Columns[e.ColumnIndex].Name == "eliminar" && e.RowIndex >= 0)
            {
                e.Paint(e.CellBounds, DataGridViewPaintParts.All);

                DataGridViewButtonCell btn_eliminar = this.dgv_gestion_entradas.Rows[e.RowIndex].Cells["eliminar"] as DataGridViewButtonCell;
                Icon eliminar = new Icon(Environment.CurrentDirectory + @"\icono_eliminar.ico");
                e.Graphics.DrawIcon(eliminar, e.CellBounds.Left + 3, e.CellBounds.Top + 3);

                this.dgv_gestion_entradas.Rows[e.RowIndex].Height = eliminar.Height + 10;
                this.dgv_gestion_entradas.Columns[e.ColumnIndex].Width = eliminar.Width + 10;

                e.Handled = true;
            }

        }

        private void btn_salir_Click(object sender, EventArgs e)
        {
            this.Hide();
        }
    }
}
