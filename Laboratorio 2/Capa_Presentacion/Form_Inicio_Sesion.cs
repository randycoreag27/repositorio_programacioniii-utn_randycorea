﻿using Capa_Negocio;
using Objetos;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace Capa_Presentacion
{
    public partial class Form_Inicio_Sesion : Form
    {

        Archivo_XML arch_xml = new Archivo_XML();
        List<Objeto_Usuario> lista;
        Objeto_Usuario obj_u = new Objeto_Usuario();
        public static string nombre = "";
        public static string genero = "";
        public static int cedula = 0;

        public Form_Inicio_Sesion()
        {
            InitializeComponent();
            Crear_XML();
        }

        public void Validacion_Inicio_Sesion()
        {
            lista = arch_xml.Extraer_Datos();

            if (txt_usuario.Text == "" || txt_contraseña.Text == ""){
                Form_MessageBox_Validacion_Campos frm_v = new Form_MessageBox_Validacion_Campos();
                frm_v.Show();
            }

            for (int i = 0; i < lista.Count; i++ )
            {
                if (lista[i].usuario.Equals(txt_usuario.Text) && lista[i].contraseña.Equals(txt_contraseña.Text) 
                    && lista[i].rol.Equals("Administrador"))
                {
                    Form_Administrador frm_a = new Form_Administrador();
                    nombre = lista[i].nombre;
                    cedula = lista[i].cedula;
                    obj_u.usuario = lista[i].usuario;
                    obj_u.contraseña = lista[i].contraseña;
                    obj_u.rol = lista[i].rol;
                    frm_a.Show();
                    this.Hide();

                }
                if (lista[i].usuario.Equals(txt_usuario.Text) && lista[i].contraseña.Equals(txt_contraseña.Text)
                    && lista[i].rol.Equals("Cliente"))
                {
                    Form_Cliente frm_c = new Form_Cliente();
                    nombre = lista[i].nombre;
                    genero = lista[i].genero;
                    cedula = lista[i].cedula;
                    obj_u.usuario = lista[i].usuario;
                    obj_u.contraseña = lista[i].contraseña;
                    obj_u.rol = lista[i].rol;
                    frm_c.Show();
                    this.Hide();
                }
            }

            if(obj_u.usuario != (txt_usuario.Text) && obj_u.contraseña != (txt_contraseña.Text)
                   && obj_u.rol != ("Cliente"))
            {
                   Form_MessageBox_Error_Inicio_Sesion frm_e = new Form_MessageBox_Error_Inicio_Sesion();
                   frm_e.Show();
            }
        }

        public void Crear_XML()
        {
            string ruta = "Informacion.xml";

            if (!File.Exists(ruta))
            {
                arch_xml.CrearXML(ruta, "Informacion");
            }
            else
            {
                arch_xml.LeerXML(ruta);
            }
        }

        private void btn_salir_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btn_ventana_registro_Click(object sender, EventArgs e)
        {
            Form_Registro form_registro = new Form_Registro();
            form_registro.Show();
            this.Hide();
        }

        private void btn_iniciar_sesion_Click(object sender, EventArgs e)
        {
            Validacion_Inicio_Sesion();
        }
    }
}
