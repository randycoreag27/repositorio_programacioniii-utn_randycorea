﻿using Capa_Negocio;
using Objetos;
using System;
using System.IO;
using System.Windows.Forms;

namespace Capa_Presentacion
{
    public partial class Form_Registro : Form
    {

        Objeto_Usuario obj_u = new Objeto_Usuario();
        Archivo_XML arch_xml = new Archivo_XML();

        public Form_Registro()
        {
            InitializeComponent();
            Crear_XML();
            Cargar_Combo();
        }

        public void Cargar_Combo()
        {
            cbx_genero.Items.Add("Masculino");
            cbx_genero.Items.Add("Femenino");
        }

        public void Crear_XML()
        {
            string ruta = "Informacion.xml";

            if (!File.Exists(ruta))
            {
                arch_xml.CrearXML(ruta, "Informacion");
            }
            else
            {
                arch_xml.LeerXML(ruta);
            }
        }

        public void Limpiar_Campos()
        {
            txt_cedula.Clear();
            txt_contraseña.Clear();
            txt_nombre.Clear();
            txt_usuario.Clear();
        }

        public void Obtener_Datos()
        {
            obj_u.cedula = Convert.ToInt32(txt_cedula.Text);
            obj_u.nombre = txt_nombre.Text;
            obj_u.genero = cbx_genero.SelectedItem.ToString();
            obj_u.rol = "Cliente";
            obj_u.usuario = txt_usuario.Text;
            obj_u.contraseña = txt_contraseña.Text;
        }

        private void btn_salir_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btn_registrarse_Click(object sender, EventArgs e)
        {
            if (txt_cedula.Text == "" || txt_nombre.Text == "" || txt_usuario.Text == ""
                || txt_contraseña.Text == "" || cbx_genero.SelectedItem == null)
            {
                Form_MessageBox_Validacion_Campos v = new Form_MessageBox_Validacion_Campos();
                v.Show();
            }
            else
            {
                Obtener_Datos();
                arch_xml.Registrar_Usuario(obj_u);
                Limpiar_Campos();
                Form_MessageBox_Confirmacion frm_confi = new Form_MessageBox_Confirmacion();
                frm_confi.Show();
            }
            
        }

        private void btn_regresar_Click(object sender, EventArgs e)
        {
            Form_Inicio_Sesion frm_ini = new Form_Inicio_Sesion();
            frm_ini.Show();
            this.Hide();
        }
    }
}
