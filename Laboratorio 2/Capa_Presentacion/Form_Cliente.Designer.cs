﻿
namespace Capa_Presentacion
{
    partial class Form_Cliente
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form_Cliente));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.dvg_venta_entradas = new System.Windows.Forms.DataGridView();
            this.btn_comprar = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.txt_cantidad_entradas = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txt_monto_total = new System.Windows.Forms.TextBox();
            this.column_localidad = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.column_precio = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.column_disponibles = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.column_cantidad = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.agregar = new System.Windows.Forms.DataGridViewButtonColumn();
            this.btn_salir = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dvg_venta_entradas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_salir)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(18, 43);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(411, 378);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // dvg_venta_entradas
            // 
            this.dvg_venta_entradas.AllowUserToAddRows = false;
            this.dvg_venta_entradas.AllowUserToDeleteRows = false;
            this.dvg_venta_entradas.AllowUserToResizeColumns = false;
            this.dvg_venta_entradas.AllowUserToResizeRows = false;
            this.dvg_venta_entradas.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCellsExceptHeader;
            this.dvg_venta_entradas.BackgroundColor = System.Drawing.Color.Black;
            this.dvg_venta_entradas.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dvg_venta_entradas.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal;
            this.dvg_venta_entradas.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dvg_venta_entradas.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dvg_venta_entradas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dvg_venta_entradas.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.column_localidad,
            this.column_precio,
            this.column_disponibles,
            this.column_cantidad,
            this.agregar});
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dvg_venta_entradas.DefaultCellStyle = dataGridViewCellStyle3;
            this.dvg_venta_entradas.EnableHeadersVisualStyles = false;
            this.dvg_venta_entradas.GridColor = System.Drawing.Color.White;
            this.dvg_venta_entradas.Location = new System.Drawing.Point(445, 43);
            this.dvg_venta_entradas.Name = "dvg_venta_entradas";
            this.dvg_venta_entradas.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dvg_venta_entradas.RowHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.dvg_venta_entradas.RowHeadersVisible = false;
            this.dvg_venta_entradas.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dvg_venta_entradas.Size = new System.Drawing.Size(401, 258);
            this.dvg_venta_entradas.TabIndex = 2;
            this.dvg_venta_entradas.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dvg_venta_entradas_CellContentClick);
            this.dvg_venta_entradas.CellPainting += new System.Windows.Forms.DataGridViewCellPaintingEventHandler(this.dvg_venta_entradas_CellPainting);
            // 
            // btn_comprar
            // 
            this.btn_comprar.BackColor = System.Drawing.Color.Black;
            this.btn_comprar.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btn_comprar.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DimGray;
            this.btn_comprar.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Black;
            this.btn_comprar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_comprar.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_comprar.ForeColor = System.Drawing.Color.White;
            this.btn_comprar.Location = new System.Drawing.Point(738, 405);
            this.btn_comprar.Name = "btn_comprar";
            this.btn_comprar.Size = new System.Drawing.Size(119, 39);
            this.btn_comprar.TabIndex = 4;
            this.btn_comprar.Text = "Comprar";
            this.btn_comprar.UseVisualStyleBackColor = false;
            this.btn_comprar.Click += new System.EventHandler(this.btn_comprar_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(612, 378);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(121, 20);
            this.label1.TabIndex = 5;
            this.label1.Text = "Total a Pagar:";
            // 
            // txt_cantidad_entradas
            // 
            this.txt_cantidad_entradas.BackColor = System.Drawing.Color.Black;
            this.txt_cantidad_entradas.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txt_cantidad_entradas.Enabled = false;
            this.txt_cantidad_entradas.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_cantidad_entradas.ForeColor = System.Drawing.Color.White;
            this.txt_cantidad_entradas.Location = new System.Drawing.Point(738, 347);
            this.txt_cantidad_entradas.Name = "txt_cantidad_entradas";
            this.txt_cantidad_entradas.Size = new System.Drawing.Size(149, 19);
            this.txt_cantidad_entradas.TabIndex = 6;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(543, 346);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(189, 20);
            this.label2.TabIndex = 7;
            this.label2.Text = "Cantidad de Entradas:";
            // 
            // txt_monto_total
            // 
            this.txt_monto_total.BackColor = System.Drawing.Color.Black;
            this.txt_monto_total.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txt_monto_total.Enabled = false;
            this.txt_monto_total.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_monto_total.ForeColor = System.Drawing.Color.White;
            this.txt_monto_total.Location = new System.Drawing.Point(738, 379);
            this.txt_monto_total.Name = "txt_monto_total";
            this.txt_monto_total.Size = new System.Drawing.Size(149, 19);
            this.txt_monto_total.TabIndex = 8;
            // 
            // column_localidad
            // 
            this.column_localidad.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.column_localidad.FillWeight = 150F;
            this.column_localidad.Frozen = true;
            this.column_localidad.HeaderText = "Localidad";
            this.column_localidad.Name = "column_localidad";
            this.column_localidad.Width = 110;
            // 
            // column_precio
            // 
            this.column_precio.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.column_precio.FillWeight = 120F;
            this.column_precio.Frozen = true;
            this.column_precio.HeaderText = "Precio";
            this.column_precio.Name = "column_precio";
            this.column_precio.Width = 110;
            // 
            // column_disponibles
            // 
            this.column_disponibles.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.column_disponibles.Frozen = true;
            this.column_disponibles.HeaderText = "Disponibles";
            this.column_disponibles.Name = "column_disponibles";
            this.column_disponibles.Visible = false;
            this.column_disponibles.Width = 110;
            // 
            // column_cantidad
            // 
            this.column_cantidad.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.White;
            this.column_cantidad.DefaultCellStyle = dataGridViewCellStyle2;
            this.column_cantidad.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.column_cantidad.FillWeight = 120F;
            this.column_cantidad.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.column_cantidad.Frozen = true;
            this.column_cantidad.HeaderText = "Cantidad";
            this.column_cantidad.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5"});
            this.column_cantidad.Name = "column_cantidad";
            this.column_cantidad.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.column_cantidad.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.column_cantidad.Width = 110;
            // 
            // agregar
            // 
            this.agregar.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.agregar.Frozen = true;
            this.agregar.HeaderText = "";
            this.agregar.Name = "agregar";
            this.agregar.Width = 22;
            // 
            // btn_salir
            // 
            this.btn_salir.BackColor = System.Drawing.Color.Transparent;
            this.btn_salir.Image = ((System.Drawing.Image)(resources.GetObject("btn_salir.Image")));
            this.btn_salir.Location = new System.Drawing.Point(822, 10);
            this.btn_salir.Name = "btn_salir";
            this.btn_salir.Size = new System.Drawing.Size(26, 24);
            this.btn_salir.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btn_salir.TabIndex = 9;
            this.btn_salir.TabStop = false;
            this.btn_salir.Click += new System.EventHandler(this.btn_salir_Click);
            // 
            // Form_Cliente
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.ClientSize = new System.Drawing.Size(860, 450);
            this.Controls.Add(this.btn_salir);
            this.Controls.Add(this.txt_monto_total);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txt_cantidad_entradas);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btn_comprar);
            this.Controls.Add(this.dvg_venta_entradas);
            this.Controls.Add(this.pictureBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Form_Cliente";
            this.Opacity = 0.9D;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form_Cliente";
            this.Load += new System.EventHandler(this.Form_Cliente_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dvg_venta_entradas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_salir)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.DataGridView dvg_venta_entradas;
        private System.Windows.Forms.Button btn_comprar;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txt_cantidad_entradas;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txt_monto_total;
        private System.Windows.Forms.DataGridViewTextBoxColumn column_localidad;
        private System.Windows.Forms.DataGridViewTextBoxColumn column_precio;
        private System.Windows.Forms.DataGridViewTextBoxColumn column_disponibles;
        private System.Windows.Forms.DataGridViewComboBoxColumn column_cantidad;
        private System.Windows.Forms.DataGridViewButtonColumn agregar;
        private System.Windows.Forms.PictureBox btn_salir;
    }
}