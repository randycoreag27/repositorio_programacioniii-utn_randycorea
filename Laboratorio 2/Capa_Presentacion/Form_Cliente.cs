﻿using Capa_Negocio;
using Objetos;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace Capa_Presentacion
{
    public partial class Form_Cliente : Form
    {

        Archivo_XML arch_xml = new Archivo_XML();
        Objeto_Entrada entrada = new Objeto_Entrada();
        Objeto_Venta venta = new Objeto_Venta();
        List<Objeto_Entrada> lista = new List<Objeto_Entrada>();
        int cantidad_final = 0;
        int monto_total = 0;
        string localidades = "";
        int dispo = 0;
        int canti = 0;

        public Form_Cliente()
        {
            InitializeComponent();
            Crear_XML();
            Mostrar_Datos();
        }

        public void Crear_XML()
        {
            string ruta = "Informacion.xml";

            if (!File.Exists(ruta))
            {
                arch_xml.CrearXML(ruta, "Informacion");
            }
            else
            {
                arch_xml.LeerXML(ruta);
            }
        }

        public void Mostrar_Datos()
        {
            lista = arch_xml.Extraer_Datos_Entradas();
            for (int i = 0; i < lista.Count; i++)
            {
                DataGridViewRow fila = new DataGridViewRow();
                fila.CreateCells(dvg_venta_entradas);
                fila.Cells[0].Value = lista[i].localidad;
                fila.Cells[1].Value = lista[i].precio;
                fila.Cells[2].Value = lista[i].cantidad;
                dvg_venta_entradas.Rows.Add(fila);
            }
        }

        public void Obtener_Datos(DataGridViewCellEventArgs e)
        {
            DataGridViewRow row = dvg_venta_entradas.Rows[e.RowIndex];
            venta.cantidad = Convert.ToInt32(row.Cells["column_cantidad"].Value);
            venta.precio_total = Convert.ToInt32(row.Cells["column_precio"].Value);
            venta.localidades = entrada.localidad = row.Cells["column_localidad"].Value.ToString();
            canti = Convert.ToInt32(row.Cells["column_cantidad"].Value);
        }

        public void Obtener_Datos2(DataGridViewCellEventArgs e)
        {
            DataGridViewRow row = dvg_venta_entradas.Rows[e.RowIndex];
            entrada.localidad = row.Cells["column_localidad"].Value.ToString();
            entrada.precio = Convert.ToInt32(row.Cells["column_precio"].Value);
            entrada.cantidad = Convert.ToInt32(row.Cells["column_disponibles"].Value) - Convert.ToInt32(row.Cells["column_cantidad"].Value);
            dispo = Convert.ToInt32(row.Cells["column_disponibles"].Value);
        }

        public void Datos_Compra()
        {

            int monto;
            int cantidad;
            string localidad;

            monto = venta.precio_total*venta.cantidad;
            cantidad = venta.cantidad;
            localidad = venta.localidades;

            monto_total = monto_total + monto;
            cantidad_final = cantidad_final + cantidad;
            localidades = localidades + localidad + " ";

            txt_cantidad_entradas.Text = cantidad_final.ToString();
            txt_monto_total.Text = monto_total.ToString();

        }

        private void Form_Cliente_Load(object sender, EventArgs e)
        {
            
        }

        private void dvg_venta_entradas_CellPainting(object sender, DataGridViewCellPaintingEventArgs e)
        {
            if (e.ColumnIndex >= 0 && this.dvg_venta_entradas.Columns[e.ColumnIndex].Name == "agregar" && e.RowIndex >= 0)
            {
                e.Paint(e.CellBounds, DataGridViewPaintParts.All);

                DataGridViewButtonCell btn_agregar = this.dvg_venta_entradas.Rows[e.RowIndex].Cells["agregar"] as DataGridViewButtonCell;
                Icon agregar = new Icon(Environment.CurrentDirectory + @"\icono_agregar.ico");
                e.Graphics.DrawIcon(agregar, e.CellBounds.Left + 3, e.CellBounds.Top + 3);

                this.dvg_venta_entradas.Rows[e.RowIndex].Height = agregar.Height + 10;
                this.dvg_venta_entradas.Columns[e.ColumnIndex].Width = agregar.Width + 10;

                e.Handled = true;
            }
        }

        private void dvg_venta_entradas_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 4)
            {
                Obtener_Datos(e);
                Obtener_Datos2(e);
                if (dispo < canti) {
                    MessageBox.Show("No hay entradas disponibles", "Error");
                }
                else
                {
                    Datos_Compra();
                }
            }
        }

        private void btn_comprar_Click(object sender, EventArgs e)
        {
            venta.localidades = localidades;
            venta.cantidad = Convert.ToInt32(txt_cantidad_entradas.Text);
            venta.precio_total = Convert.ToInt32(txt_monto_total.Text);
            venta.cedula = Form_Inicio_Sesion.cedula;
            venta.nombre = Form_Inicio_Sesion.nombre;
            venta.genero = Form_Inicio_Sesion.genero;

            arch_xml.Modificar_Entrada_Venta(entrada);
            arch_xml.Registrar_Venta(venta);
            dvg_venta_entradas.Rows.Clear();
            Mostrar_Datos();

            txt_cantidad_entradas.Clear();
            txt_monto_total.Clear();

        }

        private void btn_salir_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
