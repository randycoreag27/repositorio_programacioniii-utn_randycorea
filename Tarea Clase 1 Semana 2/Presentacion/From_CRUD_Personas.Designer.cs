﻿
namespace Presentacion
{
    partial class From_CRUD_Personas
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.txt_cedula = new System.Windows.Forms.TextBox();
            this.txt_nombre = new System.Windows.Forms.TextBox();
            this.txt_residencia = new System.Windows.Forms.TextBox();
            this.data_grid_view_personas = new System.Windows.Forms.DataGridView();
            this.boton_guardar = new System.Windows.Forms.Button();
            this.boton_eliminar = new System.Windows.Forms.Button();
            this.boton_modificar = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.date_time_persona = new System.Windows.Forms.DateTimePicker();
            this.date_time_busqueda = new System.Windows.Forms.DateTimePicker();
            this.boton_buscar = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.data_grid_view_personas)).BeginInit();
            this.SuspendLayout();
            // 
            // txt_cedula
            // 
            this.txt_cedula.Location = new System.Drawing.Point(180, 58);
            this.txt_cedula.Name = "txt_cedula";
            this.txt_cedula.Size = new System.Drawing.Size(198, 20);
            this.txt_cedula.TabIndex = 0;
            this.txt_cedula.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // txt_nombre
            // 
            this.txt_nombre.Location = new System.Drawing.Point(180, 111);
            this.txt_nombre.Name = "txt_nombre";
            this.txt_nombre.Size = new System.Drawing.Size(198, 20);
            this.txt_nombre.TabIndex = 1;
            // 
            // txt_residencia
            // 
            this.txt_residencia.Location = new System.Drawing.Point(180, 212);
            this.txt_residencia.Name = "txt_residencia";
            this.txt_residencia.Size = new System.Drawing.Size(198, 20);
            this.txt_residencia.TabIndex = 2;
            // 
            // data_grid_view_personas
            // 
            this.data_grid_view_personas.AllowUserToAddRows = false;
            this.data_grid_view_personas.AllowUserToDeleteRows = false;
            this.data_grid_view_personas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.data_grid_view_personas.Location = new System.Drawing.Point(23, 248);
            this.data_grid_view_personas.Name = "data_grid_view_personas";
            this.data_grid_view_personas.ReadOnly = true;
            this.data_grid_view_personas.Size = new System.Drawing.Size(652, 199);
            this.data_grid_view_personas.TabIndex = 3;
            this.data_grid_view_personas.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.data_grid_view_personas_CellContentClick);
            // 
            // boton_guardar
            // 
            this.boton_guardar.Location = new System.Drawing.Point(438, 111);
            this.boton_guardar.Name = "boton_guardar";
            this.boton_guardar.Size = new System.Drawing.Size(237, 35);
            this.boton_guardar.TabIndex = 4;
            this.boton_guardar.Text = "Guardar";
            this.boton_guardar.UseVisualStyleBackColor = true;
            this.boton_guardar.Click += new System.EventHandler(this.boton_guardar_Click);
            // 
            // boton_eliminar
            // 
            this.boton_eliminar.Location = new System.Drawing.Point(438, 153);
            this.boton_eliminar.Name = "boton_eliminar";
            this.boton_eliminar.Size = new System.Drawing.Size(237, 37);
            this.boton_eliminar.TabIndex = 5;
            this.boton_eliminar.Text = "Eliminar";
            this.boton_eliminar.UseVisualStyleBackColor = true;
            this.boton_eliminar.Click += new System.EventHandler(this.boton_eliminar_Click);
            // 
            // boton_modificar
            // 
            this.boton_modificar.Location = new System.Drawing.Point(438, 196);
            this.boton_modificar.Name = "boton_modificar";
            this.boton_modificar.Size = new System.Drawing.Size(237, 38);
            this.boton_modificar.TabIndex = 6;
            this.boton_modificar.Text = "Modificar";
            this.boton_modificar.UseVisualStyleBackColor = true;
            this.boton_modificar.Click += new System.EventHandler(this.boton_modificar_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft YaHei", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(18, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(107, 28);
            this.label1.TabIndex = 7;
            this.label1.Text = "Personas";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft YaHei", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(19, 58);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(70, 22);
            this.label2.TabIndex = 8;
            this.label2.Text = "Cédula:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft YaHei", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(19, 111);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(83, 22);
            this.label3.TabIndex = 9;
            this.label3.Text = "Nombre:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft YaHei", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(19, 212);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(100, 22);
            this.label4.TabIndex = 10;
            this.label4.Text = "Residencia:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft YaHei", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(19, 161);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(159, 22);
            this.label5.TabIndex = 11;
            this.label5.Text = "Fecha Nacimiento:";
            // 
            // date_time_persona
            // 
            this.date_time_persona.Location = new System.Drawing.Point(180, 162);
            this.date_time_persona.Name = "date_time_persona";
            this.date_time_persona.Size = new System.Drawing.Size(198, 20);
            this.date_time_persona.TabIndex = 12;
            // 
            // date_time_busqueda
            // 
            this.date_time_busqueda.Location = new System.Drawing.Point(438, 55);
            this.date_time_busqueda.Name = "date_time_busqueda";
            this.date_time_busqueda.Size = new System.Drawing.Size(237, 20);
            this.date_time_busqueda.TabIndex = 13;
            // 
            // boton_buscar
            // 
            this.boton_buscar.Location = new System.Drawing.Point(438, 81);
            this.boton_buscar.Name = "boton_buscar";
            this.boton_buscar.Size = new System.Drawing.Size(237, 23);
            this.boton_buscar.TabIndex = 14;
            this.boton_buscar.Text = "Buscar";
            this.boton_buscar.UseVisualStyleBackColor = true;
            this.boton_buscar.Click += new System.EventHandler(this.boton_buscar_Click);
            // 
            // From_CRUD_Personas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(699, 458);
            this.Controls.Add(this.boton_buscar);
            this.Controls.Add(this.date_time_busqueda);
            this.Controls.Add(this.date_time_persona);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.boton_modificar);
            this.Controls.Add(this.boton_eliminar);
            this.Controls.Add(this.boton_guardar);
            this.Controls.Add(this.data_grid_view_personas);
            this.Controls.Add(this.txt_residencia);
            this.Controls.Add(this.txt_nombre);
            this.Controls.Add(this.txt_cedula);
            this.MaximizeBox = false;
            this.Name = "From_CRUD_Personas";
            this.Text = "CRUD Personas";
            this.Load += new System.EventHandler(this.From_CRUD_Personas_Load);
            ((System.ComponentModel.ISupportInitialize)(this.data_grid_view_personas)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txt_cedula;
        private System.Windows.Forms.TextBox txt_nombre;
        private System.Windows.Forms.TextBox txt_residencia;
        private System.Windows.Forms.DataGridView data_grid_view_personas;
        private System.Windows.Forms.Button boton_guardar;
        private System.Windows.Forms.Button boton_eliminar;
        private System.Windows.Forms.Button boton_modificar;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DateTimePicker date_time_persona;
        private System.Windows.Forms.DateTimePicker date_time_busqueda;
        private System.Windows.Forms.Button boton_buscar;
    }
}

