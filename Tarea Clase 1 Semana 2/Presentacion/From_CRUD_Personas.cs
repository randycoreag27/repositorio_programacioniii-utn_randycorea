﻿using Negocio;
using Objetos;
using System;
using System.Windows.Forms;

namespace Presentacion
{
    public partial class From_CRUD_Personas : Form
    {
        public BO_Persona boPer;
        BO_Persona persona = new BO_Persona();
        public From_CRUD_Personas()
        {
            InitializeComponent();
            boPer = new BO_Persona();
        }

        public void LimpiarCampos()
        {
            txt_cedula.Clear();
            txt_nombre.Clear();
            date_time_persona.Value = DateTime.Now;
            txt_residencia.Clear();
        }

        public void Mostrar_Datos()
        {
            persona.Mostrar(data_grid_view_personas);
        }


        private void From_CRUD_Personas_Load(object sender, EventArgs e)
        {
            Mostrar_Datos();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void boton_guardar_Click(object sender, EventArgs e)
        {
            Objeto_Persona persona = new Objeto_Persona
            {
                cedula = Convert.ToInt32(txt_cedula.Text),
                nombre = txt_nombre.Text,
                fecha_nacimiento = Convert.ToDateTime(date_time_persona.Text),
                residencia = txt_residencia.Text
            };
            boPer.Registrar(persona);
            LimpiarCampos();
            Mostrar_Datos();
        }

        private void data_grid_view_personas_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if(e.RowIndex >= 0)
            {
                txt_cedula.Enabled = false;
                DataGridViewRow row = data_grid_view_personas.Rows[e.RowIndex];
                txt_cedula.Text = row.Cells["cedula"].Value.ToString();
                txt_nombre.Text = row.Cells["nombre"].Value.ToString();
                txt_residencia.Text = row.Cells["residencia"].Value.ToString();
                date_time_persona.Value = Convert.ToDateTime(row.Cells["fecha_nacimiento"].Value.ToString());

            }
        }

        private void boton_eliminar_Click(object sender, EventArgs e)
        {
            Objeto_Persona persona = new Objeto_Persona
            {
                cedula = Convert.ToInt32(txt_cedula.Text),
            };
            boPer.Eliminar(persona);
            LimpiarCampos();
            Mostrar_Datos();
        }

        private void boton_buscar_Click(object sender, EventArgs e)
        {
            Objeto_Persona persona = new Objeto_Persona
            {
                residencia = txt_residencia.Text
            };
            boPer.Busqueda(data_grid_view_personas, persona);
        }

        private void boton_modificar_Click(object sender, EventArgs e)
        {
            txt_cedula.Enabled = false;
            Objeto_Persona persona = new Objeto_Persona
            {
                cedula = Convert.ToInt32(txt_cedula.Text),
                nombre = txt_nombre.Text,
                fecha_nacimiento = Convert.ToDateTime(date_time_persona.Text),
                residencia = txt_residencia.Text
            };
            boPer.Modificar(persona);
            LimpiarCampos();
            Mostrar_Datos();
        }
    }
}
