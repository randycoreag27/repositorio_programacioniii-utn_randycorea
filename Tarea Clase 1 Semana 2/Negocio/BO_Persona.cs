﻿using Objetos;
using Datos;
using System.Windows.Forms;

namespace Negocio
{
    public class BO_Persona
    {
        DAO_Persona bd_persona = new DAO_Persona();
        public void Registrar(Objeto_Persona per)
        {
            bd_persona.InsertarDatos(per);
        }

        public void Eliminar(Objeto_Persona per)
        {
            bd_persona.EliminarDatos(per);
        }

        public void Mostrar(DataGridView dgv)
        {
            bd_persona.Mostrar_Personas(dgv);
        }

        public void Busqueda(DataGridView dgv, Objeto_Persona per)
        {
            bd_persona.Busqueda_Personas(dgv, per);
        }

        public void Modificar(Objeto_Persona per)
        {
            bd_persona.Modificar_Personas(per);
        }
    }
}
