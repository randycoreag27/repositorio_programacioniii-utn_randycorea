﻿using Npgsql;
using Objetos;
using System;
using System.Data;
using System.Windows.Forms;

namespace Datos
{
    public class DAO_Persona
    {
        static NpgsqlConnection conexion;
        static NpgsqlCommand cmd;
        Objeto_Persona persona;

        public void InsertarDatos(Objeto_Persona per)
        {
            conexion = Conexion_BD.connection();
            conexion.Open();
            cmd = new NpgsqlCommand("INSERT INTO Persona(cedula,nombre,fecha_nacimiento,residencia) VALUES " +
                "('" + per.cedula + "', '" + per.nombre + "', '" + per.fecha_nacimiento + "', '" + per.residencia + "')", conexion);
            cmd.ExecuteNonQuery();
            conexion.Close();
        }

        public void EliminarDatos(Objeto_Persona per)
        {
            conexion = Conexion_BD.connection();
            conexion.Open();
            cmd = new NpgsqlCommand("DELETE FROM Persona WHERE cedula = '" + per.cedula + "'", conexion);
            cmd.ExecuteNonQuery();
            conexion.Close();
            MessageBox.Show("Datos Eliminados Correctamente");

        }

        public void Mostrar_Personas(DataGridView dgv)
        {
            conexion = Conexion_BD.connection();
            DataTable data_table = new DataTable();
            NpgsqlDataAdapter adapter = new NpgsqlDataAdapter("SELECT cedula, nombre, fecha_nacimiento, residencia FROM Persona", conexion);
            adapter.Fill(data_table);
            dgv.DataSource = data_table;
        }

        public void Busqueda_Personas(DataGridView dgv, Objeto_Persona per)
        {
            conexion = Conexion_BD.connection();
            DataTable data_table = new DataTable();
            NpgsqlDataAdapter adapter = new NpgsqlDataAdapter(" SELECT cedula, nombre, fecha_nacimiento, residencia FROM Persona WHERE fecha_nacimiento <= '" + per.fecha_nacimiento + "'", conexion);
            adapter.Fill(data_table);
            dgv.DataSource = data_table;
        }

        public void Modificar_Personas(Objeto_Persona per)
        {
            conexion = Conexion_BD.connection();
            conexion.Open();
            cmd = new NpgsqlCommand("UPDATE Persona SET nombre = '" + per.nombre + "', " + "fecha_nacimiento = '" + per.fecha_nacimiento +"', " + 
                "residencia = '" + per.residencia + "' WHERE cedula = '" + per.cedula + "'", conexion);
            cmd.ExecuteNonQuery();
            conexion.Close();
            MessageBox.Show("Datos Modificados Correctamente");

        }


    }
}
