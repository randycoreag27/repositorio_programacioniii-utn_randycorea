﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Objetos
{
    public class Objeto_Persona
    {
        public int id_persona { get; set; }
        public int cedula { get; set; }
        public string nombre { get; set; }
        public DateTime fecha_nacimiento { get; set; }
        public string residencia { get; set; }
        
    }
}
